Rem Attribute VBA_ModuleType=VBAModule
Option VBASupport 1
' Declare Global Arrays
Public Toggle_Option As Variant
Public Redefine_Database As Boolean
Public Last_Row_OLD As Integer, BTR_Count As Integer
Public Alpha_Roster As String, BIR As String, BTR As String '<<-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- Stores the Name of the File Locations
Public EDIPI_Rst() As String, DCTB_Rst() As String, Unit_Rst() As String, Shop_Rst() As String '<<-------------------------------------------------------------------------------------------------------------------------------------------------------------- For the Alpha Roster
Public BIR_Personnel() As String, BIR_Duty() As String, BIR_Contract() As String, BIR_Service() As String, BIR_PME_COMP() As String '<<------------------------------------------------------------------------------------------------------------------------------------- For the BIR
Public BTR_NAMES() As String, BTR_Security() As String, BTR_Education() As String, BTR_BCP() As String, BTR_PFT() As String, BTR_CFT() As String, BTR_Swim() As String, BTR_MCMAP() As String, BTR_NBC() As String, BTR_Rifle() As String, BTR_Pistol() As String  '<<---- For the BTR

' Extract PME data from the BTR; this specific type of information is so hard to extract, that I had to create this function for the job
Private Function PME_Extract(Wksheet As Worksheet, PME As String, PME_Append As String, Rank_Type As String)

    Dim First_Pass As Boolean: Rem-- Prevents each duplicates erasing routine from erasing too much
    Dim ONE_PME As Boolean: Rem-- SNCO's have 2 types of PME; if there is an entry for 1 of each this variable is still true
    Dim PME_Data As String: Rem-- all calculations will be stored here for the final result
    Dim PME_Type As String: Rem-- the original SNCO PME String 'PME' must be preserved; so, use another variable for string manipulations...
    Dim PME_Duplicates() As String: Rem-- store all duplicates inside this array for filtering later on...
    Dim Last_Career As Integer: Rem-- store the quantity of SNCO Career Course entries
    Dim Last_Advanced As Integer: Rem-- store the quantity of SNCO Advanced Course entries
    Dim PME_Start As Integer, PME_Finish As Integer, Temp_PME_Limit As Integer
    
    PME_Data = ""
    ONE_PME = False
    PME_Quantity = 0
    First_Pass = False
    With Wksheet.Cells
        Set StrSearch = .Find(what:="Service Schools/Special Skills", LookIn:=xlValues, LookAt:=xlPart)
        PME_Start = StrSearch.Row + 2: Rem-- the headers don't count as PME, so skip them
        Set StrSearch = .Find(what:="Test Scores", LookIn:=xlValues, LookAt:=xlPart)
        PME_Finish = StrSearch.Row - 1
    
        '>>Debug.Assert (Rank_Type <> "SNCO")
        If Rank_Type <> "SNCO" Then
            PME_Quantity = Application.WorksheetFunction.CountIf(.Range("C" & PME_Start & ":C" & PME_Finish), PME & STR_SPLIT(PME_Append, ".", 1))
            PME_Quantity = PME_Quantity + Application.WorksheetFunction.CountIf(.Range("C" & PME_Start & ":C" & PME_Finish), PME & STR_SPLIT(PME_Append, ".", 2))
        Else
            PME_Quantity = Application.WorksheetFunction.CountIf(.Range("C" & PME_Start & ":C" & PME_Finish), STR_SPLIT(PME, ".", 1) & STR_SPLIT(STR_SPLIT(PME_Append, "|", 1), ".", 1))
            PME_Quantity = PME_Quantity + Application.WorksheetFunction.CountIf(.Range("C" & PME_Start & ":C" & PME_Finish), STR_SPLIT(PME, ".", 1) & STR_SPLIT(STR_SPLIT(PME_Append, "|", 1), ".", 2))
            Last_Career = PME_Quantity
            PME_Quantity = PME_Quantity + Application.WorksheetFunction.CountIf(.Range("C" & PME_Start & ":C" & PME_Finish), STR_SPLIT(PME, ".", 2) & STR_SPLIT(STR_SPLIT(PME_Append, "|", 2), ".", 1))
            PME_Quantity = PME_Quantity + Application.WorksheetFunction.CountIf(.Range("C" & PME_Start & ":C" & PME_Finish), STR_SPLIT(PME, ".", 2) & STR_SPLIT(STR_SPLIT(PME_Append, "|", 2), ".", 2))
            Last_Advanced = PME_Quantity - Last_Career
        End If
        Dim First_Address As String, Current_Address As String, Last_Address As String
        
        ' Check for duplicates...
        If PME_Quantity > 0 Then
            ReDim PME_Duplicates(PME_Quantity - 1)
            If Rank_Type <> "SNCO" Then
                ' Collect all the years associated with this PME
                For i = 0 To PME_Quantity - 1
                    If i > 0 Then: Last_Address = StrSearch.Address
                    Set StrSearch = .Find(what:=PME, LookIn:=xlValues, LookAt:=xlPart)
                    If i = 0 Then: First_Address = StrSearch.Address
                    If i >= 0 Then: Current_Address = StrSearch.Address
                    If i >= 1 Then
                        If Current_Address = First_Address Then
                            First_Pass = True
                            PME_Quantity = PME_Quantity - 1
                            If i = Last_Career - 1 Then: Exit For
                        End If
                    Else: PME_Duplicates(i) = .Range(StrSearch.Address).Offset(0, 2) & "." & StrSearch.Row
                    End If
                    If First_Pass = False Then
                        If i > 1 Then
                            If Current_Address = Last_Address Then
                                PME_Quantity = PME_Quantity - 1
                                If i = Last_Career - 1 Then: Exit For
                            End If
                        Else
                            If i > 1 Then: PME_Duplicates(i) = .Range(StrSearch.Address).Offset(0, 2) & "." & StrSearch.Row
                        End If
                        First_Pass = False
                    End If
                Next i
            Else
                For j = 1 To 2: Rem-- the first loop verifies the correct quantity for Career; the second, for Advanced...
                    ' Collect all the years associated with this PME
                    Select Case j
                        Case 1: Temp_PME_Limit = Last_Career
                        Case 2: Temp_PME_Limit = Last_Advanced
                    End Select
                    If Temp_PME_Limit <> 0 Then
                        For i = 0 To Temp_PME_Limit - 1
                            If i > 0 Then: Last_Address = StrSearch.Address
                            Set StrSearch = .Find(what:=STR_SPLIT(PME, ".", j), LookIn:=xlValues, LookAt:=xlPart)
                            If i = 0 And StrSearch Is Nothing Then: Exit For
                            If i = 0 Then: First_Address = StrSearch.Address
                            If i >= 0 Then: Current_Address = StrSearch.Address
                            If i >= 1 Then
                                If Current_Address = First_Address Then
                                    First_Pass = True
                                    PME_Quantity = PME_Quantity - 1
                                    If i = Last_Career - 1 Then: Exit For
                                End If
                            Else: PME_Duplicates(i) = .Range(StrSearch.Address).Offset(0, 2) & "." & StrSearch.Row
                            End If
                            If First_Pass = False Then
                                If i > 1 Then
                                    If Current_Address = Last_Address Then
                                        PME_Quantity = PME_Quantity - 1
                                        If i = Last_Career - 1 Then: Exit For
                                    End If
                                Else
                                    If i > 1 Then: PME_Duplicates(i) = .Range(StrSearch.Address).Offset(0, 2) & "." & StrSearch.Row
                                End If
                            End If
                            First_Pass = False
                        Next i
                    End If
                Next j
            End If
        End If
        Erase PME_Duplicates()
        
        If Rank_Type = "SNCO" Then
            If PME_Quantity = 1 Then: ONE_PME = True
            If PME_Quantity = 2 Then: ONE_PME = True
        Else
            If PME_Quantity = 1 Then: ONE_PME = True
        End If
        
        ' Leave this function, if the Marine has not received any PME for their grade
        If PME_Quantity = 0 Then: Exit Function
        
        ' If the Marine has received at least one PME, then continue processing...
        If ONE_PME = True Then
        
            Rem-- do nothing if the Rank_Type = "SNCO"; conduct a search later, for it will be a 2-parter...
            If Rank_Type = "SNCO" Then
            Else
                Set StrSearch = .Find(what:=PME, LookIn:=xlValues, LookAt:=xlPart)
                PME = .Range(StrSearch.Address)
            End If
            Select Case Rank_Type
                Case "Junior"
                    PME_Data = PME & ": (MarineNet) - " & .Range(StrSearch.Address).Offset(0, 1) & " - " & .Range(StrSearch.Address).Offset(0, 2)
                Case "Corporal"
                    Rem-- determine if the course is Resident/MarineNet
                    If STR_SPLIT(PME, " ", 3) = "DEP" Then
                        PME_Data = PME & ": (MarineNet) - " & .Range(StrSearch.Address).Offset(0, 1) & " - " & .Range(StrSearch.Address).Offset(0, 2)
                    Else: PME_Data = PME & ": (Resident) - " & .Range(StrSearch.Address).Offset(0, 1) & " - " & .Range(StrSearch.Address).Offset(0, 2)
                    End If
                Case "Sergeant"
                    Rem-- determine if the course is Resident/MarineNet
                    If Len(PME) <> 8 Then
                        PME_Data = PME & ": (MarineNet) - " & .Range(StrSearch.Address).Offset(0, 1) & " - " & .Range(StrSearch.Address).Offset(0, 2)
                    Else: PME_Data = PME & ": (Resident) - " & .Range(StrSearch.Address).Offset(0, 1) & " - " & .Range(StrSearch.Address).Offset(0, 2)
                    End If
                Rem-- check for Career Course & Advanced Course (include both inside a single string; delimiter = ".")
                Case "SNCO"
                    '----------------
                    ' CAREER COURSE |
                    '````````````````
                    '
                    Rem-- determine if the course is Resident/MarineNet
                    Set StrSearch = .Find(what:=STR_SPLIT(PME, ".", 1), LookIn:=xlValues, LookAt:=xlPart)
                    If StrSearch Is Nothing Then
                        PME_Data = "SNCO CAREER: INCOMPLETE"
                    Else
                        If Len(.Range(StrSearch.Address)) <> 11 Then
                        
                            Rem-- determine if the Course is current/discontinued
                            If STR_SPLIT(.Range(StrSearch.Address), " ", 3) = "COURSE" Then
                                PME_Data = STR_SPLIT(PME, ".", 1) & ": (MarineNet) - " & .Range(StrSearch.Address).Offset(0, 1) & " - " & .Range(StrSearch.Address).Offset(0, 2)
                            Else: PME_Data = STR_SPLIT(PME, ".", 1) & ": (DISCONTINUED) - " & .Range(StrSearch.Address).Offset(0, 1) & " - " & .Range(StrSearch.Address).Offset(0, 2)
                            End If
                        Else: PME_Data = STR_SPLIT(PME, ".", 1) & ": (Resident) - " & .Range(StrSearch.Address).Offset(0, 1) & " - " & .Range(StrSearch.Address).Offset(0, 2)
                        End If
                    End If
                    '------------------
                    ' ADVANCED COURSE |
                    '``````````````````
                    '
                    Rem-- determine if the course is Resident/MarineNet
                    Set StrSearch = .Find(what:=STR_SPLIT(PME, ".", 2), LookIn:=xlValues, LookAt:=xlPart)
                    If StrSearch Is Nothing Then
                        PME_Data = PME_Data & " || SNCO ADVANCED: INCOMPLETE"
                    Else
                        If Len(.Range(StrSearch.Address)) <> 20 Then
                        
                            Rem-- determine if the Course is current/discontinued
                            If STR_SPLIT(.Range(StrSearch.Address), " ", 3) = "COURSE" Then
                                PME_Data = PME_Data & " || " & STR_SPLIT(PME, ".", 2) & ": (MarineNet) - " & .Range(StrSearch.Address).Offset(0, 1) & " - " & .Range(StrSearch.Address).Offset(0, 2)
                            Else: PME_Data = PME_Data & " || " & STR_SPLIT(PME, ".", 2) & ": (DISCONTINUED) - " & .Range(StrSearch.Address).Offset(0, 1) & " - " & .Range(StrSearch.Address).Offset(0, 2)
                            End If
                        Else: PME_Data = PME_Data & " || " & STR_SPLIT(PME, ".", 2) & ": (Resident) - " & .Range(StrSearch.Address).Offset(0, 1) & " - " & .Range(StrSearch.Address).Offset(0, 2)
                        End If
                    End If
            End Select
        Else
            ReDim PME_Duplicates(PME_Quantity - 1)

            ' Check for duplicates...
            If Rank_Type <> "SNCO" Then
                For i = 0 To PME_Quantity - 1
                    If i > 0 Then: Last_Address = StrSearch.Address
                    Set StrSearch = .Find(what:=PME, LookIn:=xlValues, LookAt:=xlPart)
                    If i = 0 And StrSearch Is Nothing Then: Exit For
                    If i = 0 Then: First_Address = StrSearch.Address
                    If i >= 0 Then: Current_Address = StrSearch.Address
                    If i >= 1 Then
                        If Current_Address = First_Address Then
                            First_Pass = True
                            PME_Quantity = PME_Quantity - 1
                            If i = Last_Career - 1 Then: Exit For
                        End If
                    Else: PME_Duplicates(i) = .Range(StrSearch.Address).Offset(0, 2) & "." & StrSearch.Row
                    End If
                    If First_Pass = False Then
                        If i > 1 Then
                            If Current_Address = Last_Address Then
                                PME_Quantity = PME_Quantity - 1
                                If i = Last_Career - 1 Then: Exit For
                            End If
                        Else
                            If i > 1 Then: PME_Duplicates(i) = .Range(StrSearch.Address).Offset(0, 2) & "." & StrSearch.Row
                        End If
                        First_Pass = False
                    End If
                Next i
                    
                ' Determine the latest year
                For i = 0 To PME_Quantity - 1
                    Dim Max_Year As Integer
            
                    If Max_Year = Empty Then
                        Max_Year = STR_SPLIT(PME_Duplicates(i), ".", 1)
                    Else
                        If PME_Duplicates(i) > Max_Year Then
                            Max_Year = STR_SPLIT(PME_Duplicates(i), ".", 1)
                        End If
                    End If
                        
                    ' The latest year has been found; now, find the row associated to that year
                    If i = PME_Quantity - 1 Then
                        For j = 0 To PME_Quantity - 1
                        
                            Rem-- only the latest version will get stored.
                            If Max_Year = STR_SPLIT(PME_Duplicates(j), ".", 1) Then
                                Set StrSearch = .Find(what:=PME, LookIn:=xlValues, LookAt:=xlPart)
                                PME = .Range(StrSearch.Address)
                                Select Case Rank_Type
                                    Case "Junior"
                                        PME = .Range("C" & STR_SPLIT(PME_Duplicates(j), ".", 2))
                                        PME_Data = PME & ": (MarineNet) - " & .Range("D" & STR_SPLIT(PME_Duplicates(j), ".", 2)) & " - " & .Range("E" & STR_SPLIT(PME_Duplicates(j), ".", 2))
                                    Case "Corporal"
                                        PME = .Range("C" & STR_SPLIT(PME_Duplicates(j), ".", 2))
                                        
                                        Rem-- determine if the course is Resident/MarineNet
                                        If STR_SPLIT(PME, " ", 3) = "DEP" Then
                                            PME_Data = PME & ": (MarineNet) - " & .Range("D" & STR_SPLIT(PME_Duplicates(j), ".", 2)) & " - " & .Range("E" & STR_SPLIT(PME_Duplicates(j), ".", 2))
                                        Else: PME_Data = PME & ": (Resident) - " & .Range("D" & STR_SPLIT(PME_Duplicates(j), ".", 2)) & " - " & .Range("E" & STR_SPLIT(PME_Duplicates(j), ".", 2))
                                        End If
                                    Case "Sergeant"
                                        PME = .Range("C" & STR_SPLIT(PME_Duplicates(j), ".", 2))
                                        
                                        Rem-- determine if the course is Resident/MarineNet
                                        If Len(PME) <> 8 Then
                                            PME_Data = PME & ": (MarineNet) - " & .Range("D" & STR_SPLIT(PME_Duplicates(j), ".", 2)) & " - " & .Range("E" & STR_SPLIT(PME_Duplicates(j), ".", 2))
                                        Else: PME_Data = PME & ": (Resident) - " & .Range("D" & STR_SPLIT(PME_Duplicates(j), ".", 2)) & " - " & .Range("E" & STR_SPLIT(PME_Duplicates(j), ".", 2))
                                        End If
                                End Select
                            End If
                        Next j
                    End If
                Next i
            Else
                ' Collect all the years associated with this PME _
                ````````````````````````````````````````````````
                Dim k As Long: Rem-- I need 'k', because I'm going to have 3 Loops-- this main one, and 2 nested-loops...
                Dim SNCO_PME As String: Rem-- the variable 'PME' must not be altered; thus, a new variable will be used...
                                
                For k = 1 To 2
                    PME_Quantity = Application.WorksheetFunction.CountIf(.Range("C" & PME_Start & ":C" & PME_Finish), STR_SPLIT(PME, ".", k))
                    ReDim PME_Duplicates(PME_Quantity)
                    
                    ' Collect all the years associated with this PME
                    If PME_Quantity <> 0 Then
                        For i = 0 To PME_Quantity - 1
                            If i > 0 Then: Last_Address = StrSearch.Address
                            Set StrSearch = .Find(what:=STR_SPLIT(PME, ".", k), LookIn:=xlValues, LookAt:=xlPart)
                            If i = 0 And StrSearch Is Nothing Then: Exit For
                            If i = 0 Then: First_Address = StrSearch.Address
                            If i >= 0 Then: Current_Address = StrSearch.Address
                            If i >= 1 Then
                                If Current_Address = First_Address Then
                                    First_Pass = True
                                    PME_Quantity = PME_Quantity - 1
                                    If i = Last_Career - 1 Then: Exit For
                                End If
                            Else: PME_Duplicates(i) = .Range(StrSearch.Address).Offset(0, 2) & "." & StrSearch.Row
                            End If
                            If First_Pass = False Then
                                If i > 1 Then
                                    If Current_Address = Last_Address Then
                                        PME_Quantity = PME_Quantity - 1
                                        If i = Last_Career - 1 Then: Exit For
                                    End If
                                Else
                                    If i > 1 Then: PME_Duplicates(i) = .Range(StrSearch.Address).Offset(0, 2) & "." & StrSearch.Row
                                End If
                            End If
                            First_Pass = False
                        Next i
                    End If
                    
                    Rem-- these integers will be set as conditions later on...
                    Select Case k
                        Case 1: Last_Career = PME_Quantity: Temp_PME_Limit = Last_Career
                        Case 2: Last_Advanced = PME_Quantity - Last_Career: Temp_PME_Limit = Last_Advanced
                    End Select
                    For i = 0 To PME_Quantity - 1
                        Set StrSearch = .Find(what:=STR_SPLIT(PME, ".", k), LookIn:=xlValues, LookAt:=xlPart)
                        PME_Duplicates(i) = .Range(StrSearch.Address).Offset(0, 2) & "." & StrSearch.Row
                    Next i
                                                    
                    ' Determine the latest year
                    For i = 0 To PME_Quantity - 1
                        If Max_Year = Empty Then
                            Max_Year = STR_SPLIT(PME_Duplicates(i), ".", 1)
                        Else
                            If PME_Duplicates(i) > Max_Year Then
                                Max_Year = STR_SPLIT(PME_Duplicates(i), ".", 1)
                            End If
                        End If
                            
                        ' The latest year has been found for SNCO Career Course; now, find the row associated to that year
                        Rem-- check for Career Course & Advanced Course (include both inside a single string; delimiter = ".")
                        If Temp_PME_Limit <> 0 Then
                            For j = 0 To PME_Quantity - 1
                                If k = 1 Then
                                    If Max_Year = STR_SPLIT(PME_Duplicates(j), ".", 1) Then
                                        '----------------
                                        ' CAREER COURSE |
                                        '````````````````
                                        '
                                        Rem-- determine if the course is Resident/MarineNet
                                        Set StrSearch = .Find(what:=STR_SPLIT(PME, ".", 1), LookIn:=xlValues, LookAt:=xlPart)
                                        If StrSearch Is Nothing Then
                                            PME_Data = "SNCO CAREER: INCOMPLETE"
                                        Else
                                            SNCO_PME = .Range(StrSearch.Address)
                                            If Len(.Range(StrSearch.Address)) <> 11 Then
                                            
                                                Rem-- determine if the Course is current/discontinued
                                                If STR_SPLIT(.Range(StrSearch.Address), " ", 3) = "COURSE" Then
                                                    PME_Data = SNCO_PME & " (MarineNet) - " & .Range("D" & STR_SPLIT(PME_Duplicates(j), ".", 2)) & " - " & .Range("E" & STR_SPLIT(PME_Duplicates(j), ".", 2))
                                                Else: PME_Data = SNCO_PME & " (DISCONTINUED) - " & .Range("D" & STR_SPLIT(PME_Duplicates(j), ".", 2)) & " - " & .Range("E" & STR_SPLIT(PME_Duplicates(j), ".", 2))
                                                End If
                                            Else: PME_Data = SNCO_PME & " (Resident) - " & .Range("D" & STR_SPLIT(PME_Duplicates(j), ".", 2)) & " - " & .Range("E" & STR_SPLIT(PME_Duplicates(j), ".", 2))
                                            End If
                                        End If
                                    Else
                                        '------------------
                                        ' ADVANCED COURSE |
                                        '``````````````````
                                        '
                                        Rem-- determine if the course is Resident/MarineNet
                                        Set StrSearch = .Find(what:=STR_SPLIT(PME, ".", 2), LookIn:=xlValues, LookAt:=xlPart)
                                        If StrSearch Is Nothing Then
                                            PME_Data = PME_Data & " || SNCO ADVANCED: INCOMPLETE"
                                        Else
                                            SNCO_PME = .Range(StrSearch.Address)
                                            If Len(.Range(StrSearch.Address)) <> 20 Then
                                            
                                                Rem-- determine if the Course is current/discontinued
                                                If STR_SPLIT(.Range(StrSearch.Address), " ", 3) = "COURSE" Then
                                                    PME_Data = PME_Data & " || " & SNCO_PME & " (MarineNet) - " & .Range("D" & STR_SPLIT(PME_Duplicates(j), ".", 2)) & " - " & .Range("E" & STR_SPLIT(PME_Duplicates(j), ".", 2))
                                                Else: PME_Data = PME_Data & " || " & SNCO_PME & " (DISCONTINUED) - " & .Range("D" & STR_SPLIT(PME_Duplicates(j), ".", 2)) & " - " & .Range("E" & STR_SPLIT(PME_Duplicates(j), ".", 2))
                                                End If
                                            Else: PME_Data = PME_Data & " || " & SNCO_PME & " (Resident) - " & .Range("D" & STR_SPLIT(PME_Duplicates(j), ".", 2)) & " - " & .Range("E" & STR_SPLIT(PME_Duplicates(j), ".", 2))
                                            End If
                                        End If
                                    End If
                                End If
                            Next j
                        End If
                    Next i
                    Erase PME_Duplicates()
                Next k
            End If
        End If
    End With
    PME_Extract = PME_Data
End Function

' Capture the address of the MOL files for calling the next sub-routine
Sub Get_Files()
    Dim Msg As String, Response As String
    
    ' Prompt the User to get the Alpha Roster, before continuing
    Msg = "In order to update the Database, you must first Download the Alpha Roster (Excel 2007 Format) off of MOL.  "
    Msg = Msg & "If you have already done so, then select it via the next dialog box; otherwise, click cancel."
    Response = MsgBox(Msg, 65, "Get the Alpha Roster")
    
    If Response = 1 Then
        Alpha_Roster = Application.GetOpenFilename(, , "Select the Alpha Roster")
        
        ' Prompt the User to get the BIR file, before continuing
        If Alpha_Roster <> "False" Then
            Msg = "In order to update the Database, you must also Download the BIR file (Excel 2007 Format) off of MOL.  "
            Msg = Msg & "If you have already done so, then select it via the next dialog box; otherwise, click cancel."
            Response = MsgBox(Msg, 65, "Get the BIR")
            
            If Response = 1 Then
                BIR = Application.GetOpenFilename(, , "Select the BIR File")
                
                ' Prompt the User to get the BTR file, before continuing
                If BIR <> "False" Then
                    Msg = "Last File!  Next, you must Download the BTR file (Excel 2007 Format) off of MOL.  "
                    Msg = Msg & "If you have already done so, then select it via the next dialog box; otherwise, click cancel."
                    Response = MsgBox(Msg, 65, "Get the BTR")
                    
                    If Response = 1 Then: BTR = Application.GetOpenFilename(, , "Select the BTR File")
                End If
            End If
        End If
    End If
    
    ' If ALL Files have been selected, then continue to the Update Procedure
    If BIR <> "False" And BIR <> "" And _
       BTR <> "False" And BTR <> "" And _
       Alpha_Roster <> "False" And Alpha_Roster <> "" Then
           
           ' Inform the user to wait until the program announces that it has completed the update process
           Msg = "The Update Procedure has been initialized.  Please wait until the " & Chr(34) & "Update Completed" & Chr(34)
           Msg = Msg & " message has appeared before continuing with this session."
           Response = MsgBox(Msg, vbOKOnly & vbInformation, "Update Initialized")
       
           Call Updater
       End If
End Sub

' Gotta go Fast... SONIC!  Gotta go Fast... SONIC! Gotta go Faster-Faster, FASTER-FASTER-FASTER!?
Private Sub Go_Fasters()
    
    ' [A] : Application Object
    Dim A As Object
    Set A = Application
    A.Calculation = xlAutomatic
    
    ' Store default settings inside of an array
    Toggle_Option = Array(A.ScreenUpdating, A.DisplayStatusBar, A.Calculation, A.EnableEvents)
    
    ' Turn off unnecassary functions
    Application.ScreenUpdating = Not Toggle_Option(0)
    Application.DisplayStatusBar = Not Toggle_Option(1)
    Application.Calculation = xlCalculationManual
    Application.EnableEvents = Not Toggle_Option(3)
End Sub

' Configure settings to their original state
Private Sub Restore()
    
    ' If "Go_Fasters" has not been called, then exit-- for values are already default
    On Error GoTo ExitPoint
    
    ' Restore default settings
    Application.ScreenUpdating = Toggle_Option(0)
    Application.DisplayStatusBar = Toggle_Option(1)
    Application.Calculation = Toggle_Option(2)
    Application.EnableEvents = Toggle_Option(3)

ExitPoint:
End Sub

' Update the Spreadsheet!  In order for the changes to take effect on the Main Console without leaving Excel-- create a new intance of it (i.e. Click "Exit Program" >> Click "Enter").  This allows for Re-Calculation of the Userform Controls.
Sub Updater()

    ' Declare MOL Excel Objects
    Dim BTR_Wkb As Workbook, BIR_Wkb As Workbook, Roster_Wkb As Workbook
    Dim WS As Worksheet, Roster_Wks As Worksheet, BTR_Wks As Worksheet, BIR_Wks As Worksheet
    Dim Last_Cell As Integer
    
    ' Declare Program variables
    Dim Program As Workbook
    Dim Database As Worksheet, Log As Worksheet, Test As Worksheet
    
    ' Define Program references
    Set Program = ThisWorkbook
    Set Database = Program.Worksheets("RAW DATA")
    Set Log = Program.Worksheets("LOG")
    Set Test = Program.Worksheets("TEST")
    
    ' Turn off some Excel functionality so code runs faster
    Call Go_Fasters

    ' Clear the Current Database first
    Dim Last_Row As Integer
    Dim i As Long, j As Long
    
    Last_Row_OLD = LR()
    Last_Row = LR()
    Database.Range("A3:AI" & Last_Row).ClearContents
    '>>For i = 3 To Last_Row: Database.Rows("3:3").Delete: Next i
        
    '----------------------------------------------------------
    ' Gather all necassary information from the BTR Workbook...
    '----------------------------------------------------------
    Set BTR_Wkb = Workbooks.Open(BTR)
    
    Dim Day As String, Month As String, Year As String
    Dim Wks As Integer, iDay As Integer, iMonth As Integer, iYear As Integer
    
    BTR_Count = BTR_Wkb.Sheets.Count
    ReDim BTR_NAMES(3, BTR_Count)
    ReDim BTR_Security(0, BTR_Count)
    ReDim BTR_Education(5, BTR_Count)
    ReDim BTR_BCP(5, BTR_Count)
    ReDim BTR_PFT(7, BTR_Count)
    ReDim BTR_CFT(7, BTR_Count)
    ReDim BTR_Swim(2, BTR_Count)
    ReDim BTR_MCMAP(5, BTR_Count)
    ReDim BTR_NBC(1, BTR_Count)
    ReDim BTR_Rifle(7, BTR_Count)
    ReDim BTR_Pistol(5, BTR_Count)
    
    ' ADDED BECAUSE OF MARINE-NET REQUIREMENT
    ReDim BIR_Personnel(5, BTR_Count)
    ReDim BIR_Duty(6, BTR_Count)
    ReDim BIR_Contract(6, BTR_Count)
    ReDim BIR_Service(5, BTR_Count)
    ReDim BIR_PME_COMP(0, BTR_Count)
    
    ' Alter the Range Addresses for the Database to hold the correct amount of records counted from the BTR
    Redefine_Database = True
    If Database_Empty = True Then: Call Named_Ranges
    
    Wks = 0
    For Each WS In BTR_Wkb.Sheets
        Dim Page_Num As String
        
        ' Set the Address for the current page in the Workbook
        Page_Num = "Page1_" & Wks + 1
        Set BTR_Wks = BTR_Wkb.Sheets(Page_Num)
        
        With BTR_Wks.Cells
            Dim StrSearch As Variant
            Dim StrFound As String
            Dim Middle_Initial As String
                        
            ' Capture the Rank and Name
            BTR_NAMES(0, Wks) = STR_SPLIT(BTR_Wks.Range("A4"), " ", 1) '<<------------------------------------------------- Get the Rank of the Marine
            Middle_Initial = Mid(BTR_Wks.Range("A4"), Len(BTR_Wks.Range("A4")), 1)
            If Middle_Initial = " " Then  '<<------------------------------------------------------------------------------ Checks to see if the Marine is Missing a middle initial; if so, things get tricky!
                For i = 1 To 20
                    If Mid(BTR_Wks.Range("A4") & "-", i, 1) = " " Then Exit For
                Next i
                BTR_NAMES(1, Wks) = Mid(BTR_Wks.Range("A4") & "-", i + 1, (Len(BTR_Wks.Range("A4") & "-") - i)) ' <<------- Getting the Marine's Name without rank, is a bit tricky!
                For i = 1 To 20
                    If Mid(BTR_NAMES(1, Wks), i, 1) = " " Then Exit For
                Next i
                BTR_NAMES(1, Wks) = Mid(BTR_NAMES(1, Wks), 1, i - 1) & Mid(BTR_NAMES(1, Wks), i + 1) & "." ' <<------------ Re-format the Name to match the Database & Alpha Roster, using Concatenation
                '>>BTR_NAMES(2, Wks) = STR_SPLIT(BTR_Wks.Range("I6"), ":", 2) '<<------------------------------------------ Get the Shop of the Marine
            Else
                For i = 1 To 20
                    If Mid(BTR_Wks.Range("A4"), i, 1) = " " Then Exit For
                Next i
                BTR_NAMES(1, Wks) = Mid(BTR_Wks.Range("A4"), i + 1, (Len(BTR_Wks.Range("A4")) - i)) ' <<------------------- Getting the Marine's Name without rank, is a bit tricky!
                For i = 1 To 20
                    If Mid(BTR_NAMES(1, Wks), i, 1) = " " Then Exit For
                Next i
                BTR_NAMES(1, Wks) = Mid(BTR_NAMES(1, Wks), 1, i - 1) & Mid(BTR_NAMES(1, Wks), i + 1) & "." ' <<------------ Re-format the Name to match the Database & Alpha Roster, using Concatenation
                '>>BTR_NAMES(2, Wks) = STR_SPLIT(BTR_Wks.Range("I6"), ":", 2) '<<------------------------------------------ Get the Shop of the Marine
            End If
            
            
            ' MODIFY PROGRAM TO INCLUDE MARINE-NET INFORMATION
            '`````````````````````````````````````````````````
            '
            ' START MODIFICATION
            '
            '
            '*************************************************
            '
            '
            '
            '
            '
            
            
            ' OLD-1
            '-------
            ' Capture the USMC E-Mail Address of the Marine
            'BIR_Personnel(2, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(11, 0), ":", 2)
            'BIR_Personnel(2, Wks) = Mid(BIR_Personnel(2, Wks), 3, Len(BIR_Personnel(2, Wks)))
            
            ' NEW-1
            '-------
            ' Capture the SERE 100: CODE OF CONDUCT-JTSERE100A Date
            Set StrSearch = .Find(what:="CODE OF CONDUCT TRAINING", LookIn:=xlValues, LookAt:=xlWhole)
            If StrSearch Is Nothing Then
                BIR_Personnel(2, Wks) = "NOT COMPLETE"
            Else: BIR_Personnel(2, Wks) = .Range(StrSearch.Address).Offset(0, -1)
            End If

            ' OLD-2
            '-------
            ' Capture the Civilian Education Level of the Marine
            'BIR_Personnel(3, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(4, 0).Offset(0, 1), ":", 2)
            'BIR_Personnel(3, Wks) = Mid(BIR_Personnel(3, Wks), 3, Len(BIR_Personnel(3, Wks)))
            
            ' NEW-2
            '-------
            ' Capture the COMBATING TRAFFICKING - DD01AO0000 Date
            Set StrSearch = .Find(what:="COMBATING TRAFFICKING IN PERSONS", LookIn:=xlValues, LookAt:=xlWhole)
            If StrSearch Is Nothing Then
                BIR_Personnel(3, Wks) = "NOT COMPLETE"
            Else: BIR_Personnel(3, Wks) = .Range(StrSearch.Address).Offset(0, -1)
            End If
                        
            ' OLD-3
            ' Capture the Ed Certification of the Marine
            'BIR_Personnel(4, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(5, 0).Offset(0, 1), ":", 2)
            'BIR_Personnel(4, Wks) = Mid(BIR_Personnel(4, Wks), 3, Len(BIR_Personnel(4, Wks)))
            
            ' NEW-3
            '-------
            ' Capture the Semper Fit: Alcohol and Drug Rec, Abuse, and Prevention Date
            Set StrSearch = .Find(what:="ALCOHOL AND SUBSTANCE ABUSE PREVENTION AND CONTROL TRNG (SF)", LookIn:=xlValues, LookAt:=xlWhole)
            If StrSearch Is Nothing Then
                BIR_Personnel(4, Wks) = "NOT COMPLETE"
            Else: BIR_Personnel(4, Wks) = .Range(StrSearch.Address).Offset(0, -1)
            End If
            
            ' OLD-4
            '-------
            ' Capture the Major of the Marine
            'BIR_Personnel(5, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(6, 0).Offset(0, 1), ":", 2)
            'BIR_Personnel(5, Wks) = Mid(BIR_Personnel(5, Wks), 3, Len(BIR_Personnel(2, Wks)))
            
            ' NEW-4
            '-------
            ' Semper Fit: Nutrition Date
            Set StrSearch = .Find(what:="NUTRITION TRAINING (SF)", LookIn:=xlValues, LookAt:=xlWhole)
            If StrSearch Is Nothing Then
                BIR_Personnel(5, Wks) = "NOT COMPLETE"
            Else: BIR_Personnel(5, Wks) = .Range(StrSearch.Address).Offset(0, -1)
            End If
            
            ' OLD-5
            '-------
            ' Capture the Marines' Duty Status
            'Set StrSearch = .Find(what:="Duty Status Information", LookIn:=xlValues, LookAt:=xlWhole)
            'BIR_Duty(0, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(1, 0), ":", 2)
            'BIR_Duty(0, Wks) = Mid(BIR_Duty(0, Wks), 3, Len(BIR_Duty(0, Wks)))
            
            ' NEW-5
            '-------
            ' Capture the Semper Fit: Fitness Date
            Set StrSearch = .Find(what:="PHYSICAL FITNESS TRAINING (SF)", LookIn:=xlValues, LookAt:=xlWhole)
            If StrSearch Is Nothing Then
                BIR_Duty(0, Wks) = "NOT COMPLETE"
            Else: BIR_Duty(0, Wks) = .Range(StrSearch.Address).Offset(0, -1)
            End If
                        
            ' OLD-6
            '-------
            ' Capture the Marines' Duty Status Limit
            'BIR_Duty(1, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(2, 0), ":", 2)
            'BIR_Duty(1, Wks) = Mid(BIR_Duty(1, Wks), 3, Len(BIR_Duty(1, Wks)))
            
            ' NEW-6
            '-------
            ' Capture the Marines' Semper Fit: Hypertension/ High Cholesterol Date
            Set StrSearch = .Find(what:="HYPERTENSION EDUCATION AND CONTROL TRAINING (SF)", LookIn:=xlValues, LookAt:=xlWhole)
            If StrSearch Is Nothing Then
                BIR_Duty(1, Wks) = "NOT COMPLETE"
            Else: BIR_Duty(1, Wks) = .Range(StrSearch.Address).Offset(0, -1)
            End If
            
            ' OLD-7
            '-------
            ' Capture the Marines' Duty Status Limit Date
            'BIR_Duty(2, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(3, 0), ":", 2)
            'BIR_Duty(2, Wks) = Mid(BIR_Duty(2, Wks), 3, Len(BIR_Duty(2, Wks)))
            'Year = STR_SPLIT(BIR_Duty(2, Wks), "-", 1)
            'Month = STR_SPLIT(BIR_Duty(2, Wks), "-", 2)
            'Day = STR_SPLIT(BIR_Duty(2, Wks), "-", 3)
            '
            ' Convert the Duty Status Limit Date into an integer
            'If Year <> "" Then
            '    iYear = CInt(Year)
            '    iMonth = CInt(Month)
            '    iDay = CInt(Day)
            '
            '    ' Set the Duty Status Limit Date element using the corrected format
            '    BIR_Duty(2, Wks) = DateSerial(iYear, iMonth, iDay)
            'End If
            
            ' NEW-7
            '-------
            ' Capture the Marines' Semper Fit: Tobacco Cessation Date
            Set StrSearch = .Find(what:="TOBACCO PREVENTION AND CESSATION (SF)", LookIn:=xlValues, LookAt:=xlWhole)
            If StrSearch Is Nothing Then
                BIR_Duty(2, Wks) = "NOT COMPLETE"
            Else: BIR_Duty(2, Wks) = .Range(StrSearch.Address).Offset(0, -1)
            End If
            
            ' OLD-8
            '-------
            ' Capture the Marines' Str Cat
            'BIR_Duty(3, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(4, 0), ":", 2)
            'BIR_Duty(3, Wks) = Mid(BIR_Duty(3, Wks), 3, Len(BIR_Duty(3, Wks)))

            ' NEW-8
            '-------
            ' Capture the Marines' Semper Fit: Sexual Health Date
            Set StrSearch = .Find(what:="SEXUAL HEALTH (STI/HIV) TRAINING (SF)", LookIn:=xlValues, LookAt:=xlWhole)
            If StrSearch Is Nothing Then
                BIR_Duty(3, Wks) = "NOT COMPLETE"
            Else: BIR_Duty(3, Wks) = .Range(StrSearch.Address).Offset(0, -1)
            End If

            ' OLD-9
            '-------
            ' Capture the Marines' Str Cat Date
            'BIR_Duty(4, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(5, 0), ":", 2)
            'BIR_Duty(4, Wks) = Mid(BIR_Duty(4, Wks), 3, Len(BIR_Duty(4, Wks)))
            'Year = STR_SPLIT(BIR_Duty(4, Wks), "-", 1)
            'Month = STR_SPLIT(BIR_Duty(4, Wks), "-", 2)
            'Day = STR_SPLIT(BIR_Duty(4, Wks), "-", 3)
            '
            ' Convert the Str Cat Date into an integer
            'If Year <> "" Then
            '    iYear = CInt(Year)
            '    iMonth = CInt(Month)
            '    iDay = CInt(Day)
            '
            '    ' Set the Duty Status Limit Date element using the corrected format
            '    BIR_Duty(4, Wks) = DateSerial(iYear, iMonth, iDay)
            'End If

            
            ' NEW-9
            '-------
            ' Capture the Marines' Semper Fit: Injury Prevention Date
            Set StrSearch = .Find(what:="INJURY PREVENTION TRAINING (SF)", LookIn:=xlValues, LookAt:=xlWhole)
            If StrSearch Is Nothing Then
                BIR_Duty(4, Wks) = "NOT COMPLETE"
            Else: BIR_Duty(4, Wks) = .Range(StrSearch.Address).Offset(0, -1)
            End If
            
            ' OLD-10
            '--------
            ' Capture the Marines' Str Combat Casualty
            'BIR_Duty(5, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(6, 0), ":", 2)
            'BIR_Duty(5, Wks) = Mid(BIR_Duty(5, Wks), 3, Len(BIR_Duty(5, Wks)))
            
            ' NEW-10
            '--------
            ' Capture the Marines' Joint Anti-Terrorism Level 1 - JATLV10000 Date
            Set StrSearch = .Find(what:="ANTI-TERRORIST ANNUAL TRAINING DATE", LookIn:=xlValues, LookAt:=xlWhole)
            If StrSearch Is Nothing Then
                BIR_Duty(5, Wks) = "NOT COMPLETE"
            Else: BIR_Duty(5, Wks) = .Range(StrSearch.Address).Offset(0, -1)
            End If
            
            ' OLD-11
            '--------
            ' Capture the Marines' Str Combat Casualty Date
            'BIR_Duty(6, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(7, 0), ":", 2)
            'BIR_Duty(6, Wks) = Mid(BIR_Duty(6, Wks), 3, Len(BIR_Duty(6, Wks)))
            
            ' NEW-11
            '--------
            ' Capture the Marines' Records Management (DON) - Everyone's Responsibility M01RMT0700 Date
            Set StrSearch = .Find(what:="MC RECORDS MANAGEMENT COURSE", LookIn:=xlValues, LookAt:=xlWhole)
            If StrSearch Is Nothing Then
                BIR_Duty(6, Wks) = "NOT COMPLETE"
            Else: BIR_Duty(6, Wks) = .Range(StrSearch.Address).Offset(0, -1)
            End If
            
            ' OLD-12
            '--------
            ' Capture the Marines' Original Contract Entry Date
            'Set StrSearch = .Find(what:="Contract Information", LookIn:=xlValues, LookAt:=xlWhole)
            'BIR_Contract(0, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(6, 0), ":", 2)
            'BIR_Contract(0, Wks) = Mid(BIR_Contract(0, Wks), 3, Len(BIR_Contract(0, Wks)))
            'Year = Mid(BIR_Contract(0, Wks), 1, 4)
            'Month = Mid(BIR_Contract(0, Wks), 5, 2)
            'Day = Mid(BIR_Contract(0, Wks), 7, 2)
            '
            ' Convert the Original Contract Entry Date into an integer
            'iYear = CInt(Year)
            'iMonth = CInt(Month)
            'iDay = CInt(Day)
            '
            ' Set the Original Contract Entry Date element using the corrected format
            'BIR_Contract(0, Wks) = DateSerial(iYear, iMonth, iDay)
            
            ' NEW-12
            '--------
            ' Capture the Marines' Personal Identifiable Information (PII) - PII0090000 Date
            Set StrSearch = .Find(what:="PERSONAL IDENTIFIABLE INFORMATION", LookIn:=xlValues, LookAt:=xlWhole)
            If StrSearch Is Nothing Then
                BIR_Contract(0, Wks) = "NOT COMPLETE"
            Else: BIR_Contract(0, Wks) = .Range(StrSearch.Address).Offset(0, -1)
            End If
            
            ' OLD-13
            '--------
            ' Capture the Marines' Enlist/Accept Date
            'BIR_Contract(1, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(4, 0), ":", 2)
            'BIR_Contract(1, Wks) = Mid(BIR_Contract(1, Wks), 3, Len(BIR_Contract(1, Wks)))
            'Year = Mid(BIR_Contract(1, Wks), 1, 4)
            'Month = Mid(BIR_Contract(1, Wks), 5, 2)
            'Day = Mid(BIR_Contract(1, Wks), 7, 2)
            '
            ' Convert the Enlist/Accept Date into an integer
            'iYear = CInt(Year)
            'iMonth = CInt(Month)
            'iDay = CInt(Day)
            '
            ' Set the Enlist/Accept Date element using the corrected format
            'BIR_Contract(1, Wks) = DateSerial(iYear, iMonth, iDay)
            
            ' NEW-13
            '--------
            ' Capture the Marines' Heat Related Illness Date
            Set StrSearch = .Find(what:="HEAT INJURY PREVENTION PROGRAM", LookIn:=xlValues, LookAt:=xlWhole)
            If StrSearch Is Nothing Then
                BIR_Contract(1, Wks) = "NOT COMPLETE"
            Else: BIR_Contract(1, Wks) = .Range(StrSearch.Address).Offset(0, -1)
            End If
            
            ' OLD-14
            '--------
            ' Capture the Marines' EAS Date
            'BIR_Contract(2, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(1, 0), ":", 2)
            'BIR_Contract(2, Wks) = Mid(BIR_Contract(2, Wks), 3, Len(BIR_Contract(2, Wks)))
            'Year = Mid(BIR_Contract(2, Wks), 1, 4)
            'Month = Mid(BIR_Contract(2, Wks), 5, 2)
            'Day = Mid(BIR_Contract(2, Wks), 7, 2)
            '
            ' Convert the EAS Date into an integer
            'iYear = CInt(Year)
            'iMonth = CInt(Month)
            'iDay = CInt(Day)
            '
            ' Set the EAS Date element using the corrected format
            'BIR_Contract(2, Wks) = DateSerial(iYear, iMonth, iDay)
            
            ' NEW-14
            '--------
            ' Capture the Marines' Annual Equal Opportunity Training Date
            Set StrSearch = .Find(what:="ANNUAL MILITARY EO TRAINING (MEO)", LookIn:=xlValues, LookAt:=xlWhole)
            If StrSearch Is Nothing Then
                BIR_Contract(2, Wks) = "NOT COMPLETE"
            Else: BIR_Contract(2, Wks) = .Range(StrSearch.Address).Offset(0, -1)
            End If
            
            ' OLD-15
            '--------
            ' Capture the Marines' EOS Date
            'BIR_Contract(3, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(2, 0), ":", 2)
            'BIR_Contract(3, Wks) = Mid(BIR_Contract(3, Wks), 3, Len(BIR_Contract(3, Wks)))
            'Year = Mid(BIR_Contract(3, Wks), 1, 4)
            'Month = Mid(BIR_Contract(3, Wks), 5, 2)
            'Day = Mid(BIR_Contract(3, Wks), 7, 2)
            '
            ' Convert the EOS Date into an integer
            'iYear = CInt(Year)
            'iMonth = CInt(Month)
            'iDay = CInt(Day)
            '
            ' Set the EOS Date element using the corrected format
            'BIR_Contract(3, Wks) = DateSerial(iYear, iMonth, iDay)
            
            ' NEW-15
            '--------
            ' Capture the Marines' SACO Date
            Set StrSearch = .Find(what:="DRUG LECTURE", LookIn:=xlValues, LookAt:=xlWhole)
            If StrSearch Is Nothing Then
                BIR_Contract(3, Wks) = "NOT COMPLETE"
            Else: BIR_Contract(3, Wks) = .Range(StrSearch.Address).Offset(0, -1)
            End If
            
            ' OLD-16
            '--------
            ' Capture the Marines' PEF
            'BIR_Contract(4, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(7, 0).Offset(0, 1), ":", 2)
            'BIR_Contract(4, Wks) = Mid(BIR_Contract(4, Wks), 3, Len(BIR_Contract(4, Wks)))
            
            ' NEW-16
            '--------
            ' Capture the Marines' Operational Risk Management (ORM) Training Date
            Set StrSearch = .Find(what:="RISK MANAGEMENT", LookIn:=xlValues, LookAt:=xlWhole)
            If StrSearch Is Nothing Then
                BIR_Contract(4, Wks) = "NOT COMPLETE"
            Else: BIR_Contract(4, Wks) = .Range(StrSearch.Address).Offset(0, -1)
            End If
            
            
            ' MODIFY PROGRAM TO INCLUDE MARINE-NET INFORMATION
            '`````````````````````````````````````````````````
            '
            ' END MODIFICATION
            '
            '
            '*************************************************
            '
            '
            '
            '
            '
            
            
            ' Capture the Security Eligible Code
            Set StrSearch = .Find(what:="Service Training", LookIn:=xlValues, LookAt:=xlWhole)
            BTR_Security(0, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(2, 0), ":", 2)
            BTR_Security(0, Wks) = Mid(BTR_Security(0, Wks), 2, 1)
            
            ' Check to see if the Marine has completed the Annual Opsec Training
            Set StrSearch = .Find(what:="ANNUAL OPSEC TRAINING", LookIn:=xlValues, LookAt:=xlWhole)
            If StrSearch Is Nothing Then
                BTR_Education(0, Wks) = "INCOMPLETE"
            Else
                BTR_Education(0, Wks) = .Range(StrSearch.Address).Offset(0, -1)
                Year = STR_SPLIT(BTR_Education(0, Wks), "/", 1)
                Month = STR_SPLIT(BTR_Education(0, Wks), "/", 2)
                Day = STR_SPLIT(BTR_Education(0, Wks), "/", 3)
                
                ' Convert the Opsec Date into an integer
                iYear = CInt(Year)
                iMonth = CInt(Month)
                iDay = CInt(Day)
                
                ' Set the Opsec Date Element using the corrected format
                BTR_Education(0, Wks) = DateSerial(iYear, iMonth, iDay)
            End If
            
            ' Check to see if the Marine has completed the Information Assurance Awareness Training
            Set StrSearch = .Find(what:="INFORMATION ASSURANCE AWARENESS TRAINING", LookIn:=xlValues, LookAt:=xlWhole)
            If StrSearch Is Nothing Then
                BTR_Education(1, Wks) = "INCOMPLETE"
            Else
                BTR_Education(1, Wks) = .Range(StrSearch.Address).Offset(0, -1)
                Year = STR_SPLIT(BTR_Education(1, Wks), "/", 1)
                Month = STR_SPLIT(BTR_Education(1, Wks), "/", 2)
                Day = STR_SPLIT(BTR_Education(1, Wks), "/", 3)
                
                ' Convert the IAA Date into an integer
                iYear = CInt(Year)
                iMonth = CInt(Month)
                iDay = CInt(Day)
                
                ' Set the IAA Date Element using the corrected format
                BTR_Education(1, Wks) = DateSerial(iYear, iMonth, iDay)
            End If
            
            Dim PTP_Date As String
            
            ' Check to see if the Marine has received the Annual PTP TBI Lecture
            Set StrSearch = .Find(what:="MARINE & LEADER ANNUAL/PTP TBI LECTURE", LookIn:=xlValues, LookAt:=xlWhole)
            If StrSearch Is Nothing Then
                BTR_Education(2, Wks) = "TBI: N/A"
            Else
                PTP_Date = .Range(StrSearch.Address).Offset(0, -1)
                Year = STR_SPLIT(PTP_Date, "/", 1)
                Month = STR_SPLIT(PTP_Date, "/", 2)
                Day = STR_SPLIT(PTP_Date, "/", 3)
                
                ' Convert the PTP Date into an integer
                iYear = CInt(Year)
                iMonth = CInt(Month)
                iDay = CInt(Day)
                
                ' Set the PTP Date Element using the corrected format
                BTR_Education(2, Wks) = "TBI: " & DateSerial(iYear, iMonth, iDay)
            End If
            
            ' Check to see if the Marine has received the Annual PTP PHA-A
            Set StrSearch = .Find(what:="PRE DEPLOYMENT HEALTH ASSESSMENT", LookIn:=xlValues, LookAt:=xlWhole)
            If StrSearch Is Nothing Then
                BTR_Education(2, Wks) = BTR_Education(2, Wks) & " || PHA-A: N/A"
            Else
                PTP_Date = .Range(StrSearch.Address).Offset(0, -1)
                Year = STR_SPLIT(PTP_Date, "/", 1)
                Month = STR_SPLIT(PTP_Date, "/", 2)
                Day = STR_SPLIT(PTP_Date, "/", 3)
                
                ' Convert the PTP Date into an integer
                iYear = CInt(Year)
                iMonth = CInt(Month)
                iDay = CInt(Day)
                
                ' Set the PTP Date Element using the corrected format
                BTR_Education(2, Wks) = BTR_Education(2, Wks) & " || PHA-A: " & DateSerial(iYear, iMonth, iDay)
            End If

            ' Check to see if the Marine has received the Annual PTP PHA-B
            Set StrSearch = .Find(what:="POST DEPLOYMENT HEALTH ASSESSMENT", LookIn:=xlValues, LookAt:=xlWhole)
            If StrSearch Is Nothing Then
                BTR_Education(2, Wks) = BTR_Education(2, Wks) & " || PHA-B: N/A"
            Else
                PTP_Date = .Range(StrSearch.Address).Offset(0, -1)
                Year = STR_SPLIT(PTP_Date, "/", 1)
                Month = STR_SPLIT(PTP_Date, "/", 2)
                Day = STR_SPLIT(PTP_Date, "/", 3)
                
                ' Convert the PTP Date into an integer
                iYear = CInt(Year)
                iMonth = CInt(Month)
                iDay = CInt(Day)
                
                ' Set the PTP Date Element using the corrected format
                BTR_Education(2, Wks) = BTR_Education(2, Wks) & " || PHA-B: " & DateSerial(iYear, iMonth, iDay)
            End If
            
            ' Check to see if the Marine has received the Annual PTP PHA-C
            Set StrSearch = .Find(what:="POST DEPLOYMENT HEALTH RE-ASSESSMENT", LookIn:=xlValues, LookAt:=xlWhole)
            If StrSearch Is Nothing Then
                BTR_Education(2, Wks) = BTR_Education(2, Wks) & " || PHA-C: N/A"
            Else
                PTP_Date = .Range(StrSearch.Address).Offset(0, -1)
                Year = STR_SPLIT(PTP_Date, "/", 1)
                Month = STR_SPLIT(PTP_Date, "/", 2)
                Day = STR_SPLIT(PTP_Date, "/", 3)
                
                ' Convert the PTP Date into an integer
                iYear = CInt(Year)
                iMonth = CInt(Month)
                iDay = CInt(Day)
                
                ' Set the PTP Date Element using the corrected format
                BTR_Education(2, Wks) = BTR_Education(2, Wks) & " || PHA-C: " & DateSerial(iYear, iMonth, iDay)
            End If
            
            ' Check to see if the Marine has completed the NLMB Training (different-- depending on rank)
            Select Case CALC_Grade(BTR_NAMES(0, Wks))
                Case Is < 4: Set StrSearch = .Find(what:="JUNIOR MARINE SUICIDE PREVENTION COURSE", LookIn:=xlValues, LookAt:=xlWhole)
                Case 4 To 5: Set StrSearch = .Find(what:="NCO II SUICIDE PREVENTION COURSE", LookIn:=xlValues, LookAt:=xlWhole)
                Case 6 To 9: Set StrSearch = .Find(what:="SNCO SUICIDE PREVENTION COURSE", LookIn:=xlValues, LookAt:=xlWhole)
                Case Is > 9: Set StrSearch = .Find(what:="OFFICER SUICIDE PREVENTION COURSE", LookIn:=xlValues, LookAt:=xlWhole)
            End Select
            
            If StrSearch Is Nothing Then
                BTR_Education(3, Wks) = "INCOMPLETE"
            Else
                BTR_Education(3, Wks) = .Range(StrSearch.Address).Offset(0, -1)
                Year = STR_SPLIT(BTR_Education(3, Wks), "/", 1)
                Month = STR_SPLIT(BTR_Education(3, Wks), "/", 2)
                Day = STR_SPLIT(BTR_Education(3, Wks), "/", 3)
                
                ' Convert the IAA Date into an integer
                iYear = CInt(Year)
                iMonth = CInt(Month)
                iDay = CInt(Day)
                
                ' Set the IAA Date Element using the corrected format
                BTR_Education(3, Wks) = DateSerial(iYear, iMonth, iDay)
            End If
            
            ' Check to see if the Marine has completed the SAPR Training (there are 2 types: MarineNet & Lecture-- analyse both conditions)
            Set StrSearch = .Find(what:="SAPR ALL HANDS TRAINING PARTIAL (MARINE NET)", LookIn:=xlValues, LookAt:=xlWhole) '<<---- MarineNet
            If StrSearch Is Nothing Then
                BTR_Education(4, Wks) = "MarineNet: INCOMPLETE"
            Else
                BTR_Education(4, Wks) = "MarineNet: "
                Year = STR_SPLIT(.Range(StrSearch.Address).Offset(0, -1), "/", 1)
                Month = STR_SPLIT(.Range(StrSearch.Address).Offset(0, -1), "/", 2)
                Day = STR_SPLIT(.Range(StrSearch.Address).Offset(0, -1), "/", 3)
                
                ' Convert the SAPR Date into an integer
                iYear = CInt(Year)
                iMonth = CInt(Month)
                iDay = CInt(Day)
                
                ' Set the SAPR Date Element using the corrected format
                BTR_Education(4, Wks) = BTR_Education(4, Wks) & DateSerial(iYear, iMonth, iDay)
            End If
            
            Set StrSearch = .Find(what:="SEXUAL ASSAULT PREVENTION AND RESPONSE ANNUAL TRAINING", LookIn:=xlValues, LookAt:=xlWhole) '<<---- Lecture
            If StrSearch Is Nothing Then
                BTR_Education(4, Wks) = BTR_Education(4, Wks) & " || Lecture: INCOMPLETE"
            Else
                BTR_Education(4, Wks) = BTR_Education(4, Wks) & " || Lecture: "
                Year = STR_SPLIT(.Range(StrSearch.Address).Offset(0, -1), "/", 1)
                Month = STR_SPLIT(.Range(StrSearch.Address).Offset(0, -1), "/", 2)
                Day = STR_SPLIT(.Range(StrSearch.Address).Offset(0, -1), "/", 3)
                
                ' Convert the SAPR Date into an integer
                iYear = CInt(Year)
                iMonth = CInt(Month)
                iDay = CInt(Day)
                
                ' Set the SAPR Date Element using the corrected format
                BTR_Education(4, Wks) = BTR_Education(4, Wks) & DateSerial(iYear, iMonth, iDay)
            End If
            
            '===========================================================================================================================
            ' Check to see if the Marine is PME Complete (there are 2 types: MarineNet & Resident Course-- check for either 1/the other)
            '```````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````
            '____________________________________________________________________________________________________________
            ' These are the Descriptions of the Codes (Example of data storage: "Leading Marines (MarineNet) - PA - 2014") _
            '`````````````````````````````````````````````````````````````````````````````````````````````````````````````
            '
            Rem-- If it contains the phrase 'DEP', than it is a MarineNet Course!
            '
            '----------
            ' Juniors |
            '``````````
            '
            ' +     LEADING MARINES DEP
            '
            '------------
            ' Corporals |
            '````````````
            '
            ' +     CORPORALS COURSE DEP
            ' +     CORPORALS COURSE RESIDENT
            '
            '------------
            ' Sergeants |
            '````````````
            '
            ' +     SERGEANTS COURSE DEP
            ' +     SERGEANTS
            '
            '---------
            ' Career |
            '`````````
            '
            ' +     SNCO CAREER COURSE DEP
            ' +     SNCO CAREER
            ' +     SNCO CAREER NONRESIDENT PROGRAM (DCONTD)
            '
            '-----------
            ' Advanced |
            '```````````
            '
            ' +     SNCO ADVANCED COURSE DEP
            ' +     SNCO ADVANCED COURSE
            ' +     SNCO ADVANCED DEPWF NON-RES (8200) (DCONTD)
            '
            '--------
            ' Extra |
            '````````
            '
            ' +     SENIOR ENLISTED JOINT PME (NONRESIDENT)
            '
            '---------------
            ' Marksmanship |
            '```````````````
            '
            ' +     MARCOR COMBAT MARKSMANSHIP COACH
            ' +     COMBAT MARKSMANSHIP TRAINERS
            '
            '_______________________________________________________________________________________________________
            ' Determine the correct PME, based on Rank, and then choose the latest version-- if there are duplicates...
            '```````````````````````````````````````````````````````````````````````````````````````````````````````
            Select Case CALC_Grade(BTR_NAMES(0, Wks))
                Case Is < 4: BTR_Education(5, Wks) = PME_Extract(BTR_Wks, "LEADING MARINES DEP", "", "Junior")
                Case 4: BTR_Education(5, Wks) = PME_Extract(BTR_Wks, "CORPORALS COURSE", " DEP. RESIDENT", "Corporal")
                Case 5: BTR_Education(5, Wks) = PME_Extract(BTR_Wks, "SERGEANTS", " COURSE DEP.", "Sergeant")
                Case 6 To 9: BTR_Education(5, Wks) = PME_Extract(BTR_Wks, "SNCO CAREER.SNCO ADVANCED", " COURSE DEP.| COURSE DEP. COURSE", "SNCO")
                Case Else: Rem-- I'm not tracking PME for Officers...
            End Select
            
            ' Determine if the Marine is a CMC/CMT
            Set StrSearch = .Find(what:="MARCOR COMBAT MARKSMANSHIP COACH", LookIn:=xlValues, LookAt:=xlWhole)
            Rem-- only add a CMC column if the PME exists
            If StrSearch Is Nothing Then
            Else
                If BTR_Education(5, Wks) = "" Then
                    BTR_Education(5, Wks) = "CMC"
                Else: BTR_Education(5, Wks) = BTR_Education(5, Wks) & " || CMC"
                End If
            End If
            Set StrSearch = .Find(what:="COMBAT MARKSMANSHIP TRAINERS", LookIn:=xlValues, LookAt:=xlWhole)
            Rem-- only add a CMT column if the PME exists
            If StrSearch Is Nothing Then
            Else
                If BTR_Education(5, Wks) = "" Then
                    BTR_Education(5, Wks) = "CMT"
                Else: BTR_Education(5, Wks) = BTR_Education(5, Wks) & " || CMT"
                End If
            End If
                        
            ' Capture the BCP Height
            Set StrSearch = .Find(what:="Unit Training", LookIn:=xlValues, LookAt:=xlWhole)
            BTR_BCP(0, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(12, 0).Offset(0, 1), ":", 2)
            BTR_BCP(0, Wks) = Mid(BTR_BCP(0, Wks), 2, 2)
            
            ' Capture the BCP Weight
            BTR_BCP(1, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(13, 0).Offset(0, 1), ":", 2)
            BTR_BCP(1, Wks) = Mid(BTR_BCP(1, Wks), 2, 3)
            
            ' Capture the BCP Body Fat %
            BTR_BCP(2, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(14, 0).Offset(0, 1), ":", 2)
            BTR_BCP(2, Wks) = Mid(BTR_BCP(2, Wks), 2, 2)
            
            ' Capture the Effective Date
            BTR_BCP(3, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(15, 0).Offset(0, 1), ":", 2)
            BTR_BCP(3, Wks) = Mid(BTR_BCP(3, Wks), 2, Len(BTR_BCP(3, Wks)))
            
            Year = STR_SPLIT(BTR_BCP(3, Wks), "-", 1)
            Month = STR_SPLIT(BTR_BCP(3, Wks), "-", 2)
            Day = STR_SPLIT(BTR_BCP(3, Wks), "-", 3)
            
            ' Convert the BCP Date into an integer
            If Year <> "" Then
                iYear = CInt(Year)
                iMonth = CInt(Month)
                iDay = CInt(Day)
                
                ' Set the BCP Date Element using the corrected format
                BTR_PFT(3, Wks) = DateSerial(iYear, iMonth, iDay)
                            
                ' Capture the Standards Flag
                BTR_BCP(4, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(16, 0).Offset(0, 1), ":", 2)
                BTR_BCP(4, Wks) = Mid(BTR_BCP(4, Wks), 2, 1)
            End If
                               
            ' Capture the PFT Date; Store each part of the Date as a string in order to Match the Format with the Database
            Set StrSearch = .Find(what:="Physical Fitness Test", LookIn:=xlValues, LookAt:=xlWhole)
            BTR_PFT(0, Wks) = .Range(StrSearch.Address).Offset(2, 0).Offset(0, 1)
            Year = STR_SPLIT(BTR_PFT(0, Wks), "/", 3)
            Month = STR_SPLIT(BTR_PFT(0, Wks), "/", 1)
            Day = STR_SPLIT(BTR_PFT(0, Wks), "/", 2)
            
            ' Convert the PFT Date into an integer
            If Year <> "" Then
                iYear = CInt(Year)
                iMonth = CInt(Month)
                iDay = CInt(Day)
                
                ' Set the PFT Date Element using the corrected format
                BTR_PFT(0, Wks) = DateSerial(iYear, iMonth, iDay)
                
                ' Capture the PFT Pull-Ups
                BTR_PFT(1, Wks) = BTR_Wks.Range(StrSearch.Address).Offset(2, 0).Offset(0, 5)
                
                ' Capture the PFT Crunches
                BTR_PFT(2, Wks) = BTR_Wks.Range(StrSearch.Address).Offset(2, 0).Offset(0, 6)
                
                ' Capture the PFT Run time
                BTR_PFT(3, Wks) = BTR_Wks.Range(StrSearch.Address).Offset(2, 0).Offset(0, 7)
                
                ' Capture the PFT Score
                BTR_PFT(4, Wks) = BTR_Wks.Range(StrSearch.Address).Offset(2, 0)
                
                ' Capture the PFT Class
                BTR_PFT(5, Wks) = BTR_Wks.Range(StrSearch.Address).Offset(2, 0).Offset(0, 4)
                
                ' Set the PFT Classification Description (based on the Class)
                Select Case BTR_PFT(5, Wks)
                    Case 1: BTR_PFT(6, Wks) = "1ST CLASS"
                    Case 2: BTR_PFT(6, Wks) = "2ND CLASS"
                    Case 3: BTR_PFT(6, Wks) = "3RD CLASS"
                    Case 4: BTR_PFT(6, Wks) = "FAIL/RNT (REQUIRED NOT TAKEN)"
                    Case 5: BTR_PFT(6, Wks) = "MEDICAL"
                    Case 6: BTR_PFT(6, Wks) = "AGE"
                    Case 8: BTR_PFT(6, Wks) = "PARTIAL"
                    Case 9: BTR_PFT(6, Wks) = "EXEMPT"
                End Select
            End If

            ' Capture the CFT Date; Store each part of the Date as a string in order to Match the Format with the Database
            Set StrSearch = .Find(what:="Combat Fitness Test", LookIn:=xlValues, LookAt:=xlWhole)
            BTR_CFT(0, Wks) = .Range(StrSearch.Address).Offset(2, 0).Offset(0, 1)
            Year = STR_SPLIT(BTR_CFT(0, Wks), "/", 3)
            Month = STR_SPLIT(BTR_CFT(0, Wks), "/", 1)
            Day = STR_SPLIT(BTR_CFT(0, Wks), "/", 2)
            
            ' Convert the CFT Date into an integer
            If Year <> "" Then
                iYear = CInt(Year)
                iMonth = CInt(Month)
                iDay = CInt(Day)
                
                ' Set the CFT Date Element using the corrected format
                BTR_CFT(0, Wks) = DateSerial(iYear, iMonth, iDay)
        
                ' Capture the MTC Time
                BTR_CFT(1, Wks) = BTR_Wks.Range(StrSearch.Address).Offset(2, 0).Offset(0, 5)
                
                ' Capture the Ammo Lifts
                BTR_CFT(2, Wks) = BTR_Wks.Range(StrSearch.Address).Offset(2, 0).Offset(0, 6)
                
                ' Capture the MUF Time
                BTR_CFT(3, Wks) = BTR_Wks.Range(StrSearch.Address).Offset(2, 0).Offset(0, 7)
                
                ' Capture the CFT Score
                BTR_CFT(4, Wks) = BTR_Wks.Range(StrSearch.Address).Offset(2, 0)
                
                ' Capture the CFT Class
                BTR_CFT(5, Wks) = BTR_Wks.Range(StrSearch.Address).Offset(2, 0).Offset(0, 4)
                
                ' Set the CFT Classification Description (based on the Class)
                Select Case BTR_CFT(5, Wks)
                    Case 1: BTR_CFT(6, Wks) = "1ST CLASS"
                    Case 2: BTR_CFT(6, Wks) = "2ND CLASS"
                    Case 3: BTR_CFT(6, Wks) = "3RD CLASS"
                    Case 4: BTR_CFT(6, Wks) = "FAIL/RNT (REQUIRED NOT TAKEN)"
                    Case 5: BTR_CFT(6, Wks) = "MEDICAL"
                    Case 6: BTR_CFT(6, Wks) = "AGE"
                    Case 8: BTR_CFT(6, Wks) = "PARTIAL"
                    Case 9: BTR_CFT(6, Wks) = "EXEMPT"
                End Select
            End If
            
            ' Capture the Swim Code
            BTR_Swim(0, Wks) = STR_SPLIT(BTR_Wks.Range("A17"), ":", 2)
            
            ' Capture the Swim Requal Date
            BTR_Swim(1, Wks) = STR_SPLIT(BTR_Wks.Range("A18"), ":", 2)  '<<---- There are no slashes ('/'), so use the Mid Function...
            Year = Mid(BTR_Swim(1, Wks), 2, 4)
            Month = Mid(BTR_Swim(1, Wks), 6, 2)
            Day = 1: Rem-- BTR does not provide a day value for Swim Requal Date; Water Survival Qualification (WSQ) Report does the same thing that I do (day = 01)
            
            ' Convert the Swim Date into an integer
            If Year <> "" Then
                iYear = CInt(Year)
                iMonth = CInt(Month)
                iDay = CInt(Day)
                
                ' Set the Swim Date Element using the corrected format
                BTR_Swim(1, Wks) = DateSerial(iYear, iMonth, iDay)
            End If
            
            ' Capture the MCMAP Date
            Set StrSearch = .Find(what:="Martial Arts Training", LookIn:=xlValues, LookAt:=xlWhole)
            BTR_MCMAP(0, Wks) = BTR_Wks.Range(StrSearch.Address).Offset(2, 0).Offset(0, 1)
            Year = STR_SPLIT(BTR_MCMAP(0, Wks), "/", 1)
            Month = STR_SPLIT(BTR_MCMAP(0, Wks), "/", 2)
            Day = STR_SPLIT(BTR_MCMAP(0, Wks), "/", 3)
            
            ' Convert the MCMAP Date into an integer
            If Year <> "" Then
                iYear = CInt(Year)
                iMonth = CInt(Month)
                iDay = CInt(Day)
                
                ' Set the MCMAP Date Element using the corrected format
                BTR_MCMAP(0, Wks) = DateSerial(iYear, iMonth, iDay)
                
                ' Capture the MCMAP Qual Code
                BTR_MCMAP(1, Wks) = BTR_Wks.Range(StrSearch.Address).Offset(2, 0)
                
                ' Capture the MCMAP Qual Description
                If BTR_Wks.Range(StrSearch.Address).Offset(2, 0).Offset(0, 5) <> "UNQUALIFIED" Then
                    BTR_MCMAP(2, Wks) = BTR_Wks.Range(StrSearch.Address).Offset(2, 0).Offset(0, 5)
                Else: BTR_MCMAP(2, Wks) = "N/A UNQ."
                End If
                
                ' Search for Instructor Qualifications
                Set StrSearch = .Find(what:="BELT INSTRUCTOR", LookIn:=xlValues, LookAt:=xlPart) '<<---- Do NOT look at the whole string... only part of the string, for their are different types of Instructors.
                If StrSearch Is Nothing Then
                    BTR_MCMAP(3, Wks) = ""
                    BTR_MCMAP(4, Wks) = ""
                Else
                    ' Capture the MCMAP [Instuctor Code/Description/Completion Date]
                    BTR_MCMAP(3, Wks) = BTR_Wks.Range(StrSearch.Address).Offset(0, -6) & "    " & _
                                        BTR_Wks.Range(StrSearch.Address) & "   " & BTR_Wks.Range(StrSearch.Address).Offset(0, -4)
                                
                    ' Capture the MCMAP Recert. Date
                    BTR_MCMAP(4, Wks) = BTR_Wks.Range(StrSearch.Address).Offset(0, -2)
                End If
            End If
            
            ' Capture the NBC Date
            Set StrSearch = .Find(what:="GAS CHAMBER", LookIn:=xlValues, LookAt:=xlWhole)
            If StrSearch Is Nothing Then
                BTR_NBC(0, Wks) = "NOT COMPLETE"
            Else: BTR_NBC(0, Wks) = .Range(StrSearch.Address).Offset(0, -1)
            End If
            
            ' Capture the Rifle Date
            Set StrSearch = .Find(what:="SERVICE RIFLE", LookIn:=xlValues, LookAt:=xlWhole)
            BTR_Rifle(0, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(2, 0), ":", 2)
            BTR_Rifle(0, Wks) = Mid(BTR_Rifle(0, Wks), 2, Len(BTR_Rifle(0, Wks)))
            Year = CStr(Mid(BTR_Rifle(0, Wks), 1, 4))
            Month = CStr(Mid(BTR_Rifle(0, Wks), 5, 2))
            Day = CStr(Mid(BTR_Rifle(0, Wks), 7, 2))
            
            ' Convert the Rifle Date into an integer
            If Year <> "" Then
                iYear = CInt(Year)
                iMonth = CInt(Month)
                iDay = CInt(Day)
                
                ' Set the Rifle Date Element using the corrected format
                BTR_Rifle(0, Wks) = DateSerial(iYear, iMonth, iDay)
                
                ' Capture the Table 1 score
                BTR_Rifle(1, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(7, 0).Offset(0, 1), ":", 2)
                BTR_Rifle(1, Wks) = Mid(BTR_Rifle(1, Wks), 3, Len(BTR_Rifle(1, Wks)))
                
                ' Capture the Table 2 score
                BTR_Rifle(2, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(8, 0).Offset(0, 1), ":", 2)
                BTR_Rifle(2, Wks) = Mid(BTR_Rifle(2, Wks), 3, Len(BTR_Rifle(2, Wks)))
                
                ' Determine the Total Rifle Score
                BTR_Rifle(3, Wks) = CInt(BTR_Rifle(1, Wks)) + CInt(BTR_Rifle(2, Wks))
                
                ' Capture the Rifle Class
                BTR_Rifle(4, Wks) = Mid(STR_SPLIT(.Range(StrSearch.Address).Offset(5, 0), ":", 2), 2, 1)
                
                ' Capture the # of Expert Qualifications that this Marine has received
                BTR_Rifle(5, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(10, 0), ":", 2)
                BTR_Rifle(5, Wks) = Mid(BTR_Rifle(5, Wks), 3, Len(BTR_Rifle(5, Wks)))
                
                ' Capture the Expert Exception Code
                BTR_Rifle(6, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(11, 0), ":", 2)
                BTR_Rifle(6, Wks) = Mid(BTR_Rifle(6, Wks), 3, Len(BTR_Rifle(6, Wks)))
                
                ' Capture the Expert Exception Effective Date
                BTR_Rifle(7, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(12, 0), ":", 2)
                BTR_Rifle(7, Wks) = Mid(BTR_Rifle(7, Wks), 3, Len(BTR_Rifle(7, Wks)))
            End If

            ' Capture the Pistol Date
            Set StrSearch = .Find(what:="SERVICE PISTOL", LookIn:=xlValues, LookAt:=xlWhole)
            BTR_Pistol(0, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(2, 0), ":", 2)
            BTR_Pistol(0, Wks) = Mid(BTR_Pistol(0, Wks), 3, Len(BTR_Pistol(0, Wks)))
            Year = CStr(Mid(BTR_Pistol(0, Wks), 1, 4))
            Month = CStr(Mid(BTR_Pistol(0, Wks), 5, 2))
            Day = 1: Rem-- the Pistol Date is not provided...
            
            ' Convert the Pistol Date into an integer
            If Year <> "" Then
                iYear = CInt(Year)
                iMonth = CInt(Month)
                iDay = CInt(Day)
                
                ' Set the Pistol Date Element using the corrected format
                BTR_Pistol(0, Wks) = DateSerial(iYear, iMonth, iDay)
                            
                ' Determine the Total Pistol Score
                BTR_Pistol(1, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(4, 0), ":", 2)
                BTR_Pistol(1, Wks) = Mid(BTR_Pistol(1, Wks), 3, Len(BTR_Pistol(1, Wks)))
                
                ' Capture the Pistol Class
                BTR_Pistol(2, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(5, 0), ":", 2)
                BTR_Pistol(2, Wks) = Mid(BTR_Pistol(2, Wks), 3, Len(BTR_Pistol(2, Wks)))
                
                ' Capture the # of Expert Qualifications that this Marine has received
                BTR_Pistol(3, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(10, 0), ":", 2)
                BTR_Pistol(3, Wks) = Mid(BTR_Pistol(3, Wks), 3, Len(BTR_Pistol(3, Wks)))
                
                ' Capture the Expert Exception Code
                BTR_Pistol(4, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(11, 0), ":", 2)
                BTR_Pistol(4, Wks) = Mid(BTR_Pistol(4, Wks), 3, Len(BTR_Pistol(4, Wks)))
                
                ' Capture the Expert Exception Effective Date
                BTR_Pistol(5, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(12, 0), ":", 2)
                BTR_Pistol(5, Wks) = Mid(BTR_Pistol(5, Wks), 3, Len(BTR_Pistol(5, Wks)))
            End If
            
            ' Current Iteration of [For Each... Loop] is now complete; increment counter by 1
            Wks = Wks + 1
        End With
    Next
    
    ' Fill in the Database with all the collected values from the BTR
    Last_Cell = BTR_Count
    
    
    ' MARINE-NET DATA ...
    For i = 0 To Last_Cell: Database.Range("G" & i + 3).Formula = BIR_Personnel(2, i): Next i
    For i = 0 To Last_Cell: Database.Range("H" & i + 3).Formula = BIR_Personnel(3, i): Next i
    For i = 0 To Last_Cell: Database.Range("I" & i + 3).Formula = BIR_Personnel(4, i): Next i
    For i = 0 To Last_Cell: Database.Range("J" & i + 3).Formula = BIR_Personnel(5, i): Next i
    Erase BIR_Personnel()
    For i = 0 To Last_Cell: Database.Range("K" & i + 3).Formula = BIR_Duty(0, i): Next i
    For i = 0 To Last_Cell: Database.Range("L" & i + 3).Formula = BIR_Duty(1, i): Next i
    For i = 0 To Last_Cell: Database.Range("M" & i + 3).Formula = BIR_Duty(2, i): Next i
    For i = 0 To Last_Cell: Database.Range("N" & i + 3).Formula = BIR_Duty(3, i): Next i
    For i = 0 To Last_Cell: Database.Range("O" & i + 3).Formula = BIR_Duty(4, i): Next i
    For i = 0 To Last_Cell: Database.Range("P" & i + 3).Formula = BIR_Duty(5, i): Next i
    For i = 0 To Last_Cell: Database.Range("Q" & i + 3).Formula = BIR_Duty(6, i): Next i
    Erase BIR_Duty()
    For i = 0 To Last_Cell: Database.Range("R" & i + 3).Formula = BIR_Contract(0, i): Next i
    For i = 0 To Last_Cell: Database.Range("S" & i + 3).Formula = BIR_Contract(1, i): Next i
    For i = 0 To Last_Cell: Database.Range("T" & i + 3).Formula = BIR_Contract(2, i): Next i
    For i = 0 To Last_Cell: Database.Range("U" & i + 3).Formula = BIR_Contract(3, i): Next i
    For i = 0 To Last_Cell: Database.Range("V" & i + 3).Formula = BIR_Contract(4, i): Next i
    
    
    ' REGULAR DATA ...
    For i = 0 To Last_Cell: Database.Range("B" & i + 3).Formula = BTR_NAMES(0, i): Next i
    For i = 0 To Last_Cell: Database.Range("C" & i + 3).Formula = BTR_NAMES(1, i): Next i
    Erase BTR_NAMES()
    For i = 0 To Last_Cell: Database.Range("AH" & i + 3).Formula = BTR_Security(0, i): Next i
    Erase BTR_Security()
    For i = 0 To Last_Cell: Database.Range("AI" & i + 3).Formula = BTR_Education(0, i): Next i
    For i = 0 To Last_Cell: Database.Range("AJ" & i + 3).Formula = BTR_Education(1, i): Next i
    For i = 0 To Last_Cell: Database.Range("AK" & i + 3).Formula = BTR_Education(2, i): Next i
    For i = 0 To Last_Cell: Database.Range("AL" & i + 3).Formula = BTR_Education(3, i): Next i
    For i = 0 To Last_Cell: Database.Range("AM" & i + 3).Formula = BTR_Education(4, i): Next i
    For i = 0 To Last_Cell: Database.Range("AO" & i + 3).Formula = BTR_Education(5, i): Next i
    Erase BTR_Education()
    For i = 0 To Last_Cell: Database.Range("AP" & i + 3).Formula = BTR_BCP(0, i): Next i
    For i = 0 To Last_Cell: Database.Range("AQ" & i + 3).Formula = BTR_BCP(1, i): Next i
    For i = 0 To Last_Cell: Database.Range("AR" & i + 3).Formula = BTR_BCP(2, i): Next i
    For i = 0 To Last_Cell: Database.Range("AS" & i + 3).Formula = BTR_BCP(3, i): Next i
    For i = 0 To Last_Cell: Database.Range("AT" & i + 3).Formula = BTR_BCP(4, i): Next i
    Erase BTR_BCP()
    For i = 0 To Last_Cell: Database.Range("AU" & i + 3).Formula = BTR_PFT(0, i): Next i
    For i = 0 To Last_Cell: Database.Range("AV" & i + 3).Formula = BTR_PFT(1, i): Next i
    For i = 0 To Last_Cell: Database.Range("AW" & i + 3).Formula = BTR_PFT(2, i): Next i
    For i = 0 To Last_Cell: Database.Range("AX" & i + 3).Formula = BTR_PFT(3, i): Next i
    For i = 0 To Last_Cell: Database.Range("AY" & i + 3).Formula = BTR_PFT(4, i): Next i
    For i = 0 To Last_Cell: Database.Range("AZ" & i + 3).Formula = BTR_PFT(5, i): Next i
    For i = 0 To Last_Cell: Database.Range("BA" & i + 3).Formula = BTR_PFT(6, i): Next i
    Erase BTR_PFT()
    For i = 0 To Last_Cell: Database.Range("BB" & i + 3).Formula = BTR_CFT(0, i): Next i
    For i = 0 To Last_Cell: Database.Range("BC" & i + 3).Formula = BTR_CFT(1, i): Next i
    For i = 0 To Last_Cell: Database.Range("BD" & i + 3).Formula = BTR_CFT(2, i): Next i
    For i = 0 To Last_Cell: Database.Range("BE" & i + 3).Formula = BTR_CFT(3, i): Next i
    For i = 0 To Last_Cell: Database.Range("BF" & i + 3).Formula = BTR_CFT(4, i): Next i
    For i = 0 To Last_Cell: Database.Range("BG" & i + 3).Formula = BTR_CFT(5, i): Next i
    For i = 0 To Last_Cell: Database.Range("BH" & i + 3).Formula = BTR_CFT(6, i): Next
    Erase BTR_CFT()
    For i = 0 To Last_Cell: Database.Range("BI" & i + 3).Formula = BTR_Swim(0, i): Next i
    For i = 0 To Last_Cell: Database.Range("BJ" & i + 3).Formula = BTR_Swim(1, i): Next i
    Erase BTR_Swim()
    For i = 0 To Last_Cell: Database.Range("BK" & i + 3).Formula = BTR_MCMAP(0, i): Next i
    For i = 0 To Last_Cell: Database.Range("BL" & i + 3).Formula = BTR_MCMAP(1, i): Next i
    For i = 0 To Last_Cell: Database.Range("BM" & i + 3).Formula = STR_SPLIT(BTR_MCMAP(2, i), " ", 1): Next i '<<---- First Word in the String
    For i = 0 To Last_Cell: Database.Range("BN" & i + 3).Formula = STR_SPLIT(BTR_MCMAP(2, i), " ", 2): Next i '<<---- Second Word in the String
    For i = 0 To Last_Cell: Database.Range("BO" & i + 3).Formula = BTR_MCMAP(3, i): Next i
    For i = 0 To Last_Cell: Database.Range("BP" & i + 3).Formula = BTR_MCMAP(4, i): Next i
    Erase BTR_MCMAP()
    For i = 0 To Last_Cell: Database.Range("BQ" & i + 3).Formula = BTR_NBC(0, i): Next i
    Erase BTR_NBC()
    For i = 0 To Last_Cell: Database.Range("BR" & i + 3).Formula = BTR_Rifle(0, i): Next i
    For i = 0 To Last_Cell: Database.Range("BS" & i + 3).Formula = BTR_Rifle(1, i): Next i
    For i = 0 To Last_Cell: Database.Range("BT" & i + 3).Formula = BTR_Rifle(2, i): Next i
    For i = 0 To Last_Cell: Database.Range("BU" & i + 3).Formula = BTR_Rifle(3, i): Next i
    For i = 0 To Last_Cell: Database.Range("BV" & i + 3).Formula = BTR_Rifle(4, i): Next i
    For i = 0 To Last_Cell: Database.Range("BW" & i + 3).Formula = BTR_Rifle(5, i): Next i
    For i = 0 To Last_Cell: Database.Range("BX" & i + 3).Formula = BTR_Rifle(6, i): Next i
    For i = 0 To Last_Cell: Database.Range("BY" & i + 3).Formula = BTR_Rifle(7, i): Next i
    Erase BTR_Rifle()
    For i = 0 To Last_Cell: Database.Range("BZ" & i + 3).Formula = BTR_Pistol(0, i): Next i
    For i = 0 To Last_Cell: Database.Range("CA" & i + 3).Formula = BTR_Pistol(1, i): Next i
    For i = 0 To Last_Cell: Database.Range("CB" & i + 3).Formula = BTR_Pistol(2, i): Next i
    For i = 0 To Last_Cell: Database.Range("CC" & i + 3).Formula = BTR_Pistol(3, i): Next i
    For i = 0 To Last_Cell: Database.Range("CD" & i + 3).Formula = BTR_Pistol(4, i): Next i
    For i = 0 To Last_Cell: Database.Range("CE" & i + 3).Formula = BTR_Pistol(5, i): Next i
    Erase BTR_Pistol()
    
    
    '-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    ' Gather all necassary information from the Alpha Roster...
    ' NOTE: The BTR Workbook will remain open, in order to verify that no extra records are added from the Alpha Roster; then, it will be closed.
    '       ==>> Yes, they both contain different records-- I checked:
    '                +  BTR: All Marines attached to Unit
    '                +  Alpha: ALL attached to Unit (almost-- depending on whether IPAC transferred a Marine's records properly in 3270), including Marines that may no longer be here, Navy Personnel, or Civilean contractors.
    '
    '       * Since this shell will only be processing information for Marines that are attached to this unit, I don't need those extra records.
    '===================================================================================================
    '   The Alpha Roster must be modified to include the EDIPI #'s first!  Follow the instuctions below:
    '===================================================================================================
    '   1.)  Login to: https://www.mol.usmc.mil/
    '   2.)  Click on the Reports Menu; next, select the Unit Report radio button
    '   3.)  Click on "Launch", and select "Report Studio" in the drop-down menu
    '   4.)  Open Report Studio with the "TFAS Report Authoring" Package
    '   5.)  Select the "Open Existing" Option {or, "File" (on the menu bar) >> "Open" (File Command)}
    '   6.)  Navigate through the directories like so: "Cognos" > Public Folders >> Reports >> Rosters >> Alpha Roster (Open This File)
    '   7.)  Highlight the Platoon Column by selecting the "Company- Platoon- Work Section" Header
    '   8.)  Now, select the plus sign (+) next to the "TFAS Ad Hoc" object within the "Source" panel
    '   9.)  Navigate through the directories like so: "TFAS Ad Hoc" > Individual Marine >> Electronic Intrchg Person Id (Double Click)
    '  10.)  Select "File" (on the menu bar) >> "Save As..." (File Command)
    '  11.)  On the "Save As..." Prompt, Select the "My Folders" icon on the left-most panel
    '  12.)  Save the name as: "Alpha Roster_EDIPI" (or anything similar to distinguish that this file has been modified with more data)
    '  13.)  Then, click "File" >> "Exit"
    '  14.)  Open the "My Folders" Tab under the "Marine Online" header in the red banner
    '  15.)  Select the Alpha Roster you just created.
    '  16.)  Do not select any Filters on the next prompt; just select "Finish"
    '  17.)  Click on the "View in HTML Format" icon (a graphic of a file embellished with a red bar on top, and a globe below)
    '  18.)  Click on "View in Excel Options" >> "View in Excel 2007 Format"
    '  19.)  Save the file in your "Downloads" directory, or whichever folder you usually download files to
    '  20.)  That's it!!  When time goes by and you need a more updated Roster, you have a saved template under "My Folders" within the Report Bridge of MOL.  Simply repeat steps 14-19.
    '-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    Dim Boost As Integer, Element As Integer
    Set Roster_Wkb = Workbooks.Open(Alpha_Roster)
    Set Roster_Wks = Roster_Wkb.Sheets("Page1_1")
    
    Last_Cell = Roster_Wks.Range("B6").End(xlDown).Row
    ReDim EDIPI_Rst(BTR_Wkb.Sheets.Count)
    ReDim DCTB_Rst(BTR_Wkb.Sheets.Count)
    ReDim Unit_Rst(BTR_Wkb.Sheets.Count)
    ReDim Shop_Rst(BTR_Wkb.Sheets.Count)
    
    ' Capture data in each Column and store it inside an Array from the Alpha Roster
    Boost = 0
    Dim Name_Rng As Range, iRow As Integer
    Set Name_Rng = Database.Range("NAMES")
    With Name_Rng
        iRow = 3
        Dim Anomoly As Boolean
        Anomoly = False
        
        ' For Debugging Purposes Only!
        '>>Dim n As Integer, Difference As Integer
        '>>n = FreeFile()
        '>>Open "C:\Users\1404656640A\Documents\debug-log.txt" For Output As #n '<<------- Use local file if the share drive is down
        '>>Open "M:\(2) ACE\VMM-162\S-3\Testing\debug-log.txt" For Output As #n '<<---- This is the address of my Database Project
        
        For i = 6 To Last_Cell
            '>>Debug.Assert (i <> 176)
            Anomoly = False
            On Error Resume Next
            Set StrSearch = .Find(Mid(Roster_Wks.Range("A" & i), 1, Len(Roster_Wks.Range("A" & i)) - 1))
            '>>Difference = Abs(StrSearch.Row - iRow)
            
            ' Send Debug Output to the Immediate Window
            '>>Debug.Print "===================================================="
            '>>Debug.Print "Record # " & i - 5
            '>>Debug.Print "----------------------------------------------------"
            '>>Debug.Print "Database Address: " & vbTab & vbTab & vbTab & "Database.Range(" & Chr(34) & "C" & i - 3 & Chr(34) & ")"
            '>>Debug.Print "Alpha Roster Address: "; vbTab & "Roster_Wks.Range(" & Chr(34) & "B" & i & Chr(34) & ")"
            '>>Debug.Print "Difference = " & vbTab & vbTab & Difference
            '>>Debug.Print "Array Element =" & vbTab & vbTab & i - 6 - Boost
            '>>Debug.Print "----------------------------------------------------"
            '>>Debug.Print "Marine Searched =" & vbTab & Roster_Wks.Range("B" & i)
            '>>Debug.Print "Actual Reference =" & vbTab & Database.Range("C" & (i - 6 - Boost) + 3)
            '>>Debug.Print "Error Code = " & vbTab & vbTab & Err
            '>>Debug.Print "===================================================="
            '>>Debug.Print "                      /\"
            '>>Debug.Print "                      \/"
            
            ' Send Debug Output to debug-log.txt
            '>>Print #n, "===================================================="
            '>>Print #n, "Record # " & i - 5
            '>>Print #n, "----------------------------------------------------"
            '>>Print #n, "Database Address:" & vbTab; "  Database.Range(" & Chr(34) & "C" & i - 3 & Chr(34) & ")"
            '>>Print #n, "Alpha Roster Address: "; vbTab & "Roster_Wks.Range(" & Chr(34) & "B" & i & Chr(34) & ")"
            '>>Print #n, "Difference = " & vbTab & vbTab & Difference
            '>>Print #n, "Array Element =" & vbTab & vbTab & i - 6 - Boost
            '>>Print #n, "----------------------------------------------------"
            '>>Print #n, "Marine Searched =" & vbTab & Roster_Wks.Range("B" & i)
            '>>Print #n, "Actual Reference =" & vbTab & Database.Range("C" & (i - 6 - Boost) + 3)
            '>>Print #n, "===================================================="
            '>>Print #n, "                       /\"
            '>>Print #n, "                       \/"
            
            ' Match the First Name from the current record in the Alpha Roster, with the First Name of the Referenced Record in the Database (Last Name is too tricky; i.e. Honorifics)
            If STR_SPLIT(Roster_Wks.Range("A" & i), ",", 2) = STR_SPLIT(Database.Range("C" & (i - 6 - Boost) + 3), ",", 2) & " " Then
                iRow = iRow + 1
                
                ' No Middle Initial
                ElseIf STR_SPLIT(Roster_Wks.Range("A" & i), ",", 2) & "-." = STR_SPLIT(Database.Range("C" & (i - 6 - Boost) + 3), ",", 2) Then
                    iRow = iRow + 1
                    
                    ' Honorifics
                    ElseIf STR_SPLIT(STR_SPLIT(Roster_Wks.Range("A" & i), " ", 2), ",", 2) = STR_SPLIT(STR_SPLIT(Database.Range("C" & (i - 6 - Boost) + 3), " ", 2), ",", 1) Then
                        If Err = 91 Then
                            Err = 0
                            iRow = iRow + 1
                            
                            ' If the Search does not match the Reference, then: Record exists in BTR-- not in Alpha Roster.  Make a note of the anomoly.
                            ElseIf Err = 9 Then
                                Err = 0
                                Anomoly = True
                                iRow = iRow + 1
                        End If
            End If
            
            ' If the name on the Alpha Roster matches what's in the Database, then begin filling the arrays _
             (i.e. if the Search String is empty, there was no match; thus, keep processing the string for difference conditions)
            If StrSearch Is Nothing Then
                
                ' Test to see if there is a middle initial or not
                If Mid(STR_SPLIT(Roster_Wks.Range("A" & i), ",", 2), Len(STR_SPLIT(Roster_Wks.Range("A" & i), ",", 2)) - 1, Len(STR_SPLIT(Roster_Wks.Range("A" & i), ",", 2))) <> ". " Then
                    StrFound = STR_SPLIT(Roster_Wks.Range("A" & i), ",", 1) & "," & STR_SPLIT(Roster_Wks.Range("A" & i), ",", 2) & "-."
                    Set StrSearch = .Find(StrFound)
                    If StrSearch Is Nothing Then
                        ' Test to see if there is an honorific attached to the name like the suffix: "JR", "SR", "I", "II", etc.
                        If STR_SPLIT(Roster_Wks.Range("A" & i), " ", 2) <> "" Then
                            StrFound = STR_SPLIT(Roster_Wks.Range("A" & i), " ", 1) & STR_SPLIT(STR_SPLIT(Roster_Wks.Range("A" & i), " ", 2), ",", 1) & ", " & STR_SPLIT(STR_SPLIT(Roster_Wks.Range("A" & i), " ", 2), ",", 2) & " -."
                            
                            ' Grab the Middle Initial and append it to the string
                            '>>Mid(STR_SPLIT(Roster_Wks.Range("B" & i), ",", 2), Len(STR_SPLIT(Roster_Wks.Range("B" & i), ",", 2)) - 2, Len(STR_SPLIT(Roster_Wks.Range("B" & i), ",", 2)))
                            If Err = 9 Then
                                Err = 0
                                Boost = Boost + 1
                            Else
                                Set StrSearch = .Find(StrFound)
                                    If StrSearch Is Nothing Then
                                        Boost = Boost + 1
                                    Else
                                        If Anomoly = False Then
                                            EDIPI_Rst(i - 6 - Boost) = Roster_Wks.Range("C" & i)
                                            DCTB_Rst(i - 6 - Boost) = Roster_Wks.Range("K" & i)
                                            Unit_Rst(i - 6 - Boost) = Roster_Wks.Range("I" & i)
                                            Shop_Rst(i - 6 - Boost) = Roster_Wks.Range("D" & i)
                                        Else
                                            EDIPI_Rst(i - 6 - Boost) = "-- N/A --"
                                            DCTB_Rst(i - 6 - Boost) = "-- N/A --"
                                            Unit_Rst(i - 6 - Boost) = "----- NOT IN ALPHA ROSTER -----"
                                            Shop_Rst(i - 6 - Boost) = "-- N/A --"
                                            EDIPI_Rst((i + 1) - 6 - Boost) = Roster_Wks.Range("C" & i)
                                            DCTB_Rst((i + 1) - 6 - Boost) = Roster_Wks.Range("K" & i)
                                            Unit_Rst((i + 1) - 6 - Boost) = Roster_Wks.Range("I" & i)
                                            Shop_Rst((i + 1) - 6 - Boost) = Roster_Wks.Range("D" & i)
                                            Boost = Boost - 1
                                        End If
                                    End If
                            End If
                        End If
                    Else
                        If Anomoly = False Then
                            EDIPI_Rst(i - 6 - Boost) = Roster_Wks.Range("C" & i)
                            DCTB_Rst(i - 6 - Boost) = Roster_Wks.Range("K" & i)
                            Unit_Rst(i - 6 - Boost) = Roster_Wks.Range("I" & i)
                            Shop_Rst(i - 6 - Boost) = Roster_Wks.Range("D" & i)
                        Else
                            EDIPI_Rst(i - 6 - Boost) = "-- N/A --"
                            DCTB_Rst(i - 6 - Boost) = "-- N/A --"
                            Unit_Rst(i - 6 - Boost) = "----- NOT IN ALPHA ROSTER -----"
                            Shop_Rst(i - 6 - Boost) = "-- N/A --"
                            EDIPI_Rst((i + 1) - 6 - Boost) = Roster_Wks.Range("C" & i)
                            DCTB_Rst((i + 1) - 6 - Boost) = Roster_Wks.Range("K" & i)
                            Unit_Rst((i + 1) - 6 - Boost) = Roster_Wks.Range("I" & i)
                            Shop_Rst((i + 1) - 6 - Boost) = Roster_Wks.Range("D" & i)
                            Boost = Boost - 1
                        End If
                    End If
                    
                    ' Test to see if there is an honorific attached to the name like the suffix: "JR", "SR", "I", "II", etc.
                    ElseIf STR_SPLIT(STR_SPLIT(Roster_Wks.Range("A" & i), " ", 2), ",", 1) <> "" Then
                        StrFound = STR_SPLIT(Roster_Wks.Range("A" & i), " ", 1) & STR_SPLIT(STR_SPLIT(Roster_Wks.Range("A" & i), " ", 2), ",", 1) & ", " & STR_SPLIT(STR_SPLIT(Roster_Wks.Range("A" & i), " ", 2), ",", 2) & " " & Mid(STR_SPLIT(Roster_Wks.Range("A" & i), ",", 2), Len(STR_SPLIT(Roster_Wks.Range("A" & i), ",", 2)) - 2, Len(STR_SPLIT(Roster_Wks.Range("A" & i), ",", 2)))
                        StrFound = Mid(StrFound, 1, Len(StrFound) - 1)
                        If Err = 9 Then
                            Err = 0
                            Boost = Boost + 1
                        Else
                            Set StrSearch = .Find(StrFound)
                                If StrSearch Is Nothing Then
                                    Boost = Boost + 1
                                Else
                                    If Anomoly = False Then
                                        EDIPI_Rst(i - 6 - Boost) = Roster_Wks.Range("C" & i)
                                        DCTB_Rst(i - 6 - Boost) = Roster_Wks.Range("K" & i)
                                        Unit_Rst(i - 6 - Boost) = Roster_Wks.Range("I" & i)
                                        Shop_Rst(i - 6 - Boost) = Roster_Wks.Range("D" & i)
                                    Else
                                        EDIPI_Rst(i - 6 - Boost) = "-- N/A --"
                                        DCTB_Rst(i - 6 - Boost) = "-- N/A --"
                                        Unit_Rst(i - 6 - Boost) = "----- NOT IN ALPHA ROSTER -----"
                                        Shop_Rst(i - 6 - Boost) = "-- N/A --"
                                        EDIPI_Rst((i + 1) - 6 - Boost) = Roster_Wks.Range("C" & i)
                                        DCTB_Rst((i + 1) - 6 - Boost) = Roster_Wks.Range("K" & i)
                                        Unit_Rst((i + 1) - 6 - Boost) = Roster_Wks.Range("I" & i)
                                        Shop_Rst((i + 1) - 6 - Boost) = Roster_Wks.Range("D" & i)
                                        Boost = Boost - 1
                                    End If
                                End If
                        End If
                End If
            Else
                ' Add boolean value to check whether the Data in the Alpha Roster matches the Data in BTR
                If Anomoly = False Then
                    EDIPI_Rst(i - 6 - Boost) = Roster_Wks.Range("C" & i)
                    DCTB_Rst(i - 6 - Boost) = Roster_Wks.Range("K" & i)
                    Unit_Rst(i - 6 - Boost) = Roster_Wks.Range("I" & i)
                    Shop_Rst(i - 6 - Boost) = Roster_Wks.Range("D" & i)
                Else
                    EDIPI_Rst(i - 6 - Boost) = "-- N/A --"
                    DCTB_Rst(i - 6 - Boost) = "-- N/A --"
                    Unit_Rst(i - 6 - Boost) = "----- NOT IN ALPHA ROSTER -----"
                    Shop_Rst(i - 6 - Boost) = "-- N/A --"
                    EDIPI_Rst((i + 1) - 6 - Boost) = Roster_Wks.Range("C" & i)
                    DCTB_Rst((i + 1) - 6 - Boost) = Roster_Wks.Range("K" & i)
                    Unit_Rst((i + 1) - 6 - Boost) = Roster_Wks.Range("I" & i)
                    Shop_Rst((i + 1) - 6 - Boost) = Roster_Wks.Range("D" & i)
                    Boost = Boost - 1
                End If
            End If
Next_On_Roster:
        Next i
    End With
    
    ' Close the Debug Log
    '>>Close #n
    
    ' Fill the Database with all the EDIPI's
    Last_Cell = BTR_Wkb.Sheets.Count
    For i = 0 To Last_Cell: Database.Range("D" & i + 3).Formula = EDIPI_Rst(i): Next i
    Erase EDIPI_Rst()
        
    ' Fill the Database with all the Join Dates
    For i = 0 To Last_Cell: Database.Range("Z" & i + 3).Formula = DCTB_Rst(i): Next i
    Erase DCTB_Rst()
    
    ' Fill the Database with all the Unit Names
    For i = 0 To Last_Cell: Database.Range("AA" & i + 3).Formula = Unit_Rst(i): Next i
    Erase Unit_Rst()
    
    ' Fill the Database with all the Shops/Work Section
    For i = 0 To Last_Cell: Database.Range("AB" & i + 3).Formula = Shop_Rst(i): Next i
    Erase Shop_Rst()
        
    ' Close the Roster
    Roster_Wkb.Close
    
    ' Close the BTR Workbook
    BTR_Wkb.Close
    
    '----------------------------------------------------------
    ' Gather all necassary information from the BIR Workbook...
    '----------------------------------------------------------
    Set BIR_Wkb = Workbooks.Open(BIR)
    
    '-------------------------
    ' CONTINUE CODING HERE ...
    '-------------------------
    '
    '******************************************************************************************************
    ' CODES: SEX, DOB, E-MAIL, CIV ED LEVEL, CERT, MAJOR, [DUTY STATUS], [SERVICE], PME_COMPLETE
    '******************************************************************************************************

    ReDim BIR_Personnel(5, BTR_Count)
    ReDim BIR_Duty(6, BTR_Count)
    ReDim BIR_Contract(6, BTR_Count)
    ReDim BIR_Service(5, BTR_Count)
    ReDim BIR_PME_COMP(0, BTR_Count)
    
    ' Alter the Range Addresses for the Database to hold the correct amount of records counted from the BTR
    Redefine_Database = True
    If Database_Empty = True Then: Call Named_Ranges
    
    Wks = 0
    For Each WS In BIR_Wkb.Sheets
        
        ' Set the Address for the current page in the Workbook
        Page_Num = "Page1_" & Wks + 1
        Set BIR_Wks = BIR_Wkb.Sheets(Page_Num)
        
        With BIR_Wks.Cells
                        
            ' Capture the Gender of the Marine
            Set StrSearch = .Find(what:="Personal Information", LookIn:=xlValues, LookAt:=xlWhole)
            BIR_Personnel(0, Wks) = .Range(StrSearch.Address).Offset(7, 0).Offset(0, 1)
            BIR_Personnel(0, Wks) = STR_SPLIT(BIR_Personnel(0, Wks), ":", 3)
            BIR_Personnel(0, Wks) = Mid(BIR_Personnel(0, Wks), 3, 1)
            
            ' Capture the DOB of the Marine
            BIR_Personnel(1, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(1, 0), ":", 2)
            BIR_Personnel(1, Wks) = Mid(BIR_Personnel(1, Wks), 3, Len(BIR_Personnel(1, Wks))) '<<---- This Method will be used alot since many items contain 2 leading spaces
            Year = Mid(BIR_Personnel(1, Wks), 1, 4)
            Month = Mid(BIR_Personnel(1, Wks), 5, 2)
            Day = Mid(BIR_Personnel(1, Wks), 7, 2)
            
            ' Convert the DOB into an integer
            iYear = CInt(Year)
            iMonth = CInt(Month)
            iDay = CInt(Day)
            
            ' Set the DOB element using the corrected format
            BIR_Personnel(1, Wks) = DateSerial(iYear, iMonth, iDay)
            
            ' Capture the Marines' Bonus PEF
            BIR_Contract(5, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(8, 0).Offset(0, 1), ":", 2)
            BIR_Contract(5, Wks) = Mid(BIR_Contract(5, Wks), 3, Len(BIR_Contract(5, Wks)))
            
            ' Capture the Marines' College PEF
            BIR_Contract(6, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(9, 0).Offset(0, 1), ":", 2)
            BIR_Contract(6, Wks) = Mid(BIR_Contract(6, Wks), 3, Len(BIR_Contract(6, Wks)))
            
            ' Capture the Date of Rank (DOR) of the Marine
            Set StrSearch = .Find(what:="Service Information", LookIn:=xlValues, LookAt:=xlWhole)
            BIR_Service(0, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(2, 0).Offset(0, 1), ":", 2)
            BIR_Service(0, Wks) = Mid(BIR_Service(0, Wks), 3, Len(BIR_Service(0, Wks)))
            Year = Mid(BIR_Service(0, Wks), 1, 4)
            Month = Mid(BIR_Service(0, Wks), 5, 2)
            Day = Mid(BIR_Service(0, Wks), 7, 2)
            
            ' Convert the DOR into an integer
            iYear = CInt(Year)
            iMonth = CInt(Month)
            iDay = CInt(Day)
                                   
            ' Set the DOR element using the corrected format
            BIR_Service(0, Wks) = DateSerial(iYear, iMonth, iDay)
            
            ' Caputure the Billet of the Marine
            BIR_Service(1, Wks) = STR_SPLIT(.Range("A6"), ":", 2)
            BIR_Service(1, Wks) = Mid(BIR_Service(1, Wks), 3, Len(BIR_Service(1, Wks)))
            
            ' Capture the PMOS of the Marine
            BIR_Service(2, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(22, 0), ":", 2)
            BIR_Service(2, Wks) = Mid(BIR_Service(2, Wks), 3, Len(BIR_Service(2, Wks)))

            ' Capture the BMOS of the Marine
            BIR_Service(3, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(23, 0), ":", 2)
            BIR_Service(3, Wks) = Mid(BIR_Service(3, Wks), 3, Len(BIR_Service(3, Wks)))
            
            ' Capture the AMOS1 of the Marine
            BIR_Service(4, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(23, 0).Offset(0, 1), ":", 2)
            BIR_Service(4, Wks) = Mid(BIR_Service(4, Wks), 3, Len(BIR_Service(4, Wks)))
            
            ' Capture the AMOS2 of the Marine
            BIR_Service(5, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(24, 0).Offset(0, 1), ":", 2)
            BIR_Service(5, Wks) = Mid(BIR_Service(5, Wks), 3, Len(BIR_Service(5, Wks)))
            
            ' Capture the PME_Complete Code of the Marine
            BIR_PME_COMP(0, Wks) = STR_SPLIT(.Range(StrSearch.Address).Offset(1, 0), ":", 2)
            BIR_PME_COMP(0, Wks) = Mid(BIR_PME_COMP(0, Wks), 3, Len(BIR_PME_COMP(0, Wks)))
            
            ' Current Iteration of [For Each... Loop] is now complete; increment counter by 1
            Wks = Wks + 1
        End With
    Next
    
    ' Close the BIR Workbook
    BIR_Wkb.Close
    
    ' Fill in the Database with all the collected values from the BIR
    Last_Cell = BTR_Count
    For i = 0 To Last_Cell: Database.Range("E" & i + 3).Formula = BIR_Personnel(0, i): Next i
    For i = 0 To Last_Cell: Database.Range("F" & i + 3).Formula = BIR_Personnel(1, i): Next i
    Erase BIR_Personnel()
    Erase BIR_Duty()
    For i = 0 To Last_Cell: Database.Range("W" & i + 3).Formula = BIR_Contract(5, i): Next i
    For i = 0 To Last_Cell: Database.Range("X" & i + 3).Formula = BIR_Contract(6, i): Next i
    Erase BIR_Contract()
    For i = 0 To Last_Cell: Database.Range("Y" & i + 3).Formula = BIR_Service(0, i): Next i
    For i = 0 To Last_Cell: Database.Range("AC" & i + 3).Formula = BIR_Service(1, i): Next i
    For i = 0 To Last_Cell: Database.Range("AD" & i + 3).Formula = BIR_Service(2, i): Next i
    For i = 0 To Last_Cell: Database.Range("AE" & i + 3).Formula = BIR_Service(3, i): Next i
    For i = 0 To Last_Cell: Database.Range("AF" & i + 3).Formula = BIR_Service(4, i): Next i
    For i = 0 To Last_Cell: Database.Range("AG" & i + 3).Formula = BIR_Service(5, i): Next i
    Erase BIR_Service()
    For i = 0 To Last_Cell: Database.Range("AN" & i + 3).Formula = BIR_PME_COMP(0, i): Next i
    Erase BIR_PME_COMP()
'=============================================================================================================
   
    ' Enumerate Column A
    Last_Row = Database.Range("B3").End(xlDown).Row + 1
    Database.Activate
    Database.Range("A3") = 1
    Database.Range("A4") = 2
    Database.Range("A3:A4").Select
    Selection.AutoFill Destination:=Range("A3:A" & Last_Row - 1), Type:=xlFillDefault
    
    ' Re-format the entire spreadsheet
    Database.Range("A3:AI" & Last_Row - 1).RowHeight = 15
    Database.Range("A3:AI" & Last_Row - 1).Font.Size = 12
    Dim Checker As Integer
    For Checker = 0 To Last_Row_OLD - Last_Row: Database.Rows(Last_Row).Delete: Next Checker
    Application.DisplayAlerts = False
    Program.Save
    Application.DisplayAlerts = True

    ' After code runs, toggle options back to their original state
    Call Restore
    
    ' Announce to the user that the update is complete
    Dim Msg As String, Response As String
    
    ' Prompt the User to get the Alpha Roster, before continuing
    Msg = "The Database Update Procedure is complete!  You may now continue your session."
    Response = MsgBox(Msg, vbOKOnly & vbInformation, "Update Completed")
End Sub