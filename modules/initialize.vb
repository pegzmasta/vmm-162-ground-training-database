Rem Attribute VBA_ModuleType=VBAModule
Option VBASupport 1
Option Explicit

' Declare Global Constants
Public Const Author_1 As String * 20 = "Pagan LCpl Michael A" '<<------- New CAC Card Format
Public Const Author_2 As String * 15 = "michael.a.pagan" '<<------------ Old CAC Card Format
Public Const Author_3 As String * 11 = "1404656640A" '<<---------------- Spain Lenovo T520i CAC Card Format
'==========================================================================================================
'   FROM HERE ON OUT, CREATE CODE THAT SUPPORTS ALL FORMATS-- IN CASE SOMEONE IS USING A DIFFERENT COMPUTER
'==========================================================================================================
Public Const My_Title As String * 8 = "[Lvl. 1]"
Public Const Ops_Title As String * 8 = "[Lvl. 2]"
Public Const Admin_Title As String * 8 = "[Lvl. 3]"
Public Const SNCOIC_Title As String * 8 = "[Lvl. 4]"
Public Const SNCO_Title As String * 8 = "[Lvl. 5]"
Public Const Guest_Title As String * 8 = "[Lvl. 6]"

' Declare Global Variables
Public Last_Cell As Integer
Public Full_Name As Variant, Shop As Variant
Public Rank As String, This_Title As String * 24
Public LastUE As Integer, LastSE As Integer, LastPE As Integer, LastCE As Integer, _
       LastWE As Integer, LastBE As Integer, LastIE As Integer, Last_Row As Integer, Shop_Num As Integer, User_Row As Integer
Public Marksman As Boolean, Sharpshooter As Boolean, Expert As Boolean, UNQ As Boolean, NBC_1 As Boolean, PFT_1st As Boolean, _
       PFT_2nd As Boolean, PFT_3rd As Boolean, PFT_UNQ As Boolean, CFT_1st As Boolean, CFT_2nd As Boolean, CFT_3rd As Boolean, CFT_UNQ As Boolean, _
       Database_Empty As Boolean, ReDefine As Boolean
       
' Declare Global Arrays
Public Shop_Ranks(100) As Variant
Public Shop_Positions(100) As Integer
Public Personnel_Table(), DutyStatus_Table(), Contract_Table(), Service_Table(), Education_Table(), BCP_Table(), PFT_Table(), CFT_Table(), _
       Swim_Table(), MCMAP_Table(), NBC_Table(), Rifle_Table(), Pistol_Table()
       
' Declare Global Objects
Public Main_Console As UserForm
Public UFmain As Main_Console
Public colPersonnel As Collection, colDuty_Status As Collection, colContract As Collection, colService As Collection, colEducation As Collection, colBCP As Collection, colPFT As Collection, colCFT As Collection, _
       colSwim As Collection, colMCMAP As Collection, colNBC As Collection, colRifle As Collection, colPistol As Collection, colRecords As Collection

' Separate the Current User's Name (First[, Middle], Last) into 2 or 3 different variables
Public Function STR_SPLIT(str, sep, n) As String
    If str = "" Then
        Exit Function
    Else
        Dim V() As String
        V = Split(str, sep)
        STR_SPLIT = V(n - 1)
    End If
End Function

' Determine what the Last Row is in order to guide cell-navigation
Public Function LR() As Integer
    Dim Program As Workbook
    Set Program = ThisWorkbook
    
    Last_Row = 0
    With Program.Worksheets("RAW DATA")
        If Application.WorksheetFunction.CountBlank(.Range("B3:B4")) <> 0 Then
            Database_Empty = True
            With .Range("A3")
                .Value = 1
                .Offset(0, 1) = "LCPL"
                .Offset(0, 2) = "PAGAN,MICHAEL A."
                .Offset(0, 3) = 1404656640
                .Offset(1, 0) = 2
                .Offset(1, 1) = "PVT"
                .Offset(1, 2) = "SCHMUCKATELI,TERMINAL L."
                .Offset(1, 3) = 1234567890
            End With
            Last_Row = .Range("B3").End(xlDown).Row
        Else
            If .Range("C3") = "" Then: Database_Empty = True
            If .Range("C3") = "PAGAN,MICHAEL A." Then: Database_Empty = True
            Last_Row = .Range("B3").End(xlDown).Row
        End If
    End With
    LR = Last_Row
End Function

' Align the spaces during output to the 'Immediate Window' to allow for a better read (FOR DEBUGGING PURPOSES ONLY)
Public Function CALC_SPC(varName As Variant, varLabel As Variant) As String
    Dim Nbsp As Long
    Dim SubTotsp As Long
    Dim Totsp As String
        
    ' Label3 ("Welcome" Userform) string alignment
    If varLabel = 258 Then
        SubTotsp = Len(varName) / 2 + varLabel / 9
    
    ' Label17 ("Main_Console" Userform) string alignment
    Else: SubTotsp = Len(varName) / 2 + varLabel / 4.5
    End If
    
    For Nbsp = 1 To SubTotsp
        If Nbsp = 1 Then
            Totsp = " "
        Else
            Totsp = Totsp + " "
        End If
    Next
    CALC_SPC = Totsp
End Function

' Determine if the Current User is a Section Leader (Highest Ranking in the Shop)
Private Function MAX_RANK(ShopArray() As Variant) As Boolean
    
    Dim Max_Num As Integer
    Dim Element As Integer
    
    For Element = 0 To Shop_Num
        '======================================================
        ' For Debugging Purposes Only ...
        '>>Debug.Print "Current Element: "; Element
        '>>Debug.Print "Element Value:    "; ShopArray(Element)
        '>>Debug.Print "Current Max =    "; Max_Num
        '======================================================
        If Max_Num = Empty Then
            Max_Num = ShopArray(Element)
        Else
            If ShopArray(Element) > Max_Num Then
                Max_Num = ShopArray(Element)
            End If
        End If
    Next Element
    
    If ShopArray(0) = Max_Num Then
        MAX_RANK = True
    Else: MAX_RANK = False
    End If
End Function

' Determine what Rank the Current User is
Public Function CHK_RANK(YOUR_NAME As Variant) As String
        
    ' Declare Program variables
    Dim Program As Workbook
    Dim Database As Worksheet, Log As Worksheet, Test As Worksheet
    
    ' Define Program references
    Set Program = ThisWorkbook
    Set Database = Program.Worksheets("RAW DATA")
    Set Log = Program.Worksheets("LOG")
    Set Test = Program.Worksheets("TEST")

    Dim Name_Range As Range
    Dim Current_Address As String, End_Range As String
    
    Last_Row = LR()
    
    With Database
        Call Named_Ranges
        Set Name_Range = .Range("NAMES") '<<------------------------------ Define the Search Range
        With Name_Range
            Set YOUR_NAME = .Find(YOUR_NAME) '<<-------------------------- Find the Current User's Name
            Current_Address = YOUR_NAME.Address '<<----------------------- Save the Position where the name was found
        End With
        
        If YOUR_NAME Is Nothing Then
            Exit Function
        Else
            Rank = .Range(Current_Address).Offset(0, -1) '<<-------------- Offset to the left of that position to capture the rank
        End If
    End With
    CHK_RANK = Rank
End Function

' Determine what Grade a Marine is per Rank
Public Function CALC_Grade(Rank As Variant) As Variant

    Dim Grade As Variant

    Select Case Rank

        ' This list is subject to change based on actual encounters on MOL | _
        ````````````````````````````````````````````````````````````````````
        Case "PVT": Grade = "1"  '<<--------- E1
        Case "PFC": Grade = "2"  '<<--------- E2
        Case "LCPL": Grade = "3"  '<<-------- E3
        Case "CPL": Grade = "4"  '<<--------- E4
        Case "SGT": Grade = "5"  '<<--------- E5
        Case "SSGT": Grade = "6"  '<<-------- E6
        Case "GYSGT": Grade = "7"  '<<------- E7
        Case "MSGT": Grade = "8"  '<<-------- E8
        Case "1STSGT": Grade = "8"  '<<------ E8
        Case "MGYSGT": Grade = "9"  '<<------ E9
        Case "SGTMAJ": Grade = "9"  '<<------ E9
        Case "SGTMAJMC": Grade = "9"  '<<---- E9
        Case "WO1": Grade = "10"  '<<-------- W1
        Case "CWO2": Grade = "11"  '<<------- W2
        Case "CWO3": Grade = "12"  '<<------- W3
        Case "CWO4": Grade = "13"  '<<------- W4
        Case "CWO5": Grade = "14"  '<<------- W5
        Case "2NDLT": Grade = "15"  '<<------ O1
        Case "1STLT": Grade = "16"  '<<------ O2
        Case "CAPT": Grade = "17"  '<<------- O3
        Case "MAJ": Grade = "18"  '<<-------- O4
        Case "LTCOL": Grade = "19"  '<<------ O5
        Case "COL": Grade = "20"  '<<-------- O6
        Case "BGEN": Grade = "21"  '<<------- O7
        Case "MAJGEN": Grade = "22"  '<<----- O8
        Case "LTGEN": Grade = "23"  '<<------ O9
        Case "GEN": Grade = "24"  '<<-------- O10
    End Select
    
    CALC_Grade = Grade
End Function

' Re-format the Rank as Proper Case
Public Function Proper_Rank(Rank) As Variant

    Select Case Rank

        ' This list is subject to change based on actual encounters on MOL | _
        ````````````````````````````````````````````````````````````````````
        Case "1": Rank = "Pvt"  '<<--------- E1
        Case "2": Rank = "PFC"  '<<--------- E2
        Case "3": Rank = "LCpl"  '<<-------- E3
        Case "4": Rank = "Cpl"  '<<--------- E4
        Case "5": Rank = "Sgt"  '<<--------- E5
        Case "6": Rank = "SSgt"  '<<-------- E6
        Case "7": Rank = "GySgt"  '<<------- E7
        Case "8": Rank = "MSgt"  '<<-------- E8
        Case "9": Rank = "1st Sgt"  '<<----- E8
        Case "9": Rank = "MGySgt"  '<<------ E9
        Case "9": Rank = "SgtMaj"  '<<------ E9
        Case "9": Rank = "SgtMajMC"  '<<---- E9
        Case "10": Rank = "WO-1"  '<<------- W1
        Case "11": Rank = "CWO-2"  '<<------ W2
        Case "12": Rank = "CWO-3"  '<<------ W3
        Case "13": Rank = "CWO-4"  '<<------ W4
        Case "14": Rank = "CWO-5"  '<<------ W5
        Case "15": Rank = "2ND LT"  '<<----- O1
        Case "16": Rank = "1ST LT"  '<<----- O2
        Case "17": Rank = "Capt"  '<<------- O3
        Case "18": Rank = "Maj"  '<<-------- O4
        Case "19": Rank = "LtCol"  '<<------ O5
        Case "20": Rank = "Col"  '<<-------- O6
        Case "21": Rank = "BGen"  '<<------- O7
        Case "22": Rank = "MajGen"  '<<----- O8
        Case "23": Rank = "LtGen"  '<<------ O9
        Case "24": Rank = "Gen"  '<<-------- O10
    End Select
    
    Proper_Rank = Rank
End Function

' Determine who the current user is
Public Function User_Name(User) As String

    ' Declare Program variables
    Dim Program As Workbook
    Dim Database As Worksheet, Log As Worksheet, Test As Worksheet
    
    ' Define Program references
    Set Program = ThisWorkbook
    Set Database = Program.Worksheets("RAW DATA")
    Set Log = Program.Worksheets("LOG")
    Set Test = Program.Worksheets("TEST")
    
    ' Establish the proper titles
    Dim First As String, Middle As String, Last As String
    
    ' Determine the format of the Username (e.g. "first.m.last" || "Last Rank First M" || "EDIPI #")
    On Error Resume Next
    If STR_SPLIT(User, " ", 2) <> "" Then: '<<------------------------------------ if the delimeter is "." an error will occur
    If Err <> 9 Then '<<---------------------------------------------------------- evaluate the error variable; if err = 9, then change the String Split F(x) delimeter to "."
        
        ' Re-Format the Current User's Name into this format: (LAST,FIRST M. ), _
          and then store it in the "Full_Name" variable
        First = STR_SPLIT(User, " ", 3)
        Middle = STR_SPLIT(User, " ", 4)
        
        If Middle = "" Then
            Last = STR_SPLIT(User, " ", 1)
            Full_Name = Last & "," & First
        Else
            Middle = STR_SPLIT(User, " ", 4)
            Last = STR_SPLIT(User, " ", 1)
            
            If Len(Middle) <> 1 Then
                Full_Name = Last & "," & First & " " & Middle
            Else
                Full_Name = Last & "," & First & " " & Middle & "."
                Full_Name = UCase(Full_Name)
            End If
        End If
    Else
        ' In case an error led to this condition-- Reset Error Variable (i.e. different username format was unexpected)...
        Err = 0
        
        ' Re-Format the Current User's Name into this format: (LAST,FIRST M. ), _
          and then store it in the "Full_Name" variable
        First = STR_SPLIT(User, ".", 1)
        Middle = STR_SPLIT(User, ".", 3)
        
        ' If there is another error, then that means another CAC Card Format was encountered (i. e. EDIPI # FORMAT)
        If Err = 9 Then
            Err = 0
            Full_Name = Database.Range("EDIPI").Find(Mid(Application.UserName, 1, 10)).Offset(0, -1)
        Else
            ' No errors were encountered; evaluate for the OLD CAC Card Format
            If Middle = "" Then
                Last = STR_SPLIT(User, ".", 2)
                Full_Name = Last & "," & First
            Else
                Middle = STR_SPLIT(User, ".", 2)
                Last = STR_SPLIT(User, ".", 3)
                
                If Len(Middle) <> 1 Then
                    Full_Name = Last & "," & First & " " & Middle
                Else
                    Full_Name = Last & "," & First & " " & Middle & "."
                    Full_Name = UCase(Full_Name)
                End If
            End If
        End If
    End If
    User_Name = Full_Name
End Function

' Determine the Rank Structure of all Marines in the Current User's shop
Private Sub Shop_Structure()

    ' Declare Program variables
    Dim Program As Workbook
    Dim Database As Worksheet, Log As Worksheet, Test As Worksheet
    
    ' Define Program references
    Set Program = ThisWorkbook
    Set Database = Program.Worksheets("RAW DATA")
    Set Log = Program.Worksheets("LOG")
    Set Test = Program.Worksheets("TEST")
    
    ' Declare variables to be utilized to calculate the Shop Structure
    Dim Checker As Integer
    Dim First_Address As String
    Dim Name_Range As Range, Shop_Range As Range
    Dim Grade As Variant, Current_Rank As Variant
        
    ' Store the Grade of the Current User in an Array
    Grade = CALC_Grade(Rank)
    Shop_Ranks(0) = Grade
        
    With Database
        
        ' Define Database references _
          ``````````````````````````
        Last_Row = LR() '<<--------------------- Define the Last Row
        Set Name_Range = .Range("NAMES") '<<---- Where to search for names
        Set Shop_Range = .Range("SHOPS") '<<---- Where to search for a specific Shop
        
        ' Define Current User references _
          ``````````````````````````````
        User_Row = .Range(Name_Range.Find(Full_Name).Address).Row '<<---- Find the Row Position of the Current User
        Shop_Positions(0) = User_Row '<<--------------------------------- Store the Row Position of the Current User
        Shop = Name_Range.Find(Full_Name).Offset(0, 4) '<<--------------- Which Shop does the Current User belong to?
        
        ' How many Marines consist of this specific Shop?
        Shop_Num = Application.WorksheetFunction.CountIf(Shop_Range, Shop)
                
        ' Set the Search Criteria for the Find Method
        Set Shop = Shop_Range.Find(Shop)
                
        ' For each Marine in that Shop, Determine what their ranks are, _
          and their positions on the spreadsheet
        For Checker = 1 To Shop_Num
                                            
            Current_Rank = .Range(Shop.Address).Offset(0, -5) '<<-------- Determine the Current Rank
            Shop_Ranks(Checker) = Current_Rank '<<----------------------- Store the rank of this Marine
            Shop_Ranks(Checker) = CALC_Grade(Shop_Ranks(Checker)) '<<---- Store the grade of this Marine
            
            ' To prevent the Find Method from Revolving back to the First address...
            If First_Address <> Empty Then
                If Shop.Address = First_Address Then
                    Exit For
                End If
            End If
            
            ' If the Current User is analyzed, then continue as if this _
              iteration never occurred-- for this Grade has already been stored
            If .Range(Shop.Address).Row = User_Row Then
                Shop_Ranks(Checker) = ""
                Checker = Checker - 1
            Else
                If First_Address = Empty Then
                    First_Address = Shop.Address
                End If
            End If
            
            '========================================================================
            ' For Debugging Purposes Only ...
            '>>Debug.Print "The First Row is:     " & .Range(First_Address).Row & "."
            '>>Debug.Print "The Current Row is: " & .Range(Shop.Address).Row & "."
            '>>Debug.Print "My Row is:          " & User_Row & "."
            '>>Debug.Print "Current #:           " & Checker & "."
            '========================================================================
            
            ' Store the Row Position of this Marine
            Shop_Positions(Checker) = .Range(Shop.Address).Row
            
            ' Search for the next Marine in this Shop
            Set Shop = Shop_Range.FindNext(Shop)
        Next Checker
    End With
End Sub

' Create a priority-based permissions system, to establish access to specific options within this Program
Private Sub Permissions()

    ' Declare Program variables
    Dim Program As Workbook
    Dim Database As Worksheet, Log As Worksheet, Test As Worksheet
    
    ' Define Program references
    Set Program = ThisWorkbook
    Set Database = Program.Worksheets("RAW DATA")
    Set Log = Program.Worksheets("LOG")
    Set Test = Program.Worksheets("TEST")

    ' Declare the User Name Variable
    Dim Current_User As Variant
    
    ' Retrieve User Information from the Environment (Defined by CAC Card)
    Current_User = Application.UserName
    
    ' Define the User Name
    Current_User = User_Name(Current_User)
    
    ' Determine what the Rank is for the Current User
    Rank = CHK_RANK(Current_User)
    
    ' Determine the Shop Structure for the Current User (i.e. Gather Ranks & Positions of that Shop)
    Call Shop_Structure
    
    ' Set a Title to the Current User for access to specific options, _
      based on their Name, Rank, and Shop
    If Current_User = Author_1 Then
        This_Title = My_Title
        ElseIf Current_User = Author_2 Then
            This_Title = My_Title
            ElseIf Current_User = Author_3 Then
                This_Title = My_Title
                ElseIf Current_User = "PAGAN,MICHAEL A." Then
                    This_Title = My_Title
                    'Welcome.Show
                    ElseIf Shop = "10S3" And Shop_Ranks(0) < 7 Then
                        This_Title = Ops_Title
                        'Welcome.Show
                        ElseIf Shop = "10S1" Then
                            This_Title = Admin_Title
                            'Welcome.Show
                            ElseIf MAX_RANK(Shop_Ranks()) = True Then
                                This_Title = SNCOIC_Title
                                'Welcome.Show
                                ElseIf Shop_Ranks(0) > 5 Then
                                    This_Title = SNCO_Title
                                    'Welcome.Show
        Else
            This_Title = Guest_Title
            'Welcome.Show
    End If
End Sub

' Create an easy way to reference Ranges (it becomes imperative to alter the Ranges when dealing with an empty Database)
Sub Named_Ranges()

    ' Declare Program variables
    Dim Program As Workbook
    Dim Database As Worksheet, Log As Worksheet, Test As Worksheet
    
    ' Define Program references
    Set Program = ThisWorkbook
    Set Database = Program.Worksheets("RAW DATA")
    Set Log = Program.Worksheets("LOG")
    Set Test = Program.Worksheets("TEST")
    
    If Redefine_Database = False Then
        If ReDefine = False Then
            Last_Cell = LR()
            ' Create named ranges for easy reference
            '________
            ' Tables | _
            ``````````
            Database.Range("B3:J" & Last_Cell).NAME = "PERSONNEL"
            Database.Range("K3:Q" & Last_Cell).NAME = "DUTY_STATUS"
            Database.Range("R3:X" & Last_Cell).NAME = "CONTRACT"
            Database.Range("Y3:AH" & Last_Cell).NAME = "SERVICE"
            Database.Range("AI3:AO" & Last_Cell).NAME = "EDUCATION"
            Database.Range("AP3:AT" & Last_Cell).NAME = "BCP"
            Database.Range("AU3:BA" & Last_Cell).NAME = "PFT"
            Database.Range("BB3:BH" & Last_Cell).NAME = "CFT"
            Database.Range("BI3:BJ" & Last_Cell).NAME = "SWIM"
            Database.Range("BK3:BP" & Last_Cell).NAME = "MCMAP"
            Database.Range("BQ3:BQ" & Last_Cell).NAME = "NBC"
            Database.Range("BR3:BY" & Last_Cell).NAME = "RIFLE"
            Database.Range("BZ3:CE" & Last_Cell).NAME = "PISTOL"
            '_________
            ' Columns | _
            ```````````
            Database.Range("B3:B" & Last_Cell).NAME = "RANKS"
            Database.Range("C3:C" & Last_Cell).NAME = "NAMES"
            Database.Range("D3:D" & Last_Cell).NAME = "EDIPI"
            Database.Range("AA3:AA" & Last_Cell).NAME = "UNITS"
            Database.Range("AB3:AB" & Last_Cell).NAME = "SHOPS"
            Database.Range("AZ3:AZ" & Last_Cell).NAME = "PQUAL"
            Database.Range("BG3:BG" & Last_Cell).NAME = "CQUAL"
            Database.Range("BI3:BI" & Last_Cell).NAME = "SQUAL"
            Database.Range("BM3:BM" & Last_Cell).NAME = "BELTS"
            Database.Range("BO3:BO" & Last_Cell).NAME = "INSTRUCTORS"
            Database.Range("BV3:BV" & Last_Cell).NAME = "RQUAL"
        Else
            ' Re-Define these named Ranges and shift value of start address by -1, so that the Find Method can search properly _
              (The Find Method considers the first row in the range to be a header-- which it's not-- & skips it, that's why)
            '________
            ' Tables | _
            ``````````
            Database.Range("B2:J" & Last_Cell).NAME = "PERSONNEL"
            Database.Range("K2:Q" & Last_Cell).NAME = "DUTY_STATUS"
            Database.Range("R2:X" & Last_Cell).NAME = "CONTRACT"
            Database.Range("Y2:AH" & Last_Cell).NAME = "SERVICE"
            Database.Range("AI2:AO" & Last_Cell).NAME = "EDUCATION"
            Database.Range("AP2:AT" & Last_Cell).NAME = "BCP"
            Database.Range("AU2:BA" & Last_Cell).NAME = "PFT"
            Database.Range("BB2:BH" & Last_Cell).NAME = "CFT"
            Database.Range("BI2:BJ" & Last_Cell).NAME = "SWIM"
            Database.Range("BK2:BP" & Last_Cell).NAME = "MCMAP"
            Database.Range("BQ2:BQ" & Last_Cell).NAME = "NBC"
            Database.Range("BR2:BY" & Last_Cell).NAME = "RIFLE"
            Database.Range("BZ2:CE" & Last_Cell).NAME = "PISTOL"
            '_________
            ' Columns | _
            ```````````
            Database.Range("B2:B" & Last_Cell).NAME = "RANKS"
            Database.Range("C2:C" & Last_Cell).NAME = "NAMES"
            Database.Range("D2:D" & Last_Cell).NAME = "EDIPI"
            Database.Range("AA2:AA" & Last_Cell).NAME = "UNITS"
            Database.Range("AB2:AB" & Last_Cell).NAME = "SHOPS"
            Database.Range("AZ2:AZ" & Last_Cell).NAME = "PQUAL"
            Database.Range("BG2:BG" & Last_Cell).NAME = "CQUAL"
            Database.Range("BI2:BI" & Last_Cell).NAME = "SQUAL"
            Database.Range("BM2:BM" & Last_Cell).NAME = "BELTS"
            Database.Range("BO2:BO" & Last_Cell).NAME = "INSTRUCTORS"
            Database.Range("BV2:BV" & Last_Cell).NAME = "RQUAL"
        End If
    Else
        ' Re-Define these named ranges to match how many records there will be after the update process is complete
        '________
        ' Tables | _
        ``````````
        Database.Range("B2:J" & BTR_Count + 2).NAME = "PERSONNEL"
        Database.Range("K2:Q" & BTR_Count + 2).NAME = "DUTY_STATUS"
        Database.Range("R2:X" & BTR_Count + 2).NAME = "CONTRACT"
        Database.Range("Y2:AH" & BTR_Count + 2).NAME = "SERVICE"
        Database.Range("AI2:AO" & BTR_Count + 2).NAME = "EDUCATION"
        Database.Range("AP2:AT" & BTR_Count + 2).NAME = "BCP"
        Database.Range("AU2:BA" & BTR_Count + 2).NAME = "PFT"
        Database.Range("BB2:BH" & BTR_Count + 2).NAME = "CFT"
        Database.Range("BI2:BJ" & BTR_Count + 2).NAME = "SWIM"
        Database.Range("BK2:BP" & BTR_Count + 2).NAME = "MCMAP"
        Database.Range("BQ2:BQ" & BTR_Count + 2).NAME = "NBC"
        Database.Range("BR2:BY" & BTR_Count + 2).NAME = "RIFLE"
        Database.Range("BZ2:CE" & BTR_Count + 2).NAME = "PISTOL"
        '_________
        ' Columns | _
        ```````````
        Database.Range("B2:B" & BTR_Count + 2).NAME = "RANKS"
        Database.Range("C2:C" & BTR_Count + 2).NAME = "NAMES"
        Database.Range("D2:D" & BTR_Count + 2).NAME = "EDIPI"
        Database.Range("AA2:AA" & BTR_Count + 2).NAME = "UNITS"
        Database.Range("AB2:AB" & BTR_Count + 2).NAME = "SHOPS"
        Database.Range("AZ2:AZ" & BTR_Count + 2).NAME = "PQUAL"
        Database.Range("BG2:BG" & BTR_Count + 2).NAME = "CQUAL"
        Database.Range("BI2:BI" & BTR_Count + 2).NAME = "SQUAL"
        Database.Range("BM2:BM" & BTR_Count + 2).NAME = "BELTS"
        Database.Range("BO2:BO" & BTR_Count + 2).NAME = "INSTRUCTORS"
        Database.Range("BV2:BV" & BTR_Count + 2).NAME = "RQUAL"
    End If
End Sub

' The Main Program!
Sub GTDbase()

    ' Declare Program variables
    Dim Program As Workbook
    Dim Database As Worksheet, Log As Worksheet, Test As Worksheet
    
    ' Define Program references
    Set Program = ThisWorkbook
    Set Database = Program.Worksheets("RAW DATA")
    Set Log = Program.Worksheets("LOG")
    Set Test = Program.Worksheets("TEST")

    ' Clear all filters and show all hidden data
    ThisWorkbook.Sheets(1).Visible = xlSheetVisible
    On Error GoTo All_Data_Visible
    Database.ShowAllData

All_Data_Visible:

    ' Name all the ranges in the Database to prepare for efficient Data-Extraction for the ListView Collections
    ReDefine = False
    Redefine_Database = False
    Call Named_Ranges
    
    ' Calculate User Permissions
    Call Permissions
    
    ' Define ListView Objects as a Collection
    Set colPersonnel = New Collection
    Set colDuty_Status = New Collection
    Set colContract = New Collection
    Set colService = New Collection
    Set colEducation = New Collection
    Set colBCP = New Collection
    Set colPFT = New Collection
    Set colCFT = New Collection
    Set colSwim = New Collection
    Set colMCMAP = New Collection
    Set colNBC = New Collection
    Set colRifle = New Collection
    Set colPistol = New Collection
    
    ' Fill each Collection with the appropiate data _
    _________________________________________________
    Personnel_Table = Database.Range("PERSONNEL")
    Set colPersonnel = FillPersonnel()
    Erase Personnel_Table
    
    DutyStatus_Table = Database.Range("DUTY_STATUS")
    Set colDuty_Status = FillDuty_Status()
    Erase DutyStatus_Table
    
    Contract_Table = Database.Range("CONTRACT")
    Set colContract = FillContract()
    Erase Contract_Table
    
    Service_Table = Database.Range("SERVICE")
    Set colService = FillService()
    Erase Service_Table
    
    Education_Table = Database.Range("EDUCATION")
    Set colEducation = FillEducation()
    Erase Education_Table
    
    BCP_Table = Database.Range("BCP")
    Set colBCP = FillBCP()
    Erase BCP_Table
    
    PFT_Table = Database.Range("PFT")
    Set colPFT = FillPFT()
    Erase PFT_Table
    
    CFT_Table = Database.Range("CFT")
    Set colCFT = FillCFT()
    Erase CFT_Table
    
    Swim_Table = Database.Range("SWIM")
    Set colSwim = FillSwim()
    Erase Swim_Table
    
    MCMAP_Table = Database.Range("MCMAP")
    Set colMCMAP = FillMCMAP()
    Erase MCMAP_Table
    
    NBC_Table = Database.Range("NBC")
    Set colNBC = FillNBC()
    Erase NBC_Table
    
    Rifle_Table = Database.Range("RIFLE")
    Set colRifle = FillRifle()
    Erase Rifle_Table
    
    Pistol_Table = Database.Range("PISTOL")
    Set colPistol = FillPistol()
    Erase Pistol_Table
    
    ' Make a slight modification to the Ranges for the Find Method and the Get_ComboList Procedure
    ReDefine = True
    Call Named_Ranges
    
    ' Create a new instance of the 'Main_Console' rather than relying on the default instance
    ThisWorkbook.Sheets(1).Visible = xlSheetVeryHidden
    Set UFmain = New Main_Console
    
    ' Load the records into custom properties of the userform
    '                 | |
    '                 | |
    '                `` ``
    '                 \ /
    '                  V
    Set UFmain.Personnel = colPersonnel
    Set UFmain.Duty_Status = colDuty_Status
    Set UFmain.Contract = colContract
    Set UFmain.Service = colService
    Set UFmain.Education = colEducation
    Set UFmain.BCP = colBCP
    Set UFmain.PFT = colPFT
    Set UFmain.CFT = colCFT
    Set UFmain.SWIM = colSwim
    Set UFmain.MCMAP = colMCMAP
    Set UFmain.NBC = colNBC
    Set UFmain.Rifle = colRifle
    Set UFmain.Pistol = colPistol
    
    ' When Program is complete, make it Modal
    '>>UFmain.Show False '<<---- Show the form. Code is suspended at this point.  Append the string 'False' to make the form Modal.
    UFmain.Show
    
    ' ***** FOR DEBUGGING PURPOSES ONLY *****
    '
    ' Print out the records to the Immediate Window to make sure I didn't miss something!  Step through the code, and verify each element of each array as it passes the loop.
    '>>For i = 1 To colPersonnel.Count
    '>>Debug.Assert (i <> 255)
        '>>Debug.Print colPersonnel(i).Rank, colPersonnel(i).Name, colPersonnel(i).EDIPI, colPersonnel(i).DCTB, colPersonnel(i).Unit, colPersonnel(i).Shop, _
                       colRifle(i).REQ, colRifle(i).Rifle, colRifle(i).Table1, colRifle(i).Table2, colRifle(i).Score, colRifle(i).Code, colRifle(i).Table3, _
                       colPFT(i).PFT, colPFT(i).Status, colPFT(i).Score, colPFT(i).Class, _
                       colCFT(i).CFT, colCFT(i).Status, colCFT(i).MTC, colCFT(i).Ammo, colCFT(i).MUF, colCFT(i).Score, colCFT(i).Class, _
                       colSwim(i).Swim, colSwim(i).Status, colSwim(i).Requal, _
                       colMCMAP(i).MCMAP, colMCMAP(i).Qual, colMCMAP(i).Belt, colMCMAP(i).Instructor, colMCMAP(i).Recert, _
                       colNBC(i).NBC
    '>>Next i
End Sub

Private Function FillPersonnel()

    ' Declare Program variables
    Dim Program As Workbook
    Dim Database As Worksheet, Log As Worksheet, Test As Worksheet
    
    ' Define Program references
    Set Program = ThisWorkbook
    Set Database = Program.Worksheets("RAW DATA")
    Set Log = Program.Worksheets("LOG")
    Set Test = Program.Worksheets("TEST")

    Dim Last_Cell As Long, i As Long
    Dim clsPersonnel As Personnel_Record
    
    Set colPersonnel = New Collection
    Last_Cell = LR()
    
    For i = 1 To Last_Cell - 2
    
        Set clsPersonnel = New Personnel_Record
        clsPersonnel.Rank = Personnel_Table(i, 1)
        clsPersonnel.NAME = Personnel_Table(i, 2)
        clsPersonnel.EDIPI = Personnel_Table(i, 3)
        clsPersonnel.Sex = Personnel_Table(i, 4)
        clsPersonnel.DOB = Personnel_Table(i, 5)
        clsPersonnel.EMail = Personnel_Table(i, 6)
        clsPersonnel.Ed_Level = Personnel_Table(i, 7)
        clsPersonnel.CERT = Personnel_Table(i, 8)
        clsPersonnel.Major = Personnel_Table(i, 9)
        colPersonnel.Add clsPersonnel, CStr(i)
    Next i
    
    Set FillPersonnel = colPersonnel
End Function

Private Function FillDuty_Status()

    ' Declare Program variables
    Dim Program As Workbook
    Dim Database As Worksheet, Log As Worksheet, Test As Worksheet
    
    ' Define Program references
    Set Program = ThisWorkbook
    Set Database = Program.Worksheets("RAW DATA")
    Set Log = Program.Worksheets("LOG")
    Set Test = Program.Worksheets("TEST")

    Dim Last_Cell As Long, i As Long
    Dim clsDuty_Status As Duty_Status_Record
    
    Set colDuty_Status = New Collection
    Last_Cell = LR()
    
    For i = 1 To Last_Cell - 2
    
        Set clsDuty_Status = New Duty_Status_Record
        clsDuty_Status.Status = DutyStatus_Table(i, 1)
        clsDuty_Status.Limit = DutyStatus_Table(i, 2)
        clsDuty_Status.Limit_Date = DutyStatus_Table(i, 3)
        clsDuty_Status.STR_CAT = DutyStatus_Table(i, 4)
        clsDuty_Status.STR_CAT_Date = DutyStatus_Table(i, 5)
        clsDuty_Status.ComCasualty = DutyStatus_Table(i, 6)
        clsDuty_Status.ComCas_Date = DutyStatus_Table(i, 7)
        colDuty_Status.Add clsDuty_Status, CStr(i)
    Next i
    
    Set FillDuty_Status = colDuty_Status
End Function

Private Function FillContract()

    ' Declare Program variables
    Dim Program As Workbook
    Dim Database As Worksheet, Log As Worksheet, Test As Worksheet
    
    ' Define Program references
    Set Program = ThisWorkbook
    Set Database = Program.Worksheets("RAW DATA")
    Set Log = Program.Worksheets("LOG")
    Set Test = Program.Worksheets("TEST")

    Dim Last_Cell As Long, i As Long
    Dim clsContract As Contract_Record
    
    Set colContract = New Collection
    Last_Cell = LR()
    
    For i = 1 To Last_Cell - 2
    
        Set clsContract = New Contract_Record
        clsContract.Orig_Entry = Contract_Table(i, 1)
        clsContract.Enlist = Contract_Table(i, 2)
        clsContract.EAS = Contract_Table(i, 3)
        clsContract.EOS = Contract_Table(i, 4)
        clsContract.PEF = Contract_Table(i, 5)
        clsContract.Bonus_PEF = Contract_Table(i, 6)
        clsContract.College_PEF = Contract_Table(i, 7)
        colContract.Add clsContract, CStr(i)
    Next i
    
    Set FillContract = colContract
End Function

Private Function FillService()

    ' Declare Program variables
    Dim Program As Workbook
    Dim Database As Worksheet, Log As Worksheet, Test As Worksheet
    
    ' Define Program references
    Set Program = ThisWorkbook
    Set Database = Program.Worksheets("RAW DATA")
    Set Log = Program.Worksheets("LOG")
    Set Test = Program.Worksheets("TEST")

    Dim Last_Cell As Long, i As Long
    Dim clsService As Service_Record
    
    Set colService = New Collection
    Last_Cell = LR()
    
    For i = 1 To Last_Cell - 2
    
        Set clsService = New Service_Record
        clsService.DOR = Service_Table(i, 1)
        clsService.DCTB = Service_Table(i, 2)
        clsService.Unit = Service_Table(i, 3)
        clsService.Shop = Service_Table(i, 4)
        clsService.Billet = Service_Table(i, 5)
        clsService.PMOS = Service_Table(i, 6)
        clsService.BMOS = Service_Table(i, 7)
        clsService.AMOS1 = Service_Table(i, 8)
        clsService.AMOS2 = Service_Table(i, 9)
        clsService.Security = Service_Table(i, 10)
        colService.Add clsService, CStr(i)
    Next i
    
    Set FillService = colService
End Function

Private Function FillEducation()

    ' Declare Program variables
    Dim Program As Workbook
    Dim Database As Worksheet, Log As Worksheet, Test As Worksheet
    
    ' Define Program references
    Set Program = ThisWorkbook
    Set Database = Program.Worksheets("RAW DATA")
    Set Log = Program.Worksheets("LOG")
    Set Test = Program.Worksheets("TEST")

    Dim Last_Cell As Long, i As Long
    Dim clsEducation As Education_Record
    
    Set colEducation = New Collection
    Last_Cell = LR()
    
    For i = 1 To Last_Cell - 2
    
        Set clsEducation = New Education_Record
        clsEducation.OPSEC = Education_Table(i, 1)
        clsEducation.IAA = Education_Table(i, 2)
        clsEducation.PTP = Education_Table(i, 3)
        clsEducation.NLMB = Education_Table(i, 4)
        clsEducation.SAPR = Education_Table(i, 5)
        clsEducation.PME_Completed = Education_Table(i, 6)
        clsEducation.PME = Education_Table(i, 7)
        colEducation.Add clsEducation, CStr(i)
    Next i
    
    Set FillEducation = colEducation
End Function

Private Function FillBCP()

    ' Declare Program variables
    Dim Program As Workbook
    Dim Database As Worksheet, Log As Worksheet, Test As Worksheet
    
    ' Define Program references
    Set Program = ThisWorkbook
    Set Database = Program.Worksheets("RAW DATA")
    Set Log = Program.Worksheets("LOG")
    Set Test = Program.Worksheets("TEST")

    Dim Last_Cell As Long, i As Long
    Dim clsBCP As MCBCP_Record
    
    Set colBCP = New Collection
    Last_Cell = LR()
    
    For i = 1 To Last_Cell - 2
    
        Set clsBCP = New MCBCP_Record
        clsBCP.Height = BCP_Table(i, 1)
        clsBCP.Weight = BCP_Table(i, 2)
        clsBCP.Body_Fat = BCP_Table(i, 3)
        clsBCP.Effective = BCP_Table(i, 4)
        clsBCP.Status = BCP_Table(i, 5)
        colBCP.Add clsBCP, CStr(i)
    Next i
    
    Set FillBCP = colBCP
End Function

Private Function FillPFT()

    ' Declare Program variables
    Dim Program As Workbook
    Dim Database As Worksheet, Log As Worksheet, Test As Worksheet
    
    ' Define Program references
    Set Program = ThisWorkbook
    Set Database = Program.Worksheets("RAW DATA")
    Set Log = Program.Worksheets("LOG")
    Set Test = Program.Worksheets("TEST")

    Dim Last_Cell As Long, i As Long
    Dim clsPFT As PFT_Record
    
    Set colPFT = New Collection
    Last_Cell = LR()
    
    For i = 1 To Last_Cell - 2
    
        Set clsPFT = New PFT_Record
        clsPFT.PFT = PFT_Table(i, 1)
        clsPFT.Pull_Ups = PFT_Table(i, 2)
        clsPFT.Crunches = PFT_Table(i, 3)
        clsPFT.Run = PFT_Table(i, 4)
        clsPFT.Score = PFT_Table(i, 5)
        clsPFT.Class = PFT_Table(i, 6)
        clsPFT.Description = PFT_Table(i, 7)
        colPFT.Add clsPFT, CStr(i)
    Next i
    
    Set FillPFT = colPFT
End Function

Private Function FillCFT()

    ' Declare Program variables
    Dim Program As Workbook
    Dim Database As Worksheet, Log As Worksheet, Test As Worksheet
    
    ' Define Program references
    Set Program = ThisWorkbook
    Set Database = Program.Worksheets("RAW DATA")
    Set Log = Program.Worksheets("LOG")
    Set Test = Program.Worksheets("TEST")

    Dim Last_Cell As Long, i As Long
    Dim clsCFT As CFT_Record
    
    Set colCFT = New Collection
    Last_Cell = LR()
    
    For i = 1 To Last_Cell - 2
    
        Set clsCFT = New CFT_Record
        clsCFT.CFT = CFT_Table(i, 1)
        clsCFT.MTC = CFT_Table(i, 2)
        clsCFT.Ammo_Lifts = CFT_Table(i, 3)
        clsCFT.MUF = CFT_Table(i, 4)
        clsCFT.Score = CFT_Table(i, 5)
        clsCFT.Class = CFT_Table(i, 6)
        clsCFT.Description = CFT_Table(i, 7)
        colCFT.Add clsCFT, CStr(i)
    Next i
    
    Set FillCFT = colCFT
End Function

Private Function FillSwim()

    ' Declare Program variables
    Dim Program As Workbook
    Dim Database As Worksheet, Log As Worksheet, Test As Worksheet
    
    ' Define Program references
    Set Program = ThisWorkbook
    Set Database = Program.Worksheets("RAW DATA")
    Set Log = Program.Worksheets("LOG")
    Set Test = Program.Worksheets("TEST")

    Dim Last_Cell As Long, i As Long
    Dim clsSwim As Swim_Record
    
    Set colSwim = New Collection
    Last_Cell = LR()
    
    For i = 1 To Last_Cell - 2
    
        Set clsSwim = New Swim_Record
        clsSwim.SWIM = Swim_Table(i, 1)
        clsSwim.Requal = Swim_Table(i, 2)
        colSwim.Add clsSwim, CStr(i)
    Next i
    
    Set FillSwim = colSwim
End Function

Private Function FillMCMAP()

    ' Declare Program variables
    Dim Program As Workbook
    Dim Database As Worksheet, Log As Worksheet, Test As Worksheet
    
    ' Define Program references
    Set Program = ThisWorkbook
    Set Database = Program.Worksheets("RAW DATA")
    Set Log = Program.Worksheets("LOG")
    Set Test = Program.Worksheets("TEST")

    Dim Last_Cell As Long, i As Long
    Dim clsMCMAP As MCMAP_Record
    
    Set colMCMAP = New Collection
    Last_Cell = LR()
    
    For i = 1 To Last_Cell - 2
    
        Set clsMCMAP = New MCMAP_Record
        clsMCMAP.MCMAP = MCMAP_Table(i, 1)
        clsMCMAP.Qual = MCMAP_Table(i, 2)
        clsMCMAP.Belt = MCMAP_Table(i, 3) & " " & MCMAP_Table(i, 4)
        clsMCMAP.Instructor = MCMAP_Table(i, 5)
        clsMCMAP.Recert = MCMAP_Table(i, 6)
        colMCMAP.Add clsMCMAP, CStr(i)
    Next i
    
    Set FillMCMAP = colMCMAP
End Function

Private Function FillNBC()

    ' Declare Program variables
    Dim Program As Workbook
    Dim Database As Worksheet, Log As Worksheet, Test As Worksheet
    
    ' Define Program references
    Set Program = ThisWorkbook
    Set Database = Program.Worksheets("RAW DATA")
    Set Log = Program.Worksheets("LOG")
    Set Test = Program.Worksheets("TEST")

    Dim Last_Cell As Long, i As Long
    Dim clsNBC As NBC_Record
    
    Set colNBC = New Collection
    Last_Cell = LR()
    
    For i = 1 To Last_Cell - 2
    
        Set clsNBC = New NBC_Record
        clsNBC.NBC = NBC_Table(i, 1)
        colNBC.Add clsNBC, CStr(i)
    Next i
    
    Set FillNBC = colNBC
End Function

Private Function FillRifle()

    ' Declare Program variables
    Dim Program As Workbook
    Dim Database As Worksheet, Log As Worksheet, Test As Worksheet
    
    ' Define Program references
    Set Program = ThisWorkbook
    Set Database = Program.Worksheets("RAW DATA")
    Set Log = Program.Worksheets("LOG")
    Set Test = Program.Worksheets("TEST")

    Dim Last_Cell As Long, i As Long
    Dim clsRifle As Rifle_Record
    
    Set colRifle = New Collection
    Last_Cell = LR()
    
    For i = 1 To Last_Cell - 2
    
        Set clsRifle = New Rifle_Record
        clsRifle.Rifle = Rifle_Table(i, 1)
        clsRifle.TableI = Rifle_Table(i, 2)
        clsRifle.TableII = Rifle_Table(i, 3)
        clsRifle.Score = Rifle_Table(i, 4)
        clsRifle.Code = Rifle_Table(i, 5)
        clsRifle.Expert_Quals = Rifle_Table(i, 6)
        clsRifle.Exempt = Rifle_Table(i, 7)
        clsRifle.Exempt_Date = Rifle_Table(i, 8)
        colRifle.Add clsRifle, CStr(i)
    Next i
    
    Set FillRifle = colRifle
End Function

Private Function FillPistol()

    ' Declare Program variables
    Dim Program As Workbook
    Dim Database As Worksheet, Log As Worksheet, Test As Worksheet
    
    ' Define Program references
    Set Program = ThisWorkbook
    Set Database = Program.Worksheets("RAW DATA")
    Set Log = Program.Worksheets("LOG")
    Set Test = Program.Worksheets("TEST")

    Dim Last_Cell As Long, i As Long
    Dim clsPistol As Pistol_Record
    
    Set colPistol = New Collection
    Last_Cell = LR()
    
    For i = 1 To Last_Cell - 2
    
        Set clsPistol = New Pistol_Record
        clsPistol.Pistol = Pistol_Table(i, 1)
        clsPistol.Score = Pistol_Table(i, 2)
        clsPistol.Code = Pistol_Table(i, 3)
        clsPistol.Expert_Quals = Pistol_Table(i, 4)
        clsPistol.Exempt = Pistol_Table(i, 5)
        clsPistol.Exempt_Date = Pistol_Table(i, 6)
        colPistol.Add clsPistol, CStr(i)
    Next i
    
    Set FillPistol = colPistol
End Function