Rem Attribute VBA_ModuleType=VBAModule
Option VBASupport 1
Option Explicit

' Declare Global variables for 'Main_Console' Userform
Public Name_Address As Variant
Public First_Address As String, Current_Grade As String
Public Query As Boolean, Name_Segmented As Boolean, Non_Letter As Boolean, One_Middle As Boolean, _
       EDIPI_TEN As Boolean, EDIPI_NUM As Boolean, Uppercase As Boolean, Lowercase As Boolean, _
       UNITS As Boolean, SHOPS As Boolean, Rifle As Boolean, PFT As Boolean, CFT As Boolean, _
       SWIM As Boolean, MCMAP As Boolean, MCMAP_2 As Boolean, SKIP As Boolean, ID As Boolean, _
       No_Name As Boolean, EDIPI As Boolean, No_EDIPI As Boolean, Filter As Boolean, Rifle_Complete As Boolean, _
       PFT_Complete As Boolean, CFT_Complete As Boolean, Swim_Complete As Boolean, MCMAP_Complete As Boolean, _
       Rifle_Incomplete As Boolean, PFT_Incomplete As Boolean, CFT_Incomplete As Boolean, Swim_Incomplete As Boolean, _
       MCMAP_Incomplete As Boolean, NBC_2 As Boolean, Limit_Exceeded As Boolean, Get_Ranks As Boolean, _
       Ranks_After_Query As Boolean, Back2Query As Boolean, Next_Grade As Boolean, List_Officers As Boolean, List_SNCO As Boolean, _
       List_NCO As Boolean, List_Junior_Marines As Boolean, List_Rank As Boolean
       
Public Unit_Rows() As Integer, Shop_Rows() As Integer, Rifle_Rows() As Integer, PFT_Rows() As Integer, _
       CFT_Rows() As Integer, Swim_Rows() As Integer, MCMAP_Rows() As Integer, NBC_Rows() As Integer, _
       Unit_Num As Integer, Rifle_Num As Integer, PFT_Num As Integer, CFT_Num As Integer, _
       Swim_Num As Integer, MCMAP_Num As Integer, NBC_Num As Integer, Last_Value As Integer, _
       Expiration_Year As Integer, Expiration_Date As Date, Query_Counter As Integer

' Declare Global Arrays for 'Main_Console' Userform
Public UnitList(20) As Variant
Public ShopList(20) As Variant
Public PFT_List(20) As Variant
Public CFT_List(20) As Variant
Public Swim_Qual(20) As Variant
Public Belt_List(20) As Variant
Public Belt_Inst(20) As Variant
Public Personnel_Arr(6) As Variant
Public Rifle_Arr(3) As Variant
Public PFT_Arr(4) As Variant
Public CFT_Arr(4) As Variant
Public Swim_Arr(0) As Variant
Public MCMAP_Arr(1) As Variant
Public Row_Positions() As Integer

'-----------------------------------------------------------------------------------------------------------------------------------------|
'                                                      ###############################                                                    |
'  ==================================================  ####  USERFORM PROCEDURES  ####  ==================================================|
'                                                      ###############################                                                    |
'-----------------------------------------------------------------------------------------------------------------------------------------|

' This Procedure Calculates how the Query should filter the Ranks of the Records when prompted
Public Function Filter_Ranks(Rank_Address) As Variant
    Dim Rank_Column As Integer
    ThisWorkbook.Sheets(1).Visible = xlSheetVisible
    
    Rank_Column = (Range(Rank_Address).Column - 2) * -1
    If Ranks_After_Query = True Then
        List_Rank = False
        Current_Grade = CALC_Grade(ThisWorkbook.Sheets(1).Range(Rank_Address).Offset(0, Rank_Column))
        If List_Junior_Marines = True Then
            If Current_Grade >= 4 Then
                List_Rank = False
            Else
                List_Rank = True
                GoTo Filtered
            End If
        End If
        If List_NCO = True Then
            If Current_Grade <= 3 Or Current_Grade >= 6 Then
                List_Rank = False
            Else
                List_Rank = True
                GoTo Filtered
            End If
        End If
        If List_SNCO = True Then
            If Current_Grade <= 5 Or Current_Grade >= 10 Then
                List_Rank = False
            Else
                List_Rank = True
                GoTo Filtered
            End If
        End If
        If List_Officers = True Then
            If Current_Grade <= 10 Then
                List_Rank = False
            Else
                List_Rank = True
                GoTo Filtered
            End If
        End If
    End If
Filtered:
    ThisWorkbook.Sheets(1).Visible = xlSheetVeryHidden
    Filter_Ranks = List_Rank
End Function

' This Procedure Captures Values for 7 different Arrays, which will be used to add items to all ComboBoxes (except one: 8) in the Multi-page Query Control
Sub Get_ComboList()
    
    ' Declare Program variables
    Dim Program As Workbook
    Dim Database As Worksheet, Log As Worksheet, Test As Worksheet
    ThisWorkbook.Sheets(3).Visible = xlSheetVisible
    
    ' Define Program references
    Set Program = ThisWorkbook
    Set Database = Program.Worksheets("RAW DATA")
    Set Log = Program.Worksheets("LOG")
    Set Test = Program.Worksheets("TEST")
    
    ' Declare 'Combo List' variables
    Dim Post_Unit As Range, Post_Shop As Range, Post_PFT As Range, Post_CFT As Range, _
        Post_Swim As Range, Post_Belt As Range, Post_Inst As Range, SortRng As Range
    Dim Same_Unit As Boolean, Same_Shop As Boolean, Same_PFT As Boolean, Same_CFT As Boolean, _
        Same_Swim As Boolean, Same_Belt As Boolean, Same_Inst As Boolean, Initial As Boolean
    Dim RW As Integer, Counter As Integer, Target As Integer, Compare As Integer, _
        Unit_Margin As Integer, Shop_Margin As Integer, PFT_Margin As Integer, CFT_Margin As Integer, _
        Swim_Margin As Integer, Belt_Margin As Integer, Inst_Margin As Integer, _
        Unit_Element As Integer, Shop_Element As Integer, PFT_Element As Integer, CFT_Element As Integer, _
        Swim_Element As Integer, Belt_Element As Integer, Inst_Element As Integer
    
    ' Initialize these variables to their default values
    LastUE = 0
    LastSE = 0
    LastPE = 0
    LastCE = 0
    LastWE = 0
    LastBE = 0
    LastIE = 0
    Last_Row = LR()
    Unit_Margin = 0
    Shop_Margin = 0
    PFT_Margin = 0
    CFT_Margin = 0
    Swim_Margin = 0
    Belt_Margin = 0
    Inst_Margin = 0
            
    ' Activate Database
    Database.Activate
            
    ' Create 7 Arrays: (1) Different 'Unit' Values; (2) Different 'Shop' Values; (3) Different 'PFT' Values; _
                       (4) Different 'CFT' Values; (5) Different 'Swim' Values; (6) Different 'Belt' Values; _
                       (7) Different 'Instructor' Values
    For RW = 3 To Last_Row
        
        ' Reset these variables after each loop to maintain proper functioning
        Same_Unit = False
        Same_Shop = False
        Same_PFT = False
        Same_CFT = False
        Same_Swim = False
        Same_Belt = False
        Same_Inst = False
        
        ' Exists to prevent skipping Elements in an Array, in the case of improper element shift
        If LastUE = 0 Then
            Unit_Element = RW - 3 - Unit_Margin
        Else
            Unit_Element = RW - 3 - Unit_Margin
            If Unit_Element <> LastUE + 1 Then
                Unit_Element = LastUE + 1
                Unit_Margin = Unit_Margin + 1
            End If
        End If
        If LastSE = 0 Then
            Shop_Element = RW - 3 - Shop_Margin
        Else
            Shop_Element = RW - 3 - Shop_Margin
            If Shop_Element <> LastSE + 1 Then
                Shop_Element = LastSE + 1
                Shop_Margin = Shop_Margin + 1
            End If
        End If
        If LastPE = 0 Then
            PFT_Element = RW - 3 - PFT_Margin
        Else
            PFT_Element = RW - 3 - PFT_Margin
            If PFT_Element <> LastPE + 1 Then
                PFT_Element = LastPE + 1
                PFT_Margin = PFT_Margin + 1
            End If
        End If
        If LastCE = 0 Then
            CFT_Element = RW - 3 - CFT_Margin
        Else
            CFT_Element = RW - 3 - CFT_Margin
            If CFT_Element <> LastCE + 1 Then
                CFT_Element = LastCE + 1
                CFT_Margin = CFT_Margin + 1
            End If
        End If
        If LastWE = 0 Then
            Swim_Element = RW - 3 - Swim_Margin
        Else
            Swim_Element = RW - 3 - Swim_Margin
            If Swim_Element <> LastWE + 1 Then
                Swim_Element = LastWE + 1
                Swim_Margin = Swim_Margin + 1
            End If
        End If
        If LastBE = 0 Then
            Belt_Element = RW - 3 - Belt_Margin
        Else
            Belt_Element = RW - 3 - Belt_Margin
            If Belt_Element <> LastBE + 1 Then
                Belt_Element = LastBE + 1
                Belt_Margin = Belt_Margin + 1
            End If
        End If
        If LastIE = 0 Then
            Inst_Element = RW - 3 - Inst_Margin
        Else
            Inst_Element = RW - 3 - Inst_Margin
            If Inst_Element <> LastIE + 1 Then
                Inst_Element = LastIE + 1
                Inst_Margin = Inst_Margin + 1
            End If
        End If
                                
        ' Capture each item, compare it to the current list, and then store what doesn't match
        If RW = 3 Then
            UnitList(Unit_Element) = Cells(RW, 27)
            ShopList(Shop_Element) = Cells(RW, 28)
            PFT_List(PFT_Element) = Cells(RW, 52)
            CFT_List(CFT_Element) = Cells(RW, 59)
            Swim_Qual(Swim_Element) = Cells(RW, 61)
            Belt_List(Belt_Element) = Cells(RW, 65) & " " & Cells(RW, 66)
            Belt_Inst(Inst_Element) = Cells(RW, 67)
            '>>Debug.Assert (CFT_List(CFT_Element) <> 1)
            
            ' Class 1 through 4 are already set as options in the Userform; thus, skip them
            If PFT_List(Counter - PFT_Margin) = 1 Or _
               PFT_List(Counter - PFT_Margin) = 2 Or _
               PFT_List(Counter - PFT_Margin) = 3 Or _
               PFT_List(Counter - PFT_Margin) = 4 Then
                PFT_List(Counter - PFT_Margin) = ""
                Same_PFT = True
                PFT_Margin = PFT_Margin + 1
            End If
            
            ' Class 1 through 4 are already set as options in the Userform; thus, skip them
            If CFT_List(Counter - CFT_Margin) = 1 Or _
               CFT_List(Counter - CFT_Margin) = 2 Or _
               CFT_List(Counter - CFT_Margin) = 3 Or _
               CFT_List(Counter - CFT_Margin) = 4 Then
                CFT_List(Counter - CFT_Margin) = ""
                Same_CFT = True
                CFT_Margin = CFT_Margin + 1
            End If
        Else Rem-- Executes after the first iteration of the [For Loop]
            Counter = Counter + 1 '<<---- Count iterations after the 1st loop, to allow for Element Comparisons
            
            UnitList(Unit_Element) = Cells(RW, 27)                              '<<------ Store the Current Unit inside an Array
            ShopList(Shop_Element) = Cells(RW, 28)                              '<<------ Store the Current Shop inside an Array
            PFT_List(PFT_Element) = Cells(RW, 52)                               '<<------ Store the Current PFT Class inside an Array
            CFT_List(CFT_Element) = Cells(RW, 59)                               '<<------ Store the Current CFT Class inside an Array
            Swim_Qual(Swim_Element) = Cells(RW, 61)                             '<<------ Store the Current Swim Qual. Type inside an Array
            Belt_List(Belt_Element) = Cells(RW, 65) & " " & Cells(RW, 66)       '<<------ Store the Current Belt Type inside an Array
            Belt_Inst(Inst_Element) = Cells(RW, 67)                             '<<------ Store the Current Type of Instructor inside an Array
            '>>Debug.Assert (Inst_Element <> 5)
            
            ' Scan each Array for Duplicate Values; if Duplicates were entered, then _
              erase them & shift the Margin of the Array by 1 to the left
            For Target = 1 To Counter
                If Same_Unit = True Then: Exit For
                
                ' These 2 Statements ensure that each Iteration properly steps through each Element of each Array
                Compare = Target - Unit_Margin - 1
                If Compare < 0 Then: Compare = Compare + Unit_Margin '<<---- Shift Element to the right, if negative (e.g. -1)
                
                ' Prevent Element from comparing itself to itself, and thus, deleting itself.
                If Counter - Unit_Margin = Compare Then: Exit For
                                                    
                If UnitList(Counter - Unit_Margin) = UnitList(Compare) Then
                    UnitList(Counter - Unit_Margin) = ""
                    Same_Unit = True
                    Unit_Margin = Unit_Margin + 1
                End If
            Next Target
            For Target = 1 To Counter
                If Same_Shop = True Then: Exit For
                
                ' These 2 Statements ensure that each Iteration properly steps through each Element of each Array
                Compare = Target - Shop_Margin - 1
                If Compare < 0 Then: Compare = Compare + Shop_Margin '<<---- Shift Element to the right, if negative (e.g. -1)
                
                ' Prevent Element from comparing itself to itself, and thus, deleting itself.
                If Counter - Shop_Margin = Compare Then: Exit For
                                                                                                                        
                If ShopList(Counter - Shop_Margin) = ShopList(Compare) Then
                    ShopList(Counter - Shop_Margin) = ""
                    Same_Shop = True
                    Shop_Margin = Shop_Margin + 1
                End If
            Next Target
            For Target = 1 To Counter
                If Same_PFT = True Then: Exit For
                
                ' These 2 Statements ensure that each Iteration properly steps through each Element of each Array
                Compare = Target - PFT_Margin - 1
                If Compare < 0 Then: Compare = Compare + PFT_Margin '<<---- Shift Element to the right, if negative (e.g. -1)
                
                ' Class 1 through 4 are already set as options in the Userform; thus, skip them
                If PFT_List(Counter - PFT_Margin) = 1 Or _
                   PFT_List(Counter - PFT_Margin) = 2 Or _
                   PFT_List(Counter - PFT_Margin) = 3 Or _
                   PFT_List(Counter - PFT_Margin) = 4 Then
                    Rem-- To prevent a subscript error (err 9; a.k.a "fence post error"): remove element value, and then Exit [For Loop]; _
                    the previous compare might shift the element to position 0, and this compare might shift it beyond that (which does not compute)
                    PFT_List(Counter - PFT_Margin) = ""
                    Same_PFT = True
                    PFT_Margin = PFT_Margin + 1
                    Exit For
                End If
                
                ' Prevent Element from comparing itself to itself, and thus, deleting itself.
                If Counter - PFT_Margin = Compare Then: Exit For
                
                If PFT_List(Counter - PFT_Margin) = PFT_List(Compare) Then
                    PFT_List(Counter - PFT_Margin) = ""
                    Same_PFT = True
                    PFT_Margin = PFT_Margin + 1
                End If
            Next Target
            For Target = 1 To Counter
                If Same_CFT = True Then: Exit For
                
                ' These 2 Statements ensure that each Iteration properly steps through each Element of each Array
                Compare = Target - CFT_Margin - 1
                If Compare < 0 Then: Compare = Compare + CFT_Margin '<<---- Shift Element to the right, if negative (e.g. -1)
                                                                                                                                        
                ' Class 1 through 4 are already set as options in the Userform; thus, skip them
                If CFT_List(Counter - CFT_Margin) = 1 Or _
                   CFT_List(Counter - CFT_Margin) = 2 Or _
                   CFT_List(Counter - CFT_Margin) = 3 Or _
                   CFT_List(Counter - CFT_Margin) = 4 Then
                    Rem-- To prevent a subscript error (err 9; a.k.a "fence post error"): remove element value, and then Exit [For Loop]; _
                    the previous compare might shift the element to position 0, and this compare might shift it beyond that (which does not compute)
                    CFT_List(Counter - CFT_Margin) = ""
                    Same_CFT = True
                    CFT_Margin = CFT_Margin + 1
                    Exit For
                End If
                                                                                                                                        
                ' Prevent Element from comparing itself to itself, and thus, deleting itself.
                If Counter - CFT_Margin = Compare Then: Exit For
                                                                                                                                        
                If CFT_List(Counter - CFT_Margin) = CFT_List(Compare) Then
                    CFT_List(Counter - CFT_Margin) = ""
                    Same_CFT = True
                    CFT_Margin = CFT_Margin + 1
                End If
                                
            Next Target
            For Target = 1 To Counter
                If Same_Swim = True Then: Exit For
                
                ' These 2 Statements ensure that each Iteration properly steps through each Element of each Array
                Compare = Target - Swim_Margin - 1
                If Compare < 0 Then: Compare = Compare + Swim_Margin '<<---- Shift Element to the right, if negative (e.g. -1)
                
                ' Prevent Element from comparing itself to itself, and thus, deleting itself.
                If Counter - Swim_Margin = Compare Then: Exit For
                                                                                                                        
                If Swim_Qual(Counter - Swim_Margin) = Swim_Qual(Compare) Then
                    Rem-- To prevent a subscript error (err 9; a.k.a "fence post error"): remove element value, and then Exit [For Loop]; _
                    the previous compare might shift the element to position 0, and this compare might shift it beyond that (which does not compute)
                    Swim_Qual(Counter - Swim_Margin) = ""
                    Same_Swim = True
                    Swim_Margin = Swim_Margin + 1
                End If
            Next Target
            For Target = 1 To Counter
                If Same_Belt = True Then: Exit For
                
                ' These 2 Statements ensure that each Iteration properly steps through each Element of each Array
                Compare = Target - Belt_Margin - 1
                If Compare < 0 Then: Compare = Compare + Belt_Margin '<<---- Shift Element to the right, if negative (e.g. -1)
                
                ' Prevent Element from comparing itself to itself, and thus, deleting itself.
                If Counter - Belt_Margin = Compare Then: Exit For
                                                                                                                        
                If Belt_List(Counter - Belt_Margin) = Belt_List(Compare) Then
                    Rem-- To prevent a subscript error (err 9; a.k.a "fence post error"): remove element value, and then Exit [For Loop]; _
                    the previous compare might shift the element to position 0, and this compare might shift it beyond that (which does not compute)
                    Belt_List(Counter - Belt_Margin) = ""
                    Same_Belt = True
                    Belt_Margin = Belt_Margin + 1
                End If
            Next Target
            For Target = 1 To Counter
                If Same_Inst = True Then: Exit For
                
                ' These 2 Statements ensure that each Iteration properly steps through each Element of each Array
                Compare = Target - Inst_Margin - 1
                If Compare < 0 Then: Compare = Compare + Inst_Margin '<<---- Shift Element to the right, if negative (e.g. -1)
                
                ' Prevent Element from comparing itself to itself, and thus, deleting itself.
                If Counter - Inst_Margin = Compare Then: Exit For
                                                                                                                        
                If Belt_Inst(Counter - Inst_Margin) = Belt_Inst(Compare) Then
                    Rem-- To prevent a subscript error (err 9; a.k.a "fence post error"): remove element value, and then Exit [For Loop]; _
                    the previous compare might shift the element to position 0, and this compare might shift it beyond that (which does not compute)
                    Belt_Inst(Counter - Inst_Margin) = ""
                    Same_Inst = True
                    Inst_Margin = Inst_Margin + 1
                End If
            Next Target
        End If
                
        '========================================================================================================================================
        ' For Debugging Purposes Only ...
        '>>Debug.Assert (Shop_Element <> 7)  'RW = 17
        '>>Debug.Assert (Shop_Element <> 8)  'RW = 21
        '>>Debug.Assert (Shop_Element <> 9)  'RW = 34
        
        '>>Debug.Print "#========================================================================================================#"
        '>>Debug.Print "| The Current Iteration is: " & Counter + 1
        '>>Debug.Print "| The Row being scanned is: " & RW
        '>>Debug.Print "|"
        '>>Debug.Print "+--------------------------------------------------------------------------------------------------------+"
        If UnitList(Unit_Element) = Empty Then
            '>>Debug.Print "| The Current Unit was a Duplicate; thus, it was skipped ...; Element{" & Unit_Element & "}"
        Else
            '>>Debug.Print "| The Current Unit is :" & vbTab & vbTab & "  [" & UnitList(Unit_Element) & "] :: Element{" & Unit_Element & "}"
            LastUE = Unit_Element
        End If
        If ShopList(Shop_Element) = Empty Then
            '>>Debug.Print "| The Current Shop was a Duplicate; thus, it was skipped ...; Element{" & Shop_Element & "}"
        Else
            '>>Debug.Print "| The Current Shop is :" & vbTab & vbTab & "  [" & ShopList(Shop_Element) & "]" & CALC_SPC(UnitList(Unit_Element)) & " :: Element{" & Shop_Element & "}"
            LastSE = Shop_Element
        End If
        If PFT_List(PFT_Element) = Empty Then
            '>>Debug.Print "| The Current PFT Class was a Duplicate; thus, it was skipped ...; Element{" & PFT_Element & "}"
        Else
            '>>Debug.Print "| The Current PFT Class is :  [" & PFT_List(PFT_Element) & "]" & CALC_SPC(UnitList(Unit_Element)) & vbTab & ":: Element{" & PFT_Element & "}"
            LastPE = PFT_Element
        End If
        If CFT_List(CFT_Element) = Empty Then
            '>>Debug.Print "| The Current CFT Class was a Duplicate; thus, it was skipped ...; Element{" & CFT_Element & "}"
        Else
            '>>Debug.Print "| The Current CFT Class is :  [" & CFT_List(CFT_Element) & "]" & CALC_SPC(UnitList(Unit_Element)) & vbTab & ":: Element{" & CFT_Element & "}"
            LastCE = CFT_Element
        End If
        If Swim_Qual(Swim_Element) = Empty Then
            '>>Debug.Print "| The Current Swim Qual. was a Duplicate; thus, it was skipped ...; Element{" & Swim_Element & "}"
        Else
            '>>Debug.Print "| The Current Swim Qual. is : [" & Swim_Qual(Swim_Element) & "]:: Element{" & Swim_Element & "}"
            LastWE = Swim_Element
        End If
        If Belt_List(Belt_Element) = Empty Then
            '>>Debug.Print "| The Current Belt was a Duplicate; thus, it was skipped ...; Element{" & Belt_Element & "}"
        Else
            '>>Debug.Print "| The Current Belt is : " & vbTab & "  [" & Belt_List(Belt_Element) & "]" & CALC_SPC(Swim_Qual(Swim_Element)) & " :: Element{" & Belt_Element & "}"
            LastBE = Belt_Element
        End If
        If Belt_Inst(Inst_Element) = Empty Then
            '>>Debug.Print "| The Current Instructor Type was a Duplicate; thus, it was skipped ...; Element{" & Inst_Element & "}"
        Else
            '>>Debug.Print "| The Current Instructor Type is : [" & Belt_Inst(Inst_Element) & "]" & CALC_SPC(Swim_Qual(Swim_Element)) & " :: Element{" & Inst_Element & "}"
            LastIE = Inst_Element
        End If
        '>>Debug.Print "#========================================================================================================#"
        '>>Debug.Print ""
        '>>Debug.Print RW
    Next RW '<<---- Move on to the next row
    
    With Test
        
        ' Post All Arrays onto Worksheet
        .Activate
        If LastSE <> 0 Then
            For Target = 0 To LastSE
                Set Post_Shop = .Range("A" & 8 + Target)
                Post_Shop.Formula = ShopList(Target)
            Next Target
            For Target = 0 To LastUE
                Set Post_Unit = .Range("C" & 8 + Target)
                Post_Unit.Formula = UnitList(Target)
            Next Target
            For Target = 0 To LastPE
                Set Post_PFT = .Range("H" & 8 + Target)
                Post_PFT.Formula = PFT_List(Target)
            Next Target
            For Target = 0 To LastCE
                Set Post_CFT = .Range("I" & 8 + Target)
                Post_CFT.Formula = CFT_List(Target)
            Next Target
            For Target = 0 To LastWE
                Set Post_Swim = .Range("J" & 8 + Target)
                Post_Swim.Formula = Swim_Qual(Target)
            Next Target
            For Target = 0 To LastBE
                Set Post_Belt = .Range("R" & 8 + Target)
                Post_Belt.Formula = Belt_List(Target)
            Next Target
            For Target = 0 To LastIE
                Set Post_Inst = .Range("S" & 8 + Target)
                Post_Inst.Formula = Belt_Inst(Target)
            Next Target
                    
            ' Alphabetize each List
            Set SortRng = .Range("A8:A" & 8 + LastSE)
            SortRng.Sort key1:=SortRng, order1:=xlAscending, Orientation:=xlSortColumns
            For Target = 0 To LastSE
                ShopList(Target) = .Range("A" & 8 + Target)
            Next Target
            Set SortRng = .Range("C8:C" & 8 + LastUE)
            SortRng.Sort key1:=SortRng, order1:=xlAscending, Orientation:=xlSortColumns
            For Target = 0 To LastUE
                UnitList(Target) = .Range("C" & 8 + Target)
            Next Target
            Set SortRng = .Range("H8:H" & 8 + LastPE)
            SortRng.Sort key1:=SortRng, order1:=xlAscending, Orientation:=xlSortColumns
            For Target = 0 To LastPE
                PFT_List(Target) = .Range("H" & 8 + Target)
            Next Target
            Set SortRng = .Range("I8:I" & 8 + LastCE)
            SortRng.Sort key1:=SortRng, order1:=xlAscending, Orientation:=xlSortColumns
            For Target = 0 To LastCE
                CFT_List(Target) = .Range("I" & 8 + Target)
            Next Target
            Set SortRng = .Range("J8:J" & 8 + LastWE)
            SortRng.Sort key1:=SortRng, order1:=xlAscending, Orientation:=xlSortColumns
            For Target = 0 To LastWE
                Swim_Qual(Target) = .Range("J" & 8 + Target)
            Next Target
            Set SortRng = .Range("R8:R" & 8 + LastBE)
            SortRng.Sort key1:=SortRng, order1:=xlAscending, Orientation:=xlSortColumns
            For Target = 0 To LastBE
                Belt_List(Target) = .Range("R" & 8 + Target)
            Next Target
            Set SortRng = .Range("S8:S" & 8 + LastIE)
            SortRng.Sort key1:=SortRng, order1:=xlAscending, Orientation:=xlSortColumns
            For Target = 0 To LastIE
                Belt_Inst(Target) = .Range("S" & 8 + Target)
            Next Target
            
            ' Add new list to each Array
            For Target = 0 To LastSE
                Set Post_Shop = .Range("A" & 8 + Target)
                Post_Shop.Formula = ShopList(Target)
            Next Target
            For Target = 0 To LastUE
                Set Post_Unit = .Range("C" & 8 + Target)
                Post_Unit.Formula = UnitList(Target)
            Next Target
            For Target = 0 To LastPE
                Set Post_PFT = .Range("H" & 8 + Target)
                Post_PFT.Formula = PFT_List(Target)
            Next Target
            For Target = 0 To LastCE
                Set Post_CFT = .Range("I" & 8 + Target)
                Post_CFT.Formula = CFT_List(Target)
            Next Target
            For Target = 0 To LastWE
                Set Post_Swim = .Range("J" & 8 + Target)
                Post_Swim.Formula = Swim_Qual(Target)
            Next Target
            For Target = 0 To LastBE
                Set Post_Belt = .Range("R" & 8 + Target)
                Post_Belt.Formula = Belt_List(Target)
            Next Target
            For Target = 0 To LastIE
                Set Post_Inst = .Range("S" & 8 + Target)
                Post_Inst.Formula = Belt_Inst(Target)
            Next Target
        End If
    End With
    
    ' Re-activate Database
    Database.Activate
    ThisWorkbook.Sheets(3).Visible = xlSheetVeryHidden
End Sub

' Find the information according to the set criteria, and ouput it to the ListView Control.  This is the Main Sub-Routine; MODULAR PROCEDURE
Sub Send_Query()
    Dim i As Long
    Dim Msg As String, Response As String
        
    Call Set_Query
    
    If Query_Counter = 0 Then
        If UFmain.ComboBox8.ListCount = 0 Then: UFmain.ListView1.ListItems.Clear
        Exit Sub
    Else
        If Get_Ranks = True Then
            Get_Ranks = False
            Ranks_After_Query = True
            Back2Query = True
            Exit Sub
        End If
    End If
    Call Process_Query
        
    ' Is the Name in the Query segmented?
    Msg = "The Textboxes for the Fields: " & vbNewLine & vbNewLine
    Msg = Msg & "First" & vbNewLine & "Middle" & vbNewLine & "Last" & vbNewLine & vbNewLine & "- MUST either be all filled-in, or all blank."
    Msg = Msg & vbNewLine & vbNewLine & "NOTE:" & vbNewLine & "Do not abuse the dash(-) character; use only as a placeholder/separator"
    If Name_Segmented = True Then
        UFmain.ListView1.ListItems.Clear
        Response = MsgBox(Msg, vbOKOnly & vbCritical, "Error: Segmented Name")
        Exit Sub
    End If
    
    ' Does the name in the Query contain proper characters?
    Msg = "You must use alphabetic characters when inputting a name!" & vbNewLine
    Msg = Msg & "(no numbers; no fancy symbols-- JUST letters)" & vbNewLine & vbNewLine
    Msg = Msg & "EXCEPTION:" & vbNewLine & "+  If the Marine has no Middle Initial, then enter a dash(-) next to " & Chr(34) & "MI" & Chr(34)
    Msg = Msg & vbTab & "+  If the Last Name contains a suffix, use dash(-); thus, John II = John-II"
    If Non_Letter = True Then
        UFmain.ListView1.ListItems.Clear
        Response = MsgBox(Msg, vbOKOnly & vbCritical, "Error: Improper Characters")
        Exit Sub
    End If
    
    ' Does the length of the Middle Initial exceed 1?
    Msg = "The Middle initial MUST consist of only one Letter!"
    If One_Middle = False Then
        UFmain.ListView1.ListItems.Clear
        Response = MsgBox(Msg, vbOKOnly & vbCritical, "Error: Too Many Characters")
        Exit Sub
    End If
    
    ' Does the Name even exist in the Database?
    Msg = "The name you entered does NOT exist in the Database!"
    If No_Name = True Then
        UFmain.ListView1.ListItems.Clear
        Response = MsgBox(Msg, vbOKOnly & vbCritical, "Error: Name Does NOT Exist")
        Exit Sub
    End If

    ' Is the EDIPI # numeric?
    Msg = "The EDIPI # must consist of numbers only!"
    If EDIPI_NUM = False Then
        UFmain.ListView1.ListItems.Clear
        Response = MsgBox(Msg, vbOKOnly & vbCritical, "Error: Is that a number!?")
        Exit Sub
    End If
    
    ' Does the length of the EDIPI # equal 10?
    Msg = "The EDIPI # must consist of only 10 digits"
    If EDIPI_TEN = False Then
        UFmain.ListView1.ListItems.Clear
        Response = MsgBox(Msg, vbOKOnly & vbCritical, "Error: Not equivalent to 10")
        Exit Sub
    End If
    
    ' Does the EDIPI # even exist in the Database?
    Msg = "The EDIPI # you entered does NOT exist in the Database!"
    If No_EDIPI = True Then
        UFmain.ListView1.ListItems.Clear
        Response = MsgBox(Msg, vbOKOnly & vbCritical, "Error: EDIPI # Does NOT Exist")
        Exit Sub
    End If
    
    ' Are their too many parameters set in this Query; if so, ask the User to choose either: 'Only one' or 'None'
    Msg = "Too Many parameters have been set for this Query; you may only select options from ONE page from the 'Query Control'"
    If Limit_Exceeded = True Then
        UFmain.ListView1.ListItems.Clear
        Response = MsgBox(Msg, vbOKOnly & vbCritical, "Error: Too Many Parameters Set")
        Exit Sub
    End If

    ' Now that the Query has been returned, filter-out all other items on the ListView control
    If Filter = True Then
        ' A broad scope of several Records
        Call Apply_Filter
    Else
        ' A miniscule scope of one Record
        If ID = True Then
            Name_Address = 1 * STR_SPLIT(Name_Address, "$", 3) - 2 '<<------------------------------------------------- Change Format from Database Address, to ListView Item #
            UFmain.ListView1.ListItems.Item(Name_Address).ForeColor = vbRed '<<---------------------------------------- Isolate the Record associated with the Search Name
            For i = UFmain.ListView1.ListItems.Count To 1 Step -1
                If UFmain.ListView1.ListItems(i).ForeColor <> vbRed Then: UFmain.ListView1.ListItems.Remove (i) '<<---- Remove all the other Records
            Next i
            UFmain.ListView1.ListItems.Item(1).ForeColor = vbGreen '<<------------------------------------------------- Restore the orginal color
            
            ElseIf EDIPI = True Then
                Name_Address = 1 * STR_SPLIT(Name_Address, "$", 3) - 2 '<<------------------------------------------------- Change Format from Database Address, to ListView Item #
                UFmain.ListView1.ListItems.Item(Name_Address).ForeColor = vbRed '<<---------------------------------------- Isolate the Record associated with the Search Name
                For i = UFmain.ListView1.ListItems.Count To 1 Step -1
                    If UFmain.ListView1.ListItems(i).ForeColor <> vbRed Then: UFmain.ListView1.ListItems.Remove (i) '<<---- Remove all the other Records
                Next i
                UFmain.ListView1.ListItems.Item(1).ForeColor = vbGreen '<<------------------------------------------------- Restore the orginal color
        End If
    End If
End Sub

' Capture the values entered by the User
Private Sub Set_Query()
    Dim i As Long
    
    ' Initialize these Boolean variables
    SKIP = False
    UNITS = False
    SHOPS = False
    Rifle = False
    PFT = False
    CFT = False
    SWIM = False
    MCMAP = False
    MCMAP_2 = False
    Limit_Exceeded = False
    
    ' Get all values within the Main Userform to 'form' the Query
    With UFmain
        ' If their are too many parameters to set for the Query, then exit the procedure (# of parameters = Query_Counter)
        Query_Counter = 0 '<<------------------------------------------------------------------------------------- Query_Counter = 0
        Personnel_Arr(0) = .TextBox1.Text
        Personnel_Arr(1) = .TextBox2.Text
        Personnel_Arr(2) = .TextBox3.Text
        Personnel_Arr(3) = .TextBox4.Text
        For i = 0 To 3
            If Personnel_Arr(i) <> "" Then: Query_Counter = 1
        Next i
        
        If Query_Counter = 0 Then
            '>>Personnel_Arr(4) = .TextBox5.Text
            Personnel_Arr(5) = .ComboBox1.Text
            If Personnel_Arr(5) <> "NONE" Then: UNITS = True
            Personnel_Arr(6) = .ComboBox2.Text
            If Personnel_Arr(6) <> "NONE" Then: SHOPS = True
            If UNITS = True Or SHOPS = True Then: Query_Counter = Query_Counter + 1 ''<<-------------------------- Query_Counter = 1
            Rifle_Arr(0) = Marksman
            Rifle_Arr(1) = Sharpshooter
            Rifle_Arr(2) = Expert
            Rifle_Arr(3) = UNQ
            For i = 0 To 3
                If Rifle_Arr(i) <> False Then: Rifle = True
            Next i
            If Rifle = True Then
                Query_Counter = Query_Counter + 1 '<<------------------------------------------- Query_Counter = 2
            ElseIf Rifle_Complete = True Or Rifle_Incomplete = True Then
                Query_Counter = Query_Counter + 1
            End If
            PFT_Arr(0) = PFT_1st
            CFT_Arr(0) = CFT_1st
            PFT_Arr(1) = PFT_2nd
            CFT_Arr(1) = CFT_2nd
            PFT_Arr(2) = PFT_3rd
            CFT_Arr(2) = CFT_3rd
            PFT_Arr(3) = PFT_UNQ
            CFT_Arr(3) = CFT_UNQ
            PFT_Arr(4) = .ComboBox3.Text
            CFT_Arr(4) = .ComboBox4.Text
            For i = 0 To 4
                If i = 4 Then
                    If PFT_Arr(i) <> "NONE" Then: PFT = True
                    If CFT_Arr(i) <> "NONE" Then: CFT = True
                Else
                    If PFT_Arr(i) <> False Then: PFT = True
                    If CFT_Arr(i) <> False Then: CFT = True
                End If
            Next i
            If PFT = True Then
                Query_Counter = Query_Counter + 1 '<<--------------------------------------------- Query_Counter = 3
            ElseIf PFT_Complete = True Or PFT_Incomplete = True Then
                Query_Counter = Query_Counter + 1
            End If
            If CFT = True Then
                Query_Counter = Query_Counter + 1 '<<--------------------------------------------- Query_Counter = 4
            ElseIf CFT_Complete = True Or CFT_Incomplete = True Then
                Query_Counter = Query_Counter + 1
            End If
            Swim_Arr(0) = .ComboBox5.Text
            If Swim_Arr(0) <> "NONE" Then: SWIM = True
            If SWIM = True Then
                Query_Counter = Query_Counter + 1 '<<-------------------------------------------- Query_Counter = 5
            ElseIf Swim_Complete = True Or Swim_Incomplete = True Then
                Query_Counter = Query_Counter + 1
            End If
            MCMAP_Arr(0) = .ComboBox6.Text
            MCMAP_Arr(1) = .ComboBox7.Text
            If MCMAP_Arr(0) <> "NONE" Then: MCMAP = True
            If MCMAP_Arr(1) <> "NONE" Then: MCMAP_2 = True
            If MCMAP = True Or MCMAP_2 = True Then
                Query_Counter = Query_Counter + 1 '<<------------------------- Query_Counter = 6
            ElseIf MCMAP_Complete = True Or MCMAP_Incomplete = True Then
                Query_Counter = Query_Counter + 1
            End If
            
            ' *****  'NBC' is a Global variable and is available to all procedures; thus, it's not listed here, but will be searched for depending on its value  *****
            If NBC_1 = True Or NBC_2 = True Then: Query_Counter = Query_Counter + 1
            
            ' Leave this routine if the number of parameters are too high
            If Query_Counter > 1 Then
                Limit_Exceeded = True
                Exit Sub
                ElseIf Marksman = True And Sharpshooter = True And Expert = True And UNQ = True Then: Limit_Exceeded = True: Exit Sub
                    ElseIf PFT_1st = True And PFT_2nd = True And PFT_3rd = True And PFT_UNQ = True Then: Limit_Exceeded = True: Exit Sub
                        ElseIf CFT_1st = True And CFT_2nd = True And CFT_3rd = True And CFT_UNQ = True Then: Limit_Exceeded = True: Exit Sub
            End If
        End If
    End With
End Sub

' Retrieve the requested items, and set the filter as a record of those items
Private Sub Process_Query()

    ' Declare Program variables
    Dim Program As Workbook
    Dim Database As Worksheet, Log As Worksheet, Test As Worksheet
    
    ' Define Program references
    Set Program = ThisWorkbook
    Set Database = Program.Worksheets("RAW DATA")
    Set Log = Program.Worksheets("LOG")
    Set Test = Program.Worksheets("TEST")

    ' Declare Local variables
    Dim Search_Name As String
    Dim NString As String, Rifle_Code As String
    Dim First As Boolean, Middle As Boolean, Last As Boolean, Suffix As Boolean
    Dim i As Long, j As Long, Last_Cell As Long, Lletter As Long, Uletter As Long, Boost As Long
    
    ' Initialize these variables
    Last_Cell = LR()
    EDIPI = False
    First = False
    Middle = False
    Last = False
    Suffix = False
    No_Name = False
    No_EDIPI = False
    Filter = False
    Name_Segmented = False
    Non_Letter = False
    One_Middle = True
    EDIPI_TEN = True
    EDIPI_NUM = True
    Uppercase = False
    Lowercase = False

    ' Loop through each Collection of Records, and 'record' which ones match the Query
    '>>For i = 1 To Last_Cell - 2

        ' Determine which name segment has been filled in
        If Personnel_Arr(0) = "" Then
            First = False
        Else: First = True
        End If
        If Personnel_Arr(1) = "" Then
            Middle = False
        Else: Middle = True
        End If
        If Personnel_Arr(2) = "" Then
            Last = False
        Else: Last = True
        End If
        
        ' Determine if the name is segmented or not
        If First = False Or Middle = False Or Last = False Then: Name_Segmented = True
        If First = False And Middle = False And Last = False Then: Name_Segmented = False
        If Name_Segmented = True Then: Exit Sub
        
        ' Determine if the strings in the Names' Fields are Letters or not _
          (This will be tricky!  ASCII codes are the key.)
        For i = 0 To 2
            For j = 1 To Len(Personnel_Arr(i))
                ' To prevent the program flow from being interrupted by predictable errors
                On Error Resume Next
                
                ' Test for numbers
                NString = 1 * Mid(Personnel_Arr(i), j, 1)  '<<------ Multiply by 1, to convert the string to an integer
                Non_Letter = Application.WorksheetFunction.IsNonText(NString)
                
                ' A type mismatch error will occur, when 'NString' is evaluated with a string instead of a number
                If Err <> 13 Then
                    If Non_Letter = True Then: GoTo Exit_Point
                End If
                
                ' Reset the Error variable
                Err = 0
                
                ' Test for Uppercase Letters
                For Uletter = 65 To 90
                    NString = Mid(Personnel_Arr(i), j, 1)
                    If NString = Chr(Uletter) Then
                        Uppercase = True
                        Exit For
                    End If
                Next Uletter
                
                ' Test for Lowercase Letters
                For Lletter = 97 To 122
                    NString = Mid(Personnel_Arr(i), j, 1)
                    If NString = Chr(Lletter) Then
                        Lowercase = True
                        Exit For
                    End If
                Next Lletter
                
                ' If any character, within the set of three strings sent by the Query, is not a _
                  part of the alphabet (i.e. !, @, #, $, etc.), then stop Query and prompt error message _
                  (Exception: if the third string (Middle Initial) is a dash(-), allow it as a symbol for "No Middle Initial") _
                  (Exception: if their is an honorific/suffix attached to the last name, as in "John II", then re-format it to "John-II")
                If Uppercase <> True And Lowercase <> True Then
                    If i <> 0 Then
                        If Asc(Mid(Personnel_Arr(i), j, 1)) = 45 Then
                            If i = 1 And j <> Len(Personnel_Arr(i)) Then: Suffix = True
                            If i = 1 And Suffix = False Then
                                Name_Segmented = True
                                GoTo Exit_Point
                            End If
                        Else
                            Non_Letter = True
                            GoTo Exit_Point
                        End If
                    Else
                        Non_Letter = True
                        GoTo Exit_Point
                    End If
                End If
                
                ' Reset these boolean variables for the next iteration
                Lowercase = False
                Uppercase = False
            Next j
        Next i

        ' Determine if the length of the Middle Initial exceeds 1
        If Len(Personnel_Arr(2)) <> 0 Then
            If Len(Personnel_Arr(2)) <> 1 Then
                One_Middle = False
                GoTo Exit_Point
            End If
        End If
                        
        ' Determine if the EDIPI # is numeric or not
        If Len(Personnel_Arr(3)) <> 0 Then
            If IsNumeric(Personnel_Arr(3)) = False Then: EDIPI_NUM = False
            If EDIPI_NUM = False Then: GoTo Exit_Point
        End If
                        
        ' Determine if the length of the EDIPI # is equal to 10 or not
        If Len(Personnel_Arr(3)) <> 0 Then
            If Len(Personnel_Arr(3)) <> 10 Then
                EDIPI_TEN = False
                GoTo Exit_Point
            End If
        End If
                                        
        ' Retrieve all addresses associated with the Query
        With Database
            ' Declare local variables
            Dim clsRecords As New CDB_Filter
            Dim colRecords As New Collection
            '>>Dim FNcount As WorksheetFunction
            
            ' This method will be used repetitively; thus, a new object will be used instead for simplicity
            '>>FNcount = Application.WorksheetFunction.CountIf(FNrange, FNvar)
                        
            ' Construct a Search Name for the Query, and find the location of the string on the spreadsheet
            If First = True Then
                ID = True
                
                ' Re-format the Search Name if their is a suffix attached to the last name
                If Suffix = False Then
                    Search_Name = UCase(Personnel_Arr(1) & "," & Personnel_Arr(0) & " " & Personnel_Arr(2) & ".")
                Else: Search_Name = UCase(STR_SPLIT(Personnel_Arr(1), "-", 1) & STR_SPLIT(Personnel_Arr(1), "-", 2) & ", " & Personnel_Arr(0) & " " & Personnel_Arr(2) & ".")
                End If
                Name_Address = .Range("NAMES").Find(Search_Name).Address
                If Name_Address = Empty Then
                    No_Name = True
                    Exit Sub
                End If
                ' Search for the EDIPI # in the Database, and find the location of the string on the spreadsheet
                ElseIf Personnel_Arr(3) <> "" Then
                    EDIPI = True
                    On Error Resume Next
                    Name_Address = .Range("EDIPI").Find(Personnel_Arr(3)).Address
                    If Err = 91 Then
                        Err = 0
                        No_EDIPI = True
                        Exit Sub
                    End If
            Else
                Filter = True
         '______
         'UNITS |
         '``````|
                '
                If UNITS = True Then
                            
                    'How many cells match the 'Unit' of the Query?
                    Unit_Num = Application.WorksheetFunction.CountIf(.Range("UNITS"), Personnel_Arr(5))
                                                               
                    ' Find the first Address that matches the 'Unit' of the Query
                    NString = .Range("UNITS").Find(Personnel_Arr(5)).Address
                    Set clsRecords = New CDB_Filter
                    clsRecords.Unit_Clm = NString
                    colRecords.Add clsRecords, "UNIT " & CStr(1)
                  
                    ' Find all the Addresses that Match the 'Unit' of the Query
                    For i = 1 To Unit_Num
                        NString = .Range("UNITS").FindNext(after:=Range(NString)).Address
                        Set clsRecords = New CDB_Filter
                        clsRecords.Unit_Clm = NString
                        colRecords.Add clsRecords, "UNIT " & CStr(i + 1)
                    Next i
                End If
         '______
         'SHOPS |
         '``````|
                If SHOPS = True Then
                                            
                    ' How many cells match the 'Shop' of the Query?
                    Shop_Num = Application.WorksheetFunction.CountIf(.Range("SHOPS"), Personnel_Arr(6))
                                                            
                    ' Find the first Address that matches the 'Shop' of the Query
                    NString = .Range("SHOPS").Find(Personnel_Arr(6)).Address
                    Set clsRecords = New CDB_Filter
                    clsRecords.Shop_Clm = NString
                    colRecords.Add clsRecords, "SHOP " & CStr(1)
               
                    ' Find all the Addresses that match the 'Shop' of the Query
                    For i = 1 To Shop_Num
                        NString = .Range("SHOPS").FindNext(after:=Range(NString)).Address
                        Set clsRecords = New CDB_Filter
                        clsRecords.Shop_Clm = NString
                        colRecords.Add clsRecords, "SHOP " & CStr(i + 1)
                    Next i
                End If
         '______
         'RIFLE |
         '``````|
                If Rifle = True Then
        
                    ' How many cells match the 'Rifle Codes' of the Query (Note: there are 4 possible codes)?
                    Rifle_Num = 0
                    For i = 0 To 3
                        If Rifle_Arr(i) = True Then
                            Select Case i
                                Case "0": Rifle_Code = "M"
                                Case "1": Rifle_Code = "S"
                                Case "2": Rifle_Code = "E"
                                Case "3": Rifle_Code = "U"
                            End Select
                            Rifle_Num = Rifle_Num + Application.WorksheetFunction.CountIf(.Range("RQUAL"), "*" & Rifle_Code)
                        End If
                    Next i
                             
                    ' Find all the Addresses that match the 'Rifle Codes' of the Query
                    Boost = 0
                    For j = 0 To 3 '<<---------- Cycle through each type of Rifle Qual
                        If Rifle_Arr(j) <> True Then: GoTo End_Rifle
                        Do
                            For i = 1 + Boost To Rifle_Num
                                If i = 1 + Boost Then
                                    SKIP = True
                                    Select Case j
                                        Case "0": Rifle_Code = "M"
                                        Case "1": Rifle_Code = "S"
                                        Case "2": Rifle_Code = "E"
                                        Case "3": Rifle_Code = "U"
                                    End Select
                                    NString = .Range("RQUAL").Find(Rifle_Code).Address
                                    First_Address = NString
                                    Set clsRecords = New CDB_Filter
                                    clsRecords.Rifle_Clm = NString
                                    colRecords.Add clsRecords, "RIFLE " & Rifle_Code & CStr(i - Boost)
                                Else
                                    NString = .Range("RQUAL").FindNext(after:=Range(NString)).Address
                                    If NString = First_Address Then: Exit For
                                    Set clsRecords = New CDB_Filter
                                    clsRecords.Rifle_Clm = NString
                                    colRecords.Add clsRecords, "RIFLE " & Rifle_Code & CStr(i - Boost)
                                End If
                            Next i
                        Loop While NString <> First_Address And (i + Boost) < Rifle_Num - 1
                        Boost = i - 1
                        SKIP = False
End_Rifle:
                    Next j
                    
                    ' Check if Personnel are T&E Complete
                    ElseIf Rifle_Complete = True Then
                        Boost = 0
                        For i = 0 To Last_Cell
                            ' First, check to see if the cell contents are empty
                            If .Range("I" & i + 3) <> "" Then
                            
                                ' Capture the Rifle date address
                                NString = .Range("I" & i + 3).Address
                                
                                ' Calculate the Expiration Date for proper date comparison
                                If Month(Date) >= 10 Then
                                    Expiration_Year = STR_SPLIT(Date, "/", 3)
                                Else: Expiration_Year = STR_SPLIT(Date, "/", 3) - 1
                                End If
                                Expiration_Date = DateSerial(Expiration_Year, 9, 31)
                                
                                ' Determine if 'Rifle Qual.' is current; if so, then add value to Records
                                If .Range(NString) >= Expiration_Date Then
                                    Set clsRecords = New CDB_Filter
                                    clsRecords.Rifle_Clm = NString
                                    colRecords.Add clsRecords, "RIFLE " & CStr(i + 1) - Boost
                                Else: Boost = Boost + 1
                                End If
                            ' Prevents the Records key in the above statement from skipping a number when a blank field is encountered
                            Else: Boost = Boost + 1
                            End If
                        Next i
                        
                        ' Check if Personnel are T&E Incomplete
                        ElseIf Rifle_Incomplete = True Then
                            Boost = 0
                            For i = 0 To Last_Cell
                                ' First, check to see if the cell contents are empty
                                If .Range("I" & i + 3) <> "" Then
                                
                                    ' Capture the Rifle date address
                                    NString = .Range("I" & i + 3).Address
                                    
                                    ' Calculate the Expiration Date for proper date comparison
                                    If Month(Date) >= 10 Then
                                        Expiration_Year = STR_SPLIT(Date, "/", 3)
                                    Else: Expiration_Year = STR_SPLIT(Date, "/", 3) - 1
                                    End If
                                    Expiration_Date = DateSerial(Expiration_Year, 9, 31)
                                    
                                    ' Determine if 'Rifle Qual.' is current; if so, then add value to Records
                                    If .Range(NString) < Expiration_Date Then
                                        Set clsRecords = New CDB_Filter
                                        clsRecords.Rifle_Clm = NString
                                        colRecords.Add clsRecords, "RIFLE " & CStr(i + 1) - Boost
                                    Else: Boost = Boost + 1
                                    End If
                                ' Prevents the Records key in the above statement from skipping a number when a blank field is encountered
                                Else: Boost = Boost + 1
                                End If
                            Next i
                End If
         '______
         'PFT   |
         '``````|
                If PFT = True Then

                    ' How many cells match the 'PFT Codes' of the Query (Note: there are 4/5 possible codes)?
                    For i = 0 To 4
                        If PFT_Arr(i) = True Then
                            If i <> 4 Then '<<---- The fourth value is not a boolean, but a string that represents an integer
                                PFT_Num = PFT_Num + Application.WorksheetFunction.CountIf(.Range("PQUAL"), i + 1)
                            Else: CFT_Num = PFT_Num + Application.WorksheetFunction.CountIf(.Range("PQUAL"), (1 * PFT_Arr(i) - 1) + 1)
                            End If
                        End If
                    Next i
                             
                    ' Find all the Addresses that match the 'PFT Codes' of the Query
                    Boost = 0
                    For j = 0 To 4 '<<------- Cycle through each type of PFT Qual
                        If PFT_Arr(j) <> True Then GoTo End_PFT
                        Do
                            For i = 1 + Boost To PFT_Num
                                If i = 1 + Boost Then
                                    If j < 4 Then
                                        If SKIP = False Then
                                            SKIP = True
                                            On Error Resume Next
                                            NString = .Range("PQUAL").Find(j + 1).Address
                                            If Err = 91 Then
                                                Exit For
                                            End If
                                            First_Address = NString  '<<---- Prevent the Find Method from revolving back to the first search with this variable
                                            Set clsRecords = New CDB_Filter
                                            clsRecords.PFT_Clm = NString
                                            colRecords.Add clsRecords, "PFT Cl" & j + 1 & " " & CStr(i - Boost)
                                        End If
                                    Else
                                        If SKIP = False Then
                                            SKIP = True
                                            NString = .Range("PQUAL").Find(PFT_Arr(j)).Address
                                            First_Address = NString  '<<---- Prevent the Find Method from revolving back to the first search with this variable
                                            Set clsRecords = New CDB_Filter
                                            clsRecords.PFT_Clm = NString
                                            colRecords.Add clsRecords, "PFT Cl" & j + 1 & " " & CStr(i - Boost)
                                        End If
                                    End If
                                Else
                                    NString = .Range("PQUAL").FindNext(after:=Range(NString)).Address
                                    If NString = First_Address Then: Exit For '<<---- Exit [For Loop] when the Find Method resets
                                    Set clsRecords = New CDB_Filter
                                    clsRecords.PFT_Clm = NString
                                    colRecords.Add clsRecords, "PFT Cl" & j + 1 & " " & CStr(i - Boost)
                                    ' For Debugging Purposes only ...
                                    '>>Debug.Assert (NString <> "$R$373")
                                    '>>Debug.Print "The First Address is:       " & First_Address
                                    '>>Debug.Print "The captured String is:     " & NString
                                    '>>Debug.Print "The current iteration is:      " & CStr(i - Boost)
                                    '>>Debug.Print "The current Class is:          " & Range(NString)
                                    '>>Debug.Print "================================="
                                    '>>Debug.Print ""
                                End If
                            Next i
                        Loop While NString <> First_Address And (i + Boost) < PFT_Num - 1 And Err <> 91
                        Err = 0
                        Boost = i - 1
                        SKIP = False
                        '>>?UFmain.Records.Item("PFT Cl1 1").PFT_clm
End_PFT:
                    Next j
                    
                    ' Check if Personnel are T&E Complete
                    ElseIf PFT_Complete = True Then
                        Boost = 0
                        For i = 0 To Last_Cell
                            ' First, check to see if the cell contents are empty
                            If .Range("O" & i + 3) <> "" Then
                            
                                ' Capture the PFT date address
                                NString = .Range("O" & i + 3).Address
                                
                                ' Calculate the Expiration Date for proper date comparison
                                Expiration_Year = STR_SPLIT(Date, "/", 3) - 1
                                Expiration_Date = DateSerial(Expiration_Year, 12, 31)
                                
                                ' Determine if 'PFT' is current; if so, then add value to Records
                                If .Range(NString) >= Expiration_Date Then
                                    Set clsRecords = New CDB_Filter
                                    clsRecords.PFT_Clm = NString
                                    colRecords.Add clsRecords, "PFT " & CStr(i + 1) - Boost
                                Else: Boost = Boost + 1
                                End If
                            ' Prevents the Records key in the above statement from skipping a number when a blank field is encountered
                            Else: Boost = Boost + 1
                            End If
                        Next i
                        
                        ' Check if Personnel are T&E Incomplete
                        ElseIf PFT_Incomplete = True Then
                            Boost = 0
                            For i = 0 To Last_Cell
                                ' First, check to see if the cell contents are empty
                                If .Range("O" & i + 3) <> "" Then
                                
                                    ' Capture the PFT date address
                                    NString = .Range("O" & i + 3).Address
                                    
                                    ' Calculate the Expiration Date for proper date comparison
                                    Expiration_Year = STR_SPLIT(Date, "/", 3) - 1
                                    Expiration_Date = DateSerial(Expiration_Year, 12, 31)
                                    
                                    ' Determine if 'PFT' is current; if so, then add value to Records
                                    If .Range(NString) < Expiration_Date Then
                                        Set clsRecords = New CDB_Filter
                                        clsRecords.PFT_Clm = NString
                                        colRecords.Add clsRecords, "PFT " & CStr(i + 1) - Boost
                                    Else: Boost = Boost + 1
                                    End If
                                ' Prevents the Records key in the above statement from skipping a number when a blank field is encountered
                                Else: Boost = Boost + 1
                                End If
                            Next i
                End If
         '______
         'CFT   |
         '``````|
                If CFT = True Then

                    ' How many cells match the 'CFT Codes' of the Query (Note: there are 4/5 possible codes)?
                    For i = 0 To 4
                        If CFT_Arr(i) = True Then
                            If i <> 4 Then '<<---- The fourth value is not a boolean, but a string that represents an integer
                                CFT_Num = CFT_Num + Application.WorksheetFunction.CountIf(.Range("CQUAL"), i + 1)
                            Else: CFT_Num = CFT_Num + Application.WorksheetFunction.CountIf(.Range("CQUAL"), (1 * CFT_Arr(i) - 1) + 1)
                            End If
                        End If
                    Next i
                             
                    ' Find all the Addresses that match the 'CFT Codes' of the Query
                    Boost = 0
                    For j = 0 To 4 '<<------- Cycle through each type of CFT Qual
                        If CFT_Arr(j) <> True Then GoTo End_CFT
                        Do
                            For i = 1 + Boost To CFT_Num
                                If i = 1 + Boost Then
                                    If j < 4 Then
                                        If SKIP = False Then
                                            SKIP = True
                                            On Error Resume Next
                                            NString = .Range("CQUAL").Find(j + 1).Address
                                            If Err = 91 Then
                                                Exit For
                                            End If
                                            First_Address = NString  '<<---- Prevent the Find Method from revolving back to the first search with this variable
                                            Set clsRecords = New CDB_Filter
                                            clsRecords.CFT_Clm = NString
                                            colRecords.Add clsRecords, "CFT Cl" & j + 1 & " " & CStr(i - Boost)
                                        End If
                                    Else
                                        If SKIP = False Then
                                            SKIP = True
                                            NString = .Range("CQUAL").Find(CFT_Arr(j)).Address
                                            First_Address = NString  '<<---- Prevent the Find Method from revolving back to the first search with this variable
                                            Set clsRecords = New CDB_Filter
                                            clsRecords.CFT_Clm = NString
                                            colRecords.Add clsRecords, "CFT Cl" & j + 1 & " " & CStr(i - Boost)
                                        End If
                                    End If
                                Else
                                    NString = .Range("CQUAL").FindNext(after:=Range(NString)).Address
                                    If NString = First_Address Then: Exit For '<<---- Exit [For Loop] when the Find Method resets
                                    Set clsRecords = New CDB_Filter
                                    clsRecords.CFT_Clm = NString
                                    colRecords.Add clsRecords, "CFT Cl" & j + 1 & " " & CStr(i - Boost)
                                    ' For Debugging Purposes only ...
                                    '>>Debug.Assert (NString <> "$R$373")
                                    '>>Debug.Print "The First Address is:       " & First_Address
                                    '>>Debug.Print "The captured String is:     " & NString
                                    '>>Debug.Print "The current iteration is:      " & CStr(i - Boost)
                                    '>>Debug.Print "The current Class is:          " & Range(NString)
                                    '>>Debug.Print "================================="
                                    '>>Debug.Print ""
                                End If
                            Next i
                        Loop While NString <> First_Address And (i + Boost) < CFT_Num - 1 And Err <> 91
                        Err = 0
                        Boost = i - 1
                        SKIP = False
                        '>>?UFmain.Records.Item("CFT Cl1 1").CFT_clm
End_CFT:
                    Next j
                    
                    ' Check if Personnel are T&E Complete
                    ElseIf CFT_Complete = True Then
                        Boost = 0
                        For i = 0 To Last_Cell
                            ' First, check to see if the cell contents are empty
                            If .Range("S" & i + 3) <> "" Then
                            
                                ' Capture the CFT date address
                                NString = .Range("S" & i + 3).Address
                                
                                ' Calculate the Expiration Date for proper date comparison
                                If Month(Date) >= 7 Then
                                    Expiration_Year = STR_SPLIT(Date, "/", 3)
                                Else: Expiration_Year = STR_SPLIT(Date, "/", 3) - 1
                                End If
                                Expiration_Date = DateSerial(Expiration_Year, 6, 30)
                                
                                ' Determine if 'CFT' is current; if so, then add value to Records
                                If .Range(NString) >= Expiration_Date Then
                                    Set clsRecords = New CDB_Filter
                                    clsRecords.CFT_Clm = NString
                                    colRecords.Add clsRecords, "CFT " & CStr(i + 1) - Boost
                                Else: Boost = Boost + 1
                                End If
                            ' Prevents the Records key in the above statement from skipping a number when a blank field is encountered
                            Else: Boost = Boost + 1
                            End If
                        Next i
                        
                        ' Check if Personnel are T&E Incomplete
                        ElseIf CFT_Incomplete = True Then
                            Boost = 0
                            For i = 0 To Last_Cell
                                ' First, check to see if the cell contents are empty
                                If .Range("S" & i + 3) <> "" Then
                                
                                    ' Capture the CFT date address
                                    NString = .Range("S" & i + 3).Address
                                    
                                    ' Calculate the Expiration Date for proper date comparison
                                    If Month(Date) >= 7 Then
                                        Expiration_Year = STR_SPLIT(Date, "/", 3)
                                    Else: Expiration_Year = STR_SPLIT(Date, "/", 3) - 1
                                    End If
                                    Expiration_Date = DateSerial(Expiration_Year, 6, 30)
                                    
                                    ' Determine if 'CFT' is current; if so, then add value to Records
                                    If .Range(NString) < Expiration_Date Then
                                        Set clsRecords = New CDB_Filter
                                        clsRecords.CFT_Clm = NString
                                        colRecords.Add clsRecords, "CFT " & CStr(i + 1) - Boost
                                    Else: Boost = Boost + 1
                                    End If
                                ' Prevents the Records key in the above statement from skipping a number when a blank field is encountered
                                Else: Boost = Boost + 1
                                End If
                            Next i
                End If
         '______
         'SWIM  |
         '``````|
                If SWIM = True Then
                    ' How many cells match the 'Swim Code' of the Query?
                    Swim_Num = Application.WorksheetFunction.CountIf(.Range("SQUAL"), Swim_Arr(0))
                                            
                    ' Find the first Address that matches the 'Swim Code' of the Query
                    NString = .Range("SQUAL").Find(Swim_Arr(0), , LookIn:=xlValues, LookAt:=xlWhole).Address  '<<---- added extra parameters for a more specific search (necessary for certain strings)
                    Set clsRecords = New CDB_Filter
                    clsRecords.Swim_Clm = NString
                    colRecords.Add clsRecords, "SWIM " & CStr(1)
                    
                    ' Find all the Addresses that match the 'Swim Code' of the Query
                    For i = 1 To Swim_Num - 1 '<<------ The first one has already been stored; thus, subtract 1 from the range
                        NString = .Range("SQUAL").FindNext(after:=Range(NString)).Address
                        Set clsRecords = New CDB_Filter
                        clsRecords.Swim_Clm = NString
                        colRecords.Add clsRecords, "SWIM " & CStr(i + 1)
                    Next i
                    
                    ' Check if Personnel are T&E Complete
                    ElseIf Swim_Complete = True Then
                        Boost = 0
                        For i = 0 To Last_Cell
                            ' First, check to see if the cell contents are empty
                            If .Range("AB" & i + 3) <> "" Then
                            
                                ' Capture the Swim date address
                                NString = .Range("AB" & i + 3).Address
                                
                                ' Calculate the Expiration Date for proper date comparison
                                '>>Expiration_Year = STR_SPLIT(.Range(NString), "/", 3) + 2
                                '>>Expiration_Date = DateSerial(Expiration_Year, STR_SPLIT(.Range(NString), "/", 1), STR_SPLIT(.Range(NString), "/", 2))
                                ' *****  The Expiration Date has already been calculated  *****
                                
                                ' Determine if 'Swim Qual.' is current; if so, then add value to Records
                                If .Range(NString) >= Date Then
                                    Set clsRecords = New CDB_Filter
                                    clsRecords.Swim_Clm = NString
                                    colRecords.Add clsRecords, "SWIM " & CStr(i + 1) - Boost
                                Else: Boost = Boost + 1
                                End If
                            ' Prevents the Records key in the above statement from skipping a number when a blank field is encountered
                            Else: Boost = Boost + 1
                            End If
                        Next i
                        
                        ' Check if Personnel are T&E Incomplete
                        ElseIf Swim_Incomplete = True Then
                            Boost = 0
                            For i = 0 To Last_Cell
                                ' First, check to see if the cell contents are empty
                                If .Range("AB" & i + 3) <> "" Then
                                
                                    ' Capture the Swim date address
                                    NString = .Range("AB" & i + 3).Address
                                    
                                    ' Calculate the Expiration Date for proper date comparison
                                    '>>Expiration_Year = STR_SPLIT(.Range(NString), "/", 3) + 2
                                    '>>Expiration_Date = DateSerial(Expiration_Year, STR_SPLIT(.Range(NString), "/", 1), STR_SPLIT(.Range(NString), "/", 2))
                                    ' *****  The Expiration Date has already been calculated  *****
                                    
                                    ' Determine if 'Swim Qual.' is current; if so, then add value to Records
                                    If .Range(NString) < Date Then
                                        Set clsRecords = New CDB_Filter
                                        clsRecords.Swim_Clm = NString
                                        colRecords.Add clsRecords, "SWIM " & CStr(i + 1) - Boost
                                    Else: Boost = Boost + 1
                                    End If
                                ' Prevents the Records key in the above statement from skipping a number when a blank field is encountered
                                Else: Boost = Boost + 1
                                End If
                            Next i
                End If
         '______
         'MCMAP |
         '``````|
                If MCMAP = True Or MCMAP_2 = True Then
         
                    ' How many cells match the 'MCMAP Strings' of the Query?
                    MCMAP_Num = 0
                    MCMAP_Num = MCMAP_Num + Application.WorksheetFunction.CountIf(.Range("BELTS"), STR_SPLIT(MCMAP_Arr(0), " ", 1))
                    MCMAP_Num = MCMAP_Num + Application.WorksheetFunction.CountIf(.Range("INSTRUCTORS"), MCMAP_Arr(1))
                    
                    ' Find all the Addresses that match the 'MCMAP Strings' of the Query
                    Boost = 0
                    For j = 0 To 1
                        If MCMAP_Arr(j) = "NONE" Then: GoTo End_MCMAP
                        Do
                            For i = 1 To MCMAP_Num - Boost
                                If i = 1 Then
                                        If j = 0 Then
                                            If SKIP = False Then
                                                SKIP = True
                                                NString = .Range("BELTS").Find(STR_SPLIT(MCMAP_Arr(0), " ", 1)).Address
                                                First_Address = NString
                                                Set clsRecords = New CDB_Filter
                                                clsRecords.Belts_Clm = NString
                                                colRecords.Add clsRecords, "MCMAP B" & CStr(i)
                                            End If
                                        Else
                                            If SKIP = False Then
                                                SKIP = True
                                                NString = .Range("INSTRUCTORS").Find(MCMAP_Arr(1)).Address
                                                First_Address = NString
                                                Set clsRecords = New CDB_Filter
                                                clsRecords.Instructors_Clm = NString
                                                colRecords.Add clsRecords, "MCMAP I" & CStr(i)
                                            End If
                                        End If
                                Else
                                    If j = 0 Then
                                        Set clsRecords = New CDB_Filter
                                        NString = .Range("BELTS").FindNext(after:=Range(NString)).Address
                                        clsRecords.Belts_Clm = NString
                                        colRecords.Add clsRecords, "MCMAP B" & CStr(i)
                                    Else
                                        Set clsRecords = New CDB_Filter
                                        NString = .Range("INSTRUCTORS").FindNext(after:=Range(NString)).Address
                                        clsRecords.Instructors_Clm = NString
                                        colRecords.Add clsRecords, "MCMAP I" & CStr(i)
                                    End If
                                End If
                            Next i
                        Loop While NString <> First_Address And (i + Boost) < MCMAP_Num - 1
                        Boost = i - 2
                        SKIP = False
End_MCMAP:
                    Next j
                End If
         '______
         'NBC   |
         '``````|
                ' Check if Personnel are T&E Complete
                If NBC_1 = True Then
                                                                 
                    ' Step through the 'NBC' Range, and add all addresses to the Collection
                    Boost = 0
                    For i = 0 To Last_Cell
                        ' First, check to see if the cell contents are empty
                        If .Range("AI" & i + 3) <> "" Then
                        
                            ' Capture the NBC date address
                            NString = .Range("AI" & i + 3).Address
                            
                            ' Calculate the Expiration Date for proper date comparison
                            If Month(Date) >= 10 Then
                                Expiration_Year = STR_SPLIT(Date, "/", 3)
                            Else: Expiration_Year = STR_SPLIT(Date, "/", 3) - 1
                            End If
                            Expiration_Date = DateSerial(Expiration_Year, 9, 31)
                            
                            ' Determine if 'Gas Chamber Qual.' is current; if so, then add value to Records
                            If .Range(NString) >= Expiration_Date Then
                                Set clsRecords = New CDB_Filter
                                clsRecords.NBC_Clm = NString
                                colRecords.Add clsRecords, "NBC " & CStr(i + 1) - Boost
                            Else: Boost = Boost + 1
                            End If
                        ' Prevents the Records key in the above statement from skipping a number when a blank field is encountered
                        Else: Boost = Boost + 1
                        End If
                    Next i
                    
                    ' Check if Personnel are T&E Incomplete
                    ElseIf NBC_2 = True Then
                                                                     
                        ' Step through the 'NBC' Range, and add all addresses to the Collection
                        Boost = 0
                        For i = 0 To Last_Cell
                            ' First, check to see if the cell contents are empty
                            If .Range("AI" & i + 3) <> "" Then
                            
                                ' Capture the NBC date address
                                NString = .Range("AI" & i + 3).Address
                                
                                ' Calculate the Expiration Date for proper date comparison
                                If Month(Date) >= 10 Then
                                    Expiration_Year = STR_SPLIT(Date, "/", 3)
                                Else: Expiration_Year = STR_SPLIT(Date, "/", 3) - 1
                                End If
                                Expiration_Date = DateSerial(Expiration_Year, 9, 31)
                                
                                ' Determine if 'Gas Chamber Qual.' is current; if so, then add value to Records
                                If .Range(NString) < Expiration_Date Then
                                    Set clsRecords = New CDB_Filter
                                    clsRecords.NBC_Clm = NString
                                    colRecords.Add clsRecords, "NBC " & CStr(i + 1) - Boost
                                Else: Boost = Boost + 1
                                End If
                            ' Prevents the Records key in the above statement from skipping a number when a blank field is encountered
                            Else: Boost = Boost + 1
                            End If
                        Next i
                End If
            End If
        End With
        
        ' Send all the Items associated to the Query, into a custom-property of the 'Main_Console'
        If Filter = True Then: Set UFmain.Records = colRecords
                
Exit_Point:
End Sub

' Process the filter, and output the associated values to the ListView control
Private Sub Apply_Filter()

    ' Declare Program variables
    Dim Program As Workbook
    Dim Database As Worksheet, Log As Worksheet, Test As Worksheet
    
    ' Define Program references
    Set Program = ThisWorkbook
    Set Database = Program.Worksheets("RAW DATA")
    Set Log = Program.Worksheets("LOG")
    Set Test = Program.Worksheets("TEST")
    
    ' Set local variables
    Dim li As ListItem
    Dim i As Long, j As Long
    Dim NString As String, Rifle_Code As String
    Dim New_Code As Boolean
    Dim Boost As Integer, Booster As Integer, iRow As Integer, First_Num As Integer, Target As Integer
    
    ' Filter out items from the Database List based on the Filter Object I created
    With UFmain.ListView1
        '>>.ListItems.Clear
        '  *****  REMOVE ITEMS BASED ON WHAT RECORDS ARE IN THE FILTER CLASS  *****
        
        ' Highlight or Bold which items are in the Filter Object; remove what is not Highlighted/Bolded; then, restore the original format
        New_Code = True
        Last_Value = 0
        Boost = 1
        If UNITS = True Then
            For i = Boost To UFmain.Records.Count '<<-------------------------------------- Loop through each Record
                '>>Debug.Assert (i <> 252)
                If i = 1 Then
                    NString = UFmain.Records.Item("UNIT " & i).Unit_Clm
                    iRow = 1 * STR_SPLIT(NString, "$", 3)
                    First_Num = iRow '<<--------------------------------------------------- Save the first record to prevent this routine from revolving back to it
                    ' Highlight the Rows that match the Query
                    For Target = 1 To 80
                        If Target = 1 Then: .ListItems(iRow - 2).ForeColor = vbRed
                        If Target = UFmain.ListView1.ColumnHeaders.Count Then: Exit For
                        .ListItems(iRow - 2).ListSubItems(Target).ForeColor = vbRed
                    Next Target
                Else
                    ' If a Shop was added to the query, then an error will occur if the CPU continues to read past the last record of this type
                    On Error GoTo Another_Entry
                    NString = UFmain.Records.Item("UNIT " & i).Unit_Clm
                    iRow = 1 * STR_SPLIT(NString, "$", 3)
                    If iRow = First_Num Then
                        Boost = i
                        GoTo No_Units
                    Else
                        For Target = 1 To 80
                            If Target = 1 Then: .ListItems(iRow - 2).ForeColor = vbRed
                            .ListItems(iRow - 2).ListSubItems(Target).ForeColor = vbRed
                        Next Target
                    End If
                End If
            Next i
Another_Entry:
        Boost = i - 1
        End If
No_Units:
        If SHOPS = True Then '<<----------------------------------------------------------- Loop through each Record
            For i = 1 To (UFmain.Records.Count - Boost) '<<-------------------------------- The variable 'Boost' exists, in order to keep an accurate count, if their is more than 1 Query
                If i = i Then
                    NString = UFmain.Records.Item("SHOP " & i).Shop_Clm
                    iRow = 1 * STR_SPLIT(NString, "$", 3)
                    First_Num = iRow '<<--------------------------------------------------- Save the first record to prevent this routine from revolving back to it
                    For Target = 1 To 80
                        If Target = 1 Then: .ListItems(iRow - 2).Bold = True
                        If Target = UFmain.ListView1.ColumnHeaders.Count Then: Exit For
                        .ListItems(iRow - 2).ListSubItems(Target).Bold = True '<<---------- To prevent the clashing of highlights between different Queries in the same scope: Units=Red; Shops=Bold
                    Next Target
                Else
                    NString = UFmain.Records.Item("SHOP " & i).Shop_Clm
                    iRow = 1 * STR_SPLIT(NString, "$", 3)
                    If iRow = First_Num Then
                        Boost = i
                        SHOPS = False
                        GoTo No_Shops
                    Else
                        For Target = 1 To 80
                            If Target = 1 Then: .ListItems(iRow - 2).Bold = True
                            .ListItems(iRow - 2).ListSubItems(Target).Bold = True
                        Next Target
                    End If
                End If
            Next i
            
            ' Implement only if Shops is true
            If UNITS = False Then
                Last_Cell = LR()
                
                ' Remove all items that have not been highlighted by the Filter
                For i = .ListItems.Count To 1 Step -1  '<<----  *
                    If .ListItems.Item(i).Bold <> True Then: .ListItems.Remove (i)
                Next i
                
                ' Restore the original colors
                For i = .ListItems.Count To 1 Step -1  '<<---- *
                    For Target = 1 To 80
                        If Target = 1 Then: .ListItems(i).Bold = False
                        If Target = UFmain.ListView1.ColumnHeaders.Count Then: Exit For
                        .ListItems(i).ListSubItems(Target).Bold = False
                    Next Target
                Next i
            End If

            ' This piece of code will only be implemented, if there is more than one Query chosen _
              from the Personnel Tab on the Multipage control.
            If UNITS = True Then
                Last_Cell = LR()
                
                ' Remove all items that have not been highlighted by the Filter
                For i = .ListItems.Count To 1 Step -1  '<<----  *
                    If .ListItems.Item(i).ForeColor = vbGreen Then
                        .ListItems.Remove (i)
                    ElseIf .ListItems.Item(i).ForeColor = vbRed Then
                        If .ListItems.Item(i).Bold = False Then
                            .ListItems.Remove (i)
                        'Else: i = i + 1
                        End If
                    End If
                Next i
                
                ' Restore the original colors
                For i = .ListItems.Count To 1 Step -1  '<<---- *
                    For Target = 1 To 80
                        If Target = 1 Then
                            With .ListItems(i)
                                .Bold = False
                                .ForeColor = vbGreen
                            End With
                        End If
                        With .ListItems(i).ListSubItems(Target)
                            .Bold = False
                            .ForeColor = vbGreen
                        End With
                    Next Target
                Next i
            End If
        End If
No_Shops:
        If Rifle = True Then
            j = 0
            Err = 0 '<<-------------------------------------------------------------------- Their will be errors that need to be trapped later on
            ' Loop through each Rifle Record
            For i = Boost To UFmain.Records.Count
                '>>Debug.Assert (i <> 45)
                
                'The count WILL be manipulated, when different codes are involved for the same Query; _
                 thus, Exit the [For Loop] after the last Record-- use the 'Booster' variable to balance the count
                If i >= UFmain.Records.Count + 1 - Booster Then: Exit For
                
                'If the current Rifle Code is not a part of the Query, then move on to the next one
                If Rifle_Arr(j) <> True Then
                    For Target = 0 To 3
                        j = Target
                        If Rifle_Arr(Target) = True Then: Exit For
                    Next Target
                End If
                
                ' A substrict error will occur when reading past the last record _
                 (This may occur if their is more than one code added to the Rifle Query)
                If Err = 5 Then
                    Err = 0
                    For Target = j To 3
                        j = Target
                        If Rifle_Arr(Target) = True Then: Exit For
                    Next Target
                    New_Code = True
                End If
                
                ' If their is another code that is a part of the Rifle Query, then _
                  evaluate the first record associated with it the same way as the last code, and continue forward
                If New_Code = True Then
                    New_Code = False
                    Select Case j
                        Case 0: Rifle_Code = "M"
                        Case 1: Rifle_Code = "S"
                        Case 2: Rifle_Code = "E"
                        Case 3: Rifle_Code = "U"
                    End Select
                    On Error Resume Next
                    
                    'This statement reads the current record; if reading past the last record, an error will occur here
                    NString = UFmain.Records.Item("RIFLE " & Rifle_Code & i).Rifle_Clm
                    
                    ' Evaluate the error object to see if the last statement produced an error; if so, move on to the next code
                    If Err = 5 Then: GoTo Blank_Entry
                    
                    ' If the Query is also filtering the Ranks of the Records, then enter a new condition before continuing
                    If Ranks_After_Query = True Then
                        List_Rank = Filter_Ranks(NString)
                        If List_Rank = True Then
                            GoTo List_Marine
                        Else: GoTo Blank_Entry
                        End If
                    End If
List_Marine:
                    ' Evaluate the first record, and then Highlight it
                    iRow = 1 * STR_SPLIT(NString, "$", 3)
                    First_Num = iRow
                    For Target = 1 To 80
                        If Target = 1 Then: .ListItems(iRow - 2).ForeColor = vbRed
                        .ListItems(iRow - 2).ListSubItems(Target).ForeColor = vbRed
                    Next Target
                Else
                    NString = UFmain.Records.Item("RIFLE " & Rifle_Code & i).Rifle_Clm
                    
                    ' If the Query is also filtering the Ranks of the Records, then enter a new condition before continuing
                    If Ranks_After_Query = True Then
                        List_Rank = Filter_Ranks(NString)
                        If List_Rank = True Then
                            GoTo List_Marine_2
                        Else: GoTo Blank_Entry
                        End If
                    End If
List_Marine_2:
                    iRow = 1 * STR_SPLIT(NString, "$", 3)
                    
                    'If the current row evaluated is the equivalent of the last iteration, then the last entry has been reached
                    If iRow = Last_Value Then
                        If j = 3 Then: Exit For
                        For Target = j To 3
                            j = Target + 1
                            If Rifle_Arr(j) = True Then
                                Booster = Booster + (i - 1)
                                If Booster < 0 Then: Booster = 0
                                i = 0
                                Err = 0
                                New_Code = True
                                GoTo Blank_Entry
                            End If
                        Next Target
                    End If
                    
                    'Evaluate 'Last_Value' in order to check the relation of the [next Record] with the [current Record] upon the next iteration
                    Last_Value = iRow
                    
                    ' As long as current Record does not equal the first, continue
                    If iRow = First_Num Then
                        If j = 3 Then
                            GoTo No_Rifle
                        Else: New_Code = True
                        End If
                    Else
                        For Target = 1 To 80
                            If Target = 1 Then: .ListItems(iRow - 2).ForeColor = vbRed
                            .ListItems(iRow - 2).ListSubItems(Target).ForeColor = vbRed
                        Next Target
                    End If
                End If
Blank_Entry:
            Next i
            
            ' Filter the Database, so that 'T&E Complete Personnel' are visible
            ElseIf Rifle_Complete = True Then
                For i = Boost To UFmain.Records.Count
                    If i = 1 Then
                        NString = UFmain.Records.Item("RIFLE " & i).Rifle_Clm
                        
                        ' If the Query is also filtering the Ranks of the Records, then enter a new condition before continuing
                        If Ranks_After_Query = True Then
                            List_Rank = Filter_Ranks(NString)
                            If List_Rank = True Then
                                GoTo List_Marine_3
                            Else: GoTo Next_Rifle
                            End If
                        End If
List_Marine_3:
                        iRow = 1 * STR_SPLIT(NString, "$", 3)
                        First_Num = iRow
                        For Target = 1 To 80
                            If Target = 1 Then: .ListItems(iRow - 2).ForeColor = vbRed
                            .ListItems(iRow - 2).ListSubItems(Target).ForeColor = vbRed
                        Next Target
                    Else
                        NString = UFmain.Records.Item("RIFLE " & i).Rifle_Clm
                        
                        ' If the Query is also filtering the Ranks of the Records, then enter a new condition before continuing
                        If Ranks_After_Query = True Then
                            List_Rank = Filter_Ranks(NString)
                            If List_Rank = True Then
                                GoTo List_Marine_4
                            Else: GoTo Next_Rifle
                            End If
                        End If
List_Marine_4:
                        iRow = 1 * STR_SPLIT(NString, "$", 3)
                        If iRow = First_Num Then
                            Boost = i
                            Exit For
                        Else
                            For Target = 1 To 80
                                If Target = 1 Then: .ListItems(iRow - 2).ForeColor = vbRed
                                .ListItems(iRow - 2).ListSubItems(Target).ForeColor = vbRed
                            Next Target
                        End If
                    End If
Next_Rifle:
                Next i

                ' Filter the Database, so that 'T&E Complete Personnel' are NOT visible
                ElseIf Rifle_Incomplete = True Then
                    For i = Boost To UFmain.Records.Count
                        If i = 1 Then
                            NString = UFmain.Records.Item("RIFLE " & i).Rifle_Clm
                            
                            ' If the Query is also filtering the Ranks of the Records, then enter a new condition before continuing
                            If Ranks_After_Query = True Then
                                List_Rank = Filter_Ranks(NString)
                                If List_Rank = True Then
                                    GoTo List_Marine_5
                                Else: GoTo Next_Rifle_2
                                End If
                            End If
List_Marine_5:
                            iRow = 1 * STR_SPLIT(NString, "$", 3)
                            First_Num = iRow
                            For Target = 1 To 80
                                If Target = 1 Then: .ListItems(iRow - 2).ForeColor = vbRed
                                .ListItems(iRow - 2).ListSubItems(Target).ForeColor = vbRed
                            Next Target
                        Else
                            NString = UFmain.Records.Item("RIFLE " & i).Rifle_Clm
                            
                            ' If the Query is also filtering the Ranks of the Records, then enter a new condition before continuing
                            If Ranks_After_Query = True Then
                                List_Rank = Filter_Ranks(NString)
                                If List_Rank = True Then
                                    GoTo List_Marine_6
                                Else: GoTo Next_Rifle_2
                                End If
                            End If
List_Marine_6:
                            iRow = 1 * STR_SPLIT(NString, "$", 3)
                            If iRow = First_Num Then
                                Boost = i
                                Exit For
                            Else
                                For Target = 1 To 80
                                    If Target = 1 Then: .ListItems(iRow - 2).ForeColor = vbRed
                                    .ListItems(iRow - 2).ListSubItems(Target).ForeColor = vbRed
                                Next Target
                            End If
                        End If
Next_Rifle_2:
                    Next i
        End If
No_Rifle:
        If PFT = True Then
            j = 1
            Err = 0 '<<-------------------------------------------------------------------- Their will be errors that need to be trapped later on
            ' Loop through each PFT Record
            For i = Boost To UFmain.Records.Count
                '>>Debug.Assert (i <> 69)
                
                'The count WILL be manipulated, when different codes are involved for the same Query; _
                 thus, Exit the [For Loop] after the last Record-- use the 'Booster' variable to balance the count
                If i >= UFmain.Records.Count + 1 - Booster Then: Exit For
                
                ' If their is another code that is a part of the PFT Query, then _
                  evaluate the first record associated with it the same way as the last code, and continue forward
                If New_Code = True Then
                    On Error Resume Next
                    New_Code = False
                    
                    'This statement reads the current record; if reading past the last record, an error will occur here
                    NString = UFmain.Records.Item("PFT Cl" & j & " " & i).PFT_Clm
                    
                    ' If the Query is also filtering the Ranks of the Records, then enter a new condition before continuing
                    If Ranks_After_Query = True Then
                        List_Rank = Filter_Ranks(NString)
                        If List_Rank = True Then
                            GoTo List_Marine_7
                        Else: GoTo Blank_PFT
                        End If
                    End If
List_Marine_7:
                    ' Evaluate the error object to see if the last statement produced an error; _
                      if so, move on to the next code
                    If Err = 5 Then
                        Boost = i - 1
                        If Boost < 0 Then: Boost = 0
                        i = 0
                        Err = 0
                        j = j + 1
                        New_Code = True
                        GoTo Blank_PFT
                    End If
                        
                    ' Evaluate the first record, and then Highlight it
                    iRow = 1 * STR_SPLIT(NString, "$", 3)
                    First_Num = iRow
                    For Target = 1 To 80
                        If Target = 1 Then: .ListItems(iRow - 2).ForeColor = vbRed
                        If Target = UFmain.ListView1.ColumnHeaders.Count Then: Exit For
                        .ListItems(iRow - 2).ListSubItems(Target).ForeColor = vbRed
                    Next Target
                    
                    'Evaluate 'Last_Value' in order to check the relation of the [next Record] with the [current Record] upon the next iteration
                    Last_Value = iRow
                Else
                    NString = UFmain.Records.Item("PFT Cl" & j & " " & i).PFT_Clm
                    
                    ' If the Query is also filtering the Ranks of the Records, then enter a new condition before continuing
                    If Ranks_After_Query = True Then
                        List_Rank = Filter_Ranks(NString)
                        If List_Rank = True Then
                            GoTo List_Marine_8
                        Else: GoTo Blank_PFT
                        End If
                    End If
List_Marine_8:
                    iRow = 1 * STR_SPLIT(NString, "$", 3)
                    
                    'If the current row evaluated is the equivalent of the last iteration, then the last entry has been reached
                    If iRow = Last_Value Then
                        If j = 9 Then: Exit For
                        For Target = j To 9
                            j = Target + 1
                            If PFT_Arr(j - 1) = True Then
                                Booster = Booster + (i - 1)
                                If Booster < 0 Then: Booster = 0
                                i = 0
                                Err = 0
                                New_Code = True
                                GoTo Blank_PFT
                            End If
                        Next Target
                    End If
                    
                    'Evaluate 'Last_Value' in order to check the relation of the [next Record] with the [current Record] upon the next iteration
                    Last_Value = iRow
                    
                    ' As long as current Record does not equal the first, continue
                    If iRow = First_Num Then
                        If j = 9 Then
                            GoTo No_PFT
                        Else: New_Code = True
                        End If
                    Else
                        For Target = 1 To 80
                            If Target = 1 Then: .ListItems(iRow - 2).ForeColor = vbRed
                            If Target = UFmain.ListView1.ColumnHeaders.Count Then: Exit For
                            .ListItems(iRow - 2).ListSubItems(Target).ForeColor = vbRed
                        Next Target
                    End If
                End If
Blank_PFT:
            Next i
            
            ' Filter the Database, so that 'T&E Complete Personnel' are visible
            ElseIf PFT_Complete = True Then
                For i = Boost To UFmain.Records.Count
                    If i = 1 Then
                        NString = UFmain.Records.Item("PFT " & i).PFT_Clm
                        
                        ' If the Query is also filtering the Ranks of the Records, then enter a new condition before continuing
                        If Ranks_After_Query = True Then
                            List_Rank = Filter_Ranks(NString)
                            If List_Rank = True Then
                                GoTo List_Marine
                            Else: GoTo Next_PFT
                            End If
                        End If
List_Marine_9:
                        iRow = 1 * STR_SPLIT(NString, "$", 3)
                        First_Num = iRow
                        For Target = 1 To 80
                            If Target = 1 Then: .ListItems(iRow - 2).ForeColor = vbRed
                            .ListItems(iRow - 2).ListSubItems(Target).ForeColor = vbRed
                        Next Target
                    Else
                        NString = UFmain.Records.Item("PFT " & i).PFT_Clm
                        
                        ' If the Query is also filtering the Ranks of the Records, then enter a new condition before continuing
                        If Ranks_After_Query = True Then
                            List_Rank = Filter_Ranks(NString)
                            If List_Rank = True Then
                                GoTo List_Marine_10
                            Else: GoTo Next_PFT
                            End If
                        End If
List_Marine_10:
                        iRow = 1 * STR_SPLIT(NString, "$", 3)
                        If iRow = First_Num Then
                            Boost = i
                            Exit For
                        Else
                            For Target = 1 To 80
                                If Target = 1 Then: .ListItems(iRow - 2).ForeColor = vbRed
                                .ListItems(iRow - 2).ListSubItems(Target).ForeColor = vbRed
                            Next Target
                       End If
                    End If
Next_PFT:
                Next i

                ' Filter the Database, so that 'T&E Complete Personnel' are NOT visible
                ElseIf PFT_Incomplete = True Then
                    For i = Boost To UFmain.Records.Count
                        If i = 1 Then
                            NString = UFmain.Records.Item("PFT " & i).PFT_Clm
                            
                            ' If the Query is also filtering the Ranks of the Records, then enter a new condition before continuing
                            If Ranks_After_Query = True Then
                                List_Rank = Filter_Ranks(NString)
                                If List_Rank = True Then
                                    GoTo List_Marine_11
                                Else: GoTo Next_PFT_2
                                End If
                            End If
List_Marine_11:
                            iRow = 1 * STR_SPLIT(NString, "$", 3)
                            First_Num = iRow
                            For Target = 1 To 80
                                If Target = 1 Then: .ListItems(iRow - 2).ForeColor = vbRed
                                .ListItems(iRow - 2).ListSubItems(Target).ForeColor = vbRed
                            Next Target
                        Else
                            NString = UFmain.Records.Item("PFT " & i).PFT_Clm
                            
                            ' If the Query is also filtering the Ranks of the Records, then enter a new condition before continuing
                            If Ranks_After_Query = True Then
                                List_Rank = Filter_Ranks(NString)
                                If List_Rank = True Then
                                    GoTo List_Marine_12
                                Else: GoTo Next_PFT_2
                                End If
                            End If
List_Marine_12:
                            iRow = 1 * STR_SPLIT(NString, "$", 3)
                            If iRow = First_Num Then
                                Boost = i
                                Exit For
                            Else
                                For Target = 1 To 80
                                    If Target = 1 Then: .ListItems(iRow - 2).ForeColor = vbRed
                                    .ListItems(iRow - 2).ListSubItems(Target).ForeColor = vbRed
                                Next Target
                           End If
                        End If
Next_PFT_2:
                    Next i
        End If
No_PFT:
        If CFT = True Then
            j = 1
            Err = 0 '<<-------------------------------------------------------------------- Their will be errors that need to be trapped later on
            ' Loop through each CFT Record
            For i = Boost To UFmain.Records.Count
                '>>Debug.Assert (i <> 69)
                
                'The count WILL be manipulated, when different codes are involved for the same Query; _
                 thus, Exit the [For Loop] after the last Record-- use the 'Booster' variable to balance the count
                If i >= UFmain.Records.Count + 1 - Booster Then: Exit For
                
                ' If their is another code that is a part of the CFT Query, then _
                  evaluate the first record associated with it the same way as the last code, and continue forward
                If New_Code = True Then
                    On Error Resume Next
                    New_Code = False
                    
                    'This statement reads the current record; if reading past the last record, an error will occur here
                    NString = UFmain.Records.Item("CFT Cl" & j & " " & i).CFT_Clm
                    
                    ' If the Query is also filtering the Ranks of the Records, then enter a new condition before continuing
                    If Ranks_After_Query = True Then
                        List_Rank = Filter_Ranks(NString)
                        If List_Rank = True Then
                            GoTo List_Marine_13
                        Else: GoTo Blank_CFT
                        End If
                    End If
List_Marine_13:
                    ' Evaluate the error object to see if the last statement produced an error; _
                      if so, move on to the next code
                    If Err = 5 Then
                        Boost = i - 1
                        If Boost < 0 Then: Boost = 0
                        i = 0
                        Err = 0
                        j = j + 1
                        New_Code = True
                        GoTo Blank_CFT
                    End If
                        
                    ' Evaluate the first record, and then Highlight it
                    iRow = 1 * STR_SPLIT(NString, "$", 3)
                    First_Num = iRow
                    For Target = 1 To 80
                        If Target = 1 Then: .ListItems(iRow - 2).ForeColor = vbRed
                        If Target = UFmain.ListView1.ColumnHeaders.Count Then: Exit For
                        .ListItems(iRow - 2).ListSubItems(Target).ForeColor = vbRed
                    Next Target
                    
                    'Evaluate 'Last_Value' in order to check the relation of the [next Record] with the [current Record] upon the next iteration
                    Last_Value = iRow
                Else
                    NString = UFmain.Records.Item("CFT Cl" & j & " " & i).CFT_Clm
                    
                    ' If the Query is also filtering the Ranks of the Records, then enter a new condition before continuing
                    If Ranks_After_Query = True Then
                        List_Rank = Filter_Ranks(NString)
                        If List_Rank = True Then
                            GoTo List_Marine_14
                        Else: GoTo Blank_CFT
                        End If
                    End If
List_Marine_14:
                    iRow = 1 * STR_SPLIT(NString, "$", 3)
                    
                    'If the current row evaluated is the equivalent of the last iteration, then the last entry has been reached
                    If iRow = Last_Value Then
                        If j = 9 Then: Exit For
                        For Target = j To 9
                            j = Target + 1
                            If CFT_Arr(j - 1) = True Then
                                Booster = Booster + (i - 1)
                                If Booster < 0 Then: Booster = 0
                                i = 0
                                Err = 0
                                New_Code = True
                                GoTo Blank_CFT
                            End If
                        Next Target
                    End If
                    
                    'Evaluate 'Last_Value' in order to check the relation of the [next Record] with the [current Record] upon the next iteration
                    Last_Value = iRow
                    
                    ' As long as current Record does not equal the first, continue
                    If iRow = First_Num Then
                        If j = 9 Then
                            GoTo No_CFT
                        Else: New_Code = True
                        End If
                    Else
                        For Target = 1 To 80
                            If Target = 1 Then: .ListItems(iRow - 2).ForeColor = vbRed
                            If Target = UFmain.ListView1.ColumnHeaders.Count Then: Exit For
                            .ListItems(iRow - 2).ListSubItems(Target).ForeColor = vbRed
                        Next Target
                    End If
                End If
Blank_CFT:
            Next i
            
            ' Filter the Database, so that 'T&E Complete Personnel' are visible
            ElseIf CFT_Complete = True Then
                For i = Boost To UFmain.Records.Count
                    If i = 1 Then
                        NString = UFmain.Records.Item("CFT " & i).CFT_Clm
                        
                        ' If the Query is also filtering the Ranks of the Records, then enter a new condition before continuing
                        If Ranks_After_Query = True Then
                            List_Rank = Filter_Ranks(NString)
                            If List_Rank = True Then
                                GoTo List_Marine_15
                            Else: GoTo Next_CFT
                            End If
                        End If
List_Marine_15:
                        iRow = 1 * STR_SPLIT(NString, "$", 3)
                        First_Num = iRow
                        For Target = 1 To 80
                            If Target = 1 Then: .ListItems(iRow - 2).ForeColor = vbRed
                            .ListItems(iRow - 2).ListSubItems(Target).ForeColor = vbRed
                        Next Target
                    Else
                        NString = UFmain.Records.Item("CFT " & i).CFT_Clm
                        
                        ' If the Query is also filtering the Ranks of the Records, then enter a new condition before continuing
                        If Ranks_After_Query = True Then
                            List_Rank = Filter_Ranks(NString)
                            If List_Rank = True Then
                                GoTo List_Marine_16
                            Else: GoTo Next_CFT
                            End If
                        End If
List_Marine_16:
                        iRow = 1 * STR_SPLIT(NString, "$", 3)
                        If iRow = First_Num Then
                            Boost = i
                            Exit For
                        Else
                            For Target = 1 To 80
                                If Target = 1 Then: .ListItems(iRow - 2).ForeColor = vbRed
                                .ListItems(iRow - 2).ListSubItems(Target).ForeColor = vbRed
                            Next Target
                        End If
                    End If
Next_CFT:
                Next i

                ' Filter the Database, so that 'T&E Complete Personnel' are NOT visible
                ElseIf CFT_Incomplete = True Then
                    For i = Boost To UFmain.Records.Count
                        If i = 1 Then
                            NString = UFmain.Records.Item("CFT " & i).CFT_Clm
                            
                            ' If the Query is also filtering the Ranks of the Records, then enter a new condition before continuing
                            If Ranks_After_Query = True Then
                                List_Rank = Filter_Ranks(NString)
                                If List_Rank = True Then
                                    GoTo List_Marine_17
                                Else: GoTo Next_CFT_2
                                End If
                            End If
List_Marine_17:
                            iRow = 1 * STR_SPLIT(NString, "$", 3)
                            First_Num = iRow
                            For Target = 1 To 80
                                If Target = 1 Then: .ListItems(iRow - 2).ForeColor = vbRed
                                .ListItems(iRow - 2).ListSubItems(Target).ForeColor = vbRed
                            Next Target
                        Else
                            NString = UFmain.Records.Item("CFT " & i).CFT_Clm
                            
                            ' If the Query is also filtering the Ranks of the Records, then enter a new condition before continuing
                            If Ranks_After_Query = True Then
                                List_Rank = Filter_Ranks(NString)
                                If List_Rank = True Then
                                    GoTo List_Marine_18
                                Else: GoTo Next_CFT_2
                                End If
                            End If
List_Marine_18:
                            iRow = 1 * STR_SPLIT(NString, "$", 3)
                            If iRow = First_Num Then
                                Boost = i
                                Exit For
                            Else
                                For Target = 1 To 80
                                    If Target = 1 Then: .ListItems(iRow - 2).ForeColor = vbRed
                                    .ListItems(iRow - 2).ListSubItems(Target).ForeColor = vbRed
                                Next Target
                            End If
                        End If
Next_CFT_2:
                    Next i
        End If
No_CFT:
        If SWIM = True Then
            For i = Boost To UFmain.Records.Count
                If i = i Then
                    NString = UFmain.Records.Item("SWIM " & i).Swim_Clm
                    
                    ' If the Query is also filtering the Ranks of the Records, then enter a new condition before continuing
                    If Ranks_After_Query = True Then
                        List_Rank = Filter_Ranks(NString)
                        If List_Rank = True Then
                            GoTo List_Marine_19
                        Else: GoTo Blank_Swim
                        End If
                    End If
List_Marine_19:
                    iRow = 1 * STR_SPLIT(NString, "$", 3)
                    First_Num = iRow
                    For Target = 1 To 80
                        If Target = 1 Then: .ListItems(iRow - 2).ForeColor = vbRed
                        If Target = UFmain.ListView1.ColumnHeaders.Count Then: Exit For
                        .ListItems(iRow - 2).ListSubItems(Target).ForeColor = vbRed
                    Next Target
                Else
                    NString = UFmain.Records.Item("SWIM " & i).Swim_Clm
                    
                    ' If the Query is also filtering the Ranks of the Records, then enter a new condition before continuing
                    If Ranks_After_Query = True Then
                        List_Rank = Filter_Ranks(NString)
                        If List_Rank = True Then
                            GoTo List_Marine_20
                        Else: GoTo Blank_Swim
                        End If
                    End If
List_Marine_20:
                    iRow = 1 * STR_SPLIT(NString, "$", 3)
                    If iRow = First_Num Then
                        Boost = i
                        SWIM = False
                        GoTo No_Swim
                    Else
                        For Target = 1 To 80
                            If Target = 1 Then: .ListItems(iRow - 2).ForeColor = vbRed
                            .ListItems(iRow - 2).ListSubItems(Target).ForeColor = vbRed
                        Next Target
                    End If
                End If
Blank_Swim:
            Next i
            
            ' Filter the Database, so that 'T&E Complete Personnel' are visible
            ElseIf Swim_Complete = True Then
                For i = Boost To UFmain.Records.Count
                    If i = 1 Then
                        NString = UFmain.Records.Item("SWIM " & i).Swim_Clm
                        
                        ' If the Query is also filtering the Ranks of the Records, then enter a new condition before continuing
                        If Ranks_After_Query = True Then
                            List_Rank = Filter_Ranks(NString)
                            If List_Rank = True Then
                                GoTo List_Marine_21
                            Else: GoTo Next_Swim
                            End If
                        End If
List_Marine_21:
                        iRow = 1 * STR_SPLIT(NString, "$", 3)
                        First_Num = iRow
                        For Target = 1 To 80
                            If Target = 1 Then: .ListItems(iRow - 2).ForeColor = vbRed
                            .ListItems(iRow - 2).ListSubItems(Target).ForeColor = vbRed
                        Next Target
                    Else
                        NString = UFmain.Records.Item("SWIM " & i).Swim_Clm
                        
                        ' If the Query is also filtering the Ranks of the Records, then enter a new condition before continuing
                        If Ranks_After_Query = True Then
                            List_Rank = Filter_Ranks(NString)
                            If List_Rank = True Then
                                GoTo List_Marine_22
                            Else: GoTo Next_Swim
                            End If
                        End If
List_Marine_22:
                        iRow = 1 * STR_SPLIT(NString, "$", 3)
                        If iRow = First_Num Then
                            Boost = i
                            Exit For
                        Else
                            For Target = 1 To 80
                                If Target = 1 Then: .ListItems(iRow - 2).ForeColor = vbRed
                                .ListItems(iRow - 2).ListSubItems(Target).ForeColor = vbRed
                            Next Target
                        End If
                    End If
Next_Swim:
                Next i

                ' Filter the Database, so that 'T&E Complete Personnel' are NOT visible
                ElseIf Swim_Incomplete = True Then
                    For i = Boost To UFmain.Records.Count
                        If i = 1 Then
                            NString = UFmain.Records.Item("SWIM " & i).Swim_Clm
                            
                            ' If the Query is also filtering the Ranks of the Records, then enter a new condition before continuing
                            If Ranks_After_Query = True Then
                                List_Rank = Filter_Ranks(NString)
                                If List_Rank = True Then
                                    GoTo List_Marine_23
                                Else: GoTo Next_Swim_2
                                End If
                            End If
List_Marine_23:
                            iRow = 1 * STR_SPLIT(NString, "$", 3)
                            First_Num = iRow
                            For Target = 1 To 80
                                If Target = 1 Then: .ListItems(iRow - 2).ForeColor = vbRed
                                .ListItems(iRow - 2).ListSubItems(Target).ForeColor = vbRed
                            Next Target
                        Else
                            NString = UFmain.Records.Item("SWIM " & i).Swim_Clm
                            
                            ' If the Query is also filtering the Ranks of the Records, then enter a new condition before continuing
                            If Ranks_After_Query = True Then
                                List_Rank = Filter_Ranks(NString)
                                If List_Rank = True Then
                                    GoTo List_Marine_24
                                Else: GoTo Next_Swim_2
                                End If
                            End If
List_Marine_24:
                            iRow = 1 * STR_SPLIT(NString, "$", 3)
                            If iRow = First_Num Then
                                Boost = i
                                Exit For
                            Else
                                For Target = 1 To 80
                                    If Target = 1 Then: .ListItems(iRow - 2).ForeColor = vbRed
                                    .ListItems(iRow - 2).ListSubItems(Target).ForeColor = vbRed
                                Next Target
                            End If
                        End If
Next_Swim_2:
                    Next i
        End If
No_Swim:
        Err = 0
        If MCMAP = True Then
            For i = Boost To UFmain.Records.Count
                If i = i Then
                    On Error Resume Next
                    NString = UFmain.Records.Item("MCMAP B" & i).Belts_Clm
                    
                    ' If the Query is also filtering the Ranks of the Records, then enter a new condition before continuing
                    If Ranks_After_Query = True Then
                        List_Rank = Filter_Ranks(NString)
                        If List_Rank = True Then
                            GoTo List_Marine_25
                        Else: GoTo Blank_MCMAP
                        End If
                    End If
List_Marine_25:
                    If Err = 5 Then
                        Err = 0
                        GoTo No_MCMAP
                    End If
                    iRow = 1 * STR_SPLIT(NString, "$", 3)
                    First_Num = iRow
                    For Target = 1 To 80
                        If Target = 1 Then: .ListItems(iRow - 2).ForeColor = vbRed
                        If Target = UFmain.ListView1.ColumnHeaders.Count Then: Exit For
                        .ListItems(iRow - 2).ListSubItems(Target).ForeColor = vbRed
                    Next Target
                Else
                    NString = UFmain.Records.Item("MCMAP B" & i).Belts_Clm
                    
                    ' If the Query is also filtering the Ranks of the Records, then enter a new condition before continuing
                    If Ranks_After_Query = True Then
                        List_Rank = Filter_Ranks(NString)
                        If List_Rank = True Then
                            GoTo List_Marine_26
                        Else: GoTo Blank_MCMAP
                        End If
                    End If
List_Marine_26:
                    iRow = 1 * STR_SPLIT(NString, "$", 3)
                    If iRow = First_Num Then
                        Boost = i
                        MCMAP = False
                        GoTo No_MCMAP
                    Else
                        For Target = 1 To 80
                            If Target = 1 Then: .ListItems(iRow - 2).ForeColor = vbRed
                            .ListItems(iRow - 2).ListSubItems(Target).ForeColor = vbRed
                        Next Target
                    End If
                End If
Blank_MCMAP:
            Next i
        End If
No_MCMAP:
        Err = 0
        If MCMAP_2 = True Then
            For i = Boost To UFmain.Records.Count
                If i = i Then
                    On Error Resume Next
                    NString = UFmain.Records.Item("MCMAP I" & i).Instructors_Clm
                    
                    ' If the Query is also filtering the Ranks of the Records, then enter a new condition before continuing
                    If Ranks_After_Query = True Then
                        List_Rank = Filter_Ranks(NString)
                        If List_Rank = True Then
                            GoTo List_Marine_27
                        Else: GoTo Next_MCMAP
                        End If
                    End If
List_Marine_27:
                    If Err = 5 Then
                        Err = 0
                        GoTo No_MCMAP_2
                    End If
                    iRow = 1 * STR_SPLIT(NString, "$", 3)
                    First_Num = iRow
                    For Target = 1 To 80
                        If Target = 1 Then: .ListItems(iRow - 2).ForeColor = vbRed
                        If Target = UFmain.ListView1.ColumnHeaders.Count Then: Exit For
                        .ListItems(iRow - 2).ListSubItems(Target).ForeColor = vbRed
                    Next Target
                Else
                    NString = UFmain.Records.Item("MCMAP I" & i).Instructors_Clm
                    
                    ' If the Query is also filtering the Ranks of the Records, then enter a new condition before continuing
                    If Ranks_After_Query = True Then
                        List_Rank = Filter_Ranks(NString)
                        If List_Rank = True Then
                            GoTo List_Marine_28
                        Else: GoTo Next_MCMAP
                        End If
                    End If
List_Marine_28:
                    iRow = 1 * STR_SPLIT(NString, "$", 3)
                    If iRow = First_Num Then
                        Boost = i
                        MCMAP_2 = False
                        GoTo No_MCMAP_2
                    Else
                        For Target = 1 To 80
                            If Target = 1 Then: .ListItems(iRow - 2).ForeColor = vbRed
                            .ListItems(iRow - 2).ListSubItems(Target).ForeColor = vbRed
                        Next Target
                    End If
                End If
Next_MCMAP:
            Next i
        End If
No_MCMAP_2:
        If NBC_1 = True Or NBC_2 = True Then
            For i = Boost To UFmain.Records.Count
                If i = 1 Then
                    NString = UFmain.Records.Item("NBC " & i).NBC_Clm
                    
                    ' If the Query is also filtering the Ranks of the Records, then enter a new condition before continuing
                    If Ranks_After_Query = True Then
                        List_Rank = Filter_Ranks(NString)
                        If List_Rank = True Then
                            GoTo List_Marine_29
                        Else: GoTo Blank_NBC
                        End If
                    End If
List_Marine_29:
                    iRow = 1 * STR_SPLIT(NString, "$", 3)
                    First_Num = iRow
                    For Target = 1 To 80
                        If Target = 1 Then: .ListItems(iRow - 2).ForeColor = vbRed
                        If Target = UFmain.ListView1.ColumnHeaders.Count Then: Exit For
                        .ListItems(iRow - 2).ListSubItems(Target).ForeColor = vbRed
                    Next Target
                Else
                    NString = UFmain.Records.Item("NBC " & i).NBC_Clm
                    
                    ' If the Query is also filtering the Ranks of the Records, then enter a new condition before continuing
                    If Ranks_After_Query = True Then
                        List_Rank = Filter_Ranks(NString)
                        If List_Rank = True Then
                            GoTo List_Marine_30
                        Else: GoTo Blank_NBC
                        End If
                    End If
List_Marine_30:
                    iRow = 1 * STR_SPLIT(NString, "$", 3)
                    If iRow = First_Num Then
                        Boost = i
                        GoTo No_NBC
                    Else
                        For Target = 1 To 80
                            If Target = 1 Then: .ListItems(iRow - 2).ForeColor = vbRed
                            If Target = UFmain.ListView1.ColumnHeaders.Count Then: Exit For
                            .ListItems(iRow - 2).ListSubItems(Target).ForeColor = vbRed
                        Next Target
                    End If
                End If
Blank_NBC:
            Next i
        End If
No_NBC:
        Query = False
        If SHOPS = True And UNITS = True Then: Exit Sub
        If SHOPS = True Then: Exit Sub
        
        Last_Cell = LR()
        
        ' Remove all items that have not been highlighted by the Filter
        For i = .ListItems.Count To 1 Step -1  '<<----  *
            If .ListItems.Item(i).ForeColor = vbGreen Then: .ListItems.Remove (i)
        Next i
        
        ' Restore the original colors
        For i = .ListItems.Count To 1 Step -1  '<<---- *
            For Target = 1 To 80
                If Target = 1 Then
                    With .ListItems(i)
                        .Bold = False
                        .ForeColor = vbGreen
                    End With
                End If
                If Target = UFmain.ListView1.ColumnHeaders.Count Then: Exit For
                With .ListItems(i).ListSubItems(Target)
                    .Bold = False
                    .ForeColor = vbGreen
                End With
                If Err = 35600 Then: Exit For
            Next Target
        Next i
         
         ' *  If you delete items moving forwards, the indexes above the deleted item shift down as each item is removed (causing an 'index out bounds' error)
    End With
End Sub