Rem Attribute VBA_ModuleType=VBAClassModule
Option VBASupport 1
Option ClassModule
Option Explicit

Private msCFT As String
Private msMTC As String
Private msAmmo_Lifts As String
Private msMUF As String
Private msScore As String
Private msClass As String
Private msDescription As String

Public Property Get CFT() As String
    CFT = msCFT
End Property

Public Property Let CFT(ByVal CFT As String)
    msCFT = CFT
End Property

Public Property Get MTC() As String
    MTC = msMTC
End Property

Public Property Let MTC(ByVal MTC As String)
    msMTC = MTC
End Property

Public Property Get Ammo_Lifts() As String
    Ammo_Lifts = msAmmo_Lifts
End Property

Public Property Let Ammo_Lifts(ByVal Ammo_Lifts As String)
    msAmmo_Lifts = Ammo_Lifts
End Property

Public Property Get MUF() As String
    MUF = msMUF
End Property

Public Property Let MUF(ByVal MUF As String)
    msMUF = MUF
End Property

Public Property Get Score() As String
    Score = msScore
End Property

Public Property Let Score(ByVal Score As String)
    msScore = Score
End Property

Public Property Get Class() As String
    Class = msClass
End Property

Public Property Let Class(ByVal Class As String)
    msClass = Class
End Property

Public Property Get Description() As String
    Description = msDescription
End Property

Public Property Let Description(ByVal Description As String)
    msDescription = Description
End Property