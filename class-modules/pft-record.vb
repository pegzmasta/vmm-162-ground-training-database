Rem Attribute VBA_ModuleType=VBAClassModule
Option VBASupport 1
Option ClassModule
Option Explicit

Private msPFT As String
Private msPull_Ups As String
Private msCrunches As String
Private msRun As String
Private msScore As String
Private msClass As String
Private msDescription As String

Public Property Get PFT() As String
    PFT = msPFT
End Property

Public Property Let PFT(ByVal PFT As String)
    msPFT = PFT
End Property

Public Property Get Pull_Ups() As String
    Pull_Ups = msPull_Ups
End Property

Public Property Let Pull_Ups(ByVal Pull_Ups As String)
    msPull_Ups = Pull_Ups
End Property

Public Property Get Crunches() As String
    Crunches = msCrunches
End Property

Public Property Let Crunches(ByVal Crunches As String)
    msCrunches = Crunches
End Property

Public Property Get Run() As String
    Run = msRun
End Property

Public Property Let Run(ByVal Run As String)
    msRun = Run
End Property

Public Property Get Score() As String
    Score = msScore
End Property

Public Property Let Score(ByVal Score As String)
    msScore = Score
End Property

Public Property Get Class() As String
    Class = msClass
End Property

Public Property Let Class(ByVal Class As String)
    msClass = Class
End Property

Public Property Get Description() As String
    Description = msDescription
End Property

Public Property Let Description(ByVal Description As String)
    msDescription = Description
End Property