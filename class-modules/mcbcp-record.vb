Rem Attribute VBA_ModuleType=VBAClassModule
Option VBASupport 1
Option ClassModule
Option Explicit

Private msHeight As String
Private msWeight As String
Private msBody_Fat As String
Private msEffective As String
Private msStatus As String

Public Property Get Height() As String
    Height = msHeight
End Property

Public Property Let Height(ByVal Height As String)
    msHeight = Height
End Property

Public Property Get Weight() As String
    Weight = msWeight
End Property

Public Property Let Weight(ByVal Weight As String)
    msWeight = Weight
End Property

Public Property Get Body_Fat() As String
    Body_Fat = msBody_Fat
End Property

Public Property Let Body_Fat(ByVal Body_Fat As String)
    msBody_Fat = Body_Fat
End Property

Public Property Get Effective() As String
    Effective = msEffective
End Property

Public Property Let Effective(ByVal Effective As String)
    msEffective = Effective
End Property

Public Property Get Status() As String
    Status = msStatus
End Property

Public Property Let Status(ByVal Status As String)
    msStatus = Status
End Property
