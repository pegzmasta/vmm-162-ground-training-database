Rem Attribute VBA_ModuleType=VBAClassModule
Option VBASupport 1
Option ClassModule
Option Explicit

Private msMCMAP As String
Private msQual As String
Private msBelt As String
Private msInstructor As String
Private msRecert As String

Public Property Get MCMAP() As String
    MCMAP = msMCMAP
End Property

Public Property Let MCMAP(ByVal MCMAP As String)
    msMCMAP = MCMAP
End Property

Public Property Get Qual() As String
    Qual = msQual
End Property

Public Property Let Qual(ByVal Qual As String)
    msQual = Qual
End Property

Public Property Get Belt() As String
    Belt = msBelt
End Property

Public Property Let Belt(ByVal Belt As String)
    msBelt = Belt
End Property

Public Property Get Instructor() As String
    Instructor = msInstructor
End Property

Public Property Let Instructor(ByVal Instructor As String)
    msInstructor = Instructor
End Property

Public Property Get Recert() As String
    Recert = msRecert
End Property

Public Property Let Recert(ByVal Recert As String)
    msRecert = Recert
End Property
