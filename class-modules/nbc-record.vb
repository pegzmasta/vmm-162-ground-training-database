Rem Attribute VBA_ModuleType=VBAClassModule
Option VBASupport 1
Option ClassModule
Option Explicit

Private msNBC As String

Public Property Get NBC() As String
    NBC = msNBC
End Property

Public Property Let NBC(ByVal NBC As String)
    msNBC = NBC
End Property
