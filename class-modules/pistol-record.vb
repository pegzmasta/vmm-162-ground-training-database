Rem Attribute VBA_ModuleType=VBAClassModule
Option VBASupport 1
Option ClassModule
Option Explicit

Private msPistol As String
Private msScore As String
Private msCode As String
Private msExpert_Quals As String
Private msExempt As String
Private msExempt_Date As String

Public Property Get Pistol() As String
    Pistol = msPistol
End Property

Public Property Let Pistol(ByVal Pistol As String)
    msPistol = Pistol
End Property

Public Property Get Score() As String
    Score = msScore
End Property

Public Property Let Score(ByVal Score As String)
    msScore = Score
End Property

Public Property Get Code() As String
    Code = msCode
End Property

Public Property Let Code(ByVal Code As String)
    msCode = Code
End Property

Public Property Get Expert_Quals() As String
    Expert_Quals = msExpert_Quals
End Property

Public Property Let Expert_Quals(ByVal Expert_Quals As String)
    msExpert_Quals = Expert_Quals
End Property

Public Property Get Exempt() As String
    Exempt = msExempt
End Property

Public Property Let Exempt(ByVal Exempt As String)
    msExempt = Exempt
End Property

Public Property Get Exempt_Date() As String
    Exempt_Date = msExempt_Date
End Property

Public Property Let Exempt_Date(ByVal Exempt_Date As String)
    msExempt_Date = Exempt_Date
End Property