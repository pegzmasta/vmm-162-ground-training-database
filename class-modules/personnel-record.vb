Rem Attribute VBA_ModuleType=VBAClassModule
Option VBASupport 1
Option ClassModule
Option Explicit

Private msRank As String
Private msName As String
Private msEDIPI As String
Private msSex As String
Private msDOB As String
Private msEMail As String
Private msEd_Level As String
Private msCERT As String
Private msMajor As String

Public Property Get Rank() As String
    Rank = msRank
End Property

Public Property Let Rank(ByVal Rank As String)
    msRank = Rank
End Property

Public Property Get NAME() As String
    NAME = msName
End Property

Public Property Let NAME(ByVal NAME As String)
    msName = NAME
End Property

Public Property Get EDIPI() As String
    EDIPI = msEDIPI
End Property

Public Property Let EDIPI(ByVal EDIPI As String)
    msEDIPI = EDIPI
End Property

Public Property Get Sex() As String
    Sex = msSex
End Property

Public Property Let Sex(ByVal Sex As String)
    msSex = Sex
End Property

Public Property Get DOB() As String
    DOB = msDOB
End Property

Public Property Let DOB(ByVal DOB As String)
    msDOB = DOB
End Property

Public Property Get EMail() As String
    EMail = msEMail
End Property

Public Property Let EMail(ByVal EMail As String)
    msEMail = EMail
End Property

Public Property Get Ed_Level() As String
    Ed_Level = msEd_Level
End Property

Public Property Let Ed_Level(ByVal Ed_Level As String)
    msEd_Level = Ed_Level
End Property

Public Property Get CERT() As String
    CERT = msCERT
End Property

Public Property Let CERT(ByVal CERT As String)
    msCERT = CERT
End Property

Public Property Get Major() As String
    Major = msMajor
End Property

Public Property Let Major(ByVal Major As String)
    msMajor = Major
End Property
