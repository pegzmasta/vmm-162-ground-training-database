Rem Attribute VBA_ModuleType=VBAClassModule
Option VBASupport 1
Option ClassModule
Option Explicit

Private msStatus As String
Private msLimit As String
Private msLimit_Date As String
Private msSTR_CAT As String
Private msSTR_CAT_Date As String
Private msComCasualty As String
Private msComCas_Date As String

Public Property Get Status() As String
    Status = msStatus
End Property

Public Property Let Status(ByVal Status As String)
    msStatus = Status
End Property

Public Property Get Limit() As String
    Limit = msLimit
End Property

Public Property Let Limit(ByVal Limit As String)
    msLimit = Limit
End Property

Public Property Get Limit_Date() As String
    Limit_Date = msLimit_Date
End Property

Public Property Let Limit_Date(ByVal Limit_Date As String)
    msLimit_Date = Limit_Date
End Property

Public Property Get STR_CAT() As String
    STR_CAT = msSTR_CAT
End Property

Public Property Let STR_CAT(ByVal STR_CAT As String)
    msSTR_CAT = STR_CAT
End Property

Public Property Get STR_CAT_Date() As String
    STR_CAT_Date = msSTR_CAT_Date
End Property

Public Property Let STR_CAT_Date(ByVal STR_CAT_Date As String)
    msSTR_CAT_Date = STR_CAT_Date
End Property

Public Property Get ComCasualty() As String
    ComCasualty = msComCasualty
End Property

Public Property Let ComCasualty(ByVal ComCasualty As String)
    msComCasualty = ComCasualty
End Property

Public Property Get ComCas_Date() As String
    ComCas_Date = msComCas_Date
End Property

Public Property Let ComCas_Date(ByVal ComCas_Date As String)
    msComCas_Date = ComCas_Date
End Property