Rem Attribute VBA_ModuleType=VBAClassModule
Option VBASupport 1
Option ClassModule
Option Explicit

Private msOPSEC As String
Private msIAA As String
Private msPTP As String
Private msNLMB As String
Private msSAPR As String
Private msPME_Completed As String
Private msPME As String

Public Property Get OPSEC() As String
    OPSEC = msOPSEC
End Property

Public Property Let OPSEC(ByVal OPSEC As String)
    msOPSEC = OPSEC
End Property

Public Property Get IAA() As String
    IAA = msIAA
End Property

Public Property Let IAA(ByVal IAA As String)
    msIAA = IAA
End Property

Public Property Get PTP() As String
    PTP = msPTP
End Property

Public Property Let PTP(ByVal PTP As String)
    msPTP = PTP
End Property

Public Property Get NLMB() As String
    NLMB = msNLMB
End Property

Public Property Let NLMB(ByVal NLMB As String)
    msNLMB = NLMB
End Property

Public Property Get SAPR() As String
    SAPR = msSAPR
End Property

Public Property Let SAPR(ByVal SAPR As String)
    msSAPR = SAPR
End Property

Public Property Get PME_Completed() As String
    PME_Completed = msPME_Completed
End Property

Public Property Let PME_Completed(ByVal PME_Completed As String)
    msPME_Completed = PME_Completed
End Property

Public Property Get PME() As String
    PME = msPME
End Property

Public Property Let PME(ByVal PME As String)
    msPME = PME
End Property