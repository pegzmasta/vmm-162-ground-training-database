Rem Attribute VBA_ModuleType=VBAClassModule
Option VBASupport 1
Option ClassModule
Option Explicit

Private msRifle As String
Private msTableI As String
Private msTableII As String
Private msScore As String
Private msCode As String
Private msExpert_Quals As String
Private msExempt As String
Private msExempt_Date As String

Public Property Get Rifle() As String
    Rifle = msRifle
End Property

Public Property Let Rifle(ByVal Rifle As String)
    msRifle = Rifle
End Property

Public Property Get TableI() As String
    TableI = msTableI
End Property

Public Property Let TableI(ByVal TableI As String)
    msTableI = TableI
End Property

Public Property Get TableII() As String
    TableII = msTableII
End Property

Public Property Let TableII(ByVal TableII As String)
    msTableII = TableII
End Property

Public Property Get Score() As String
    Score = msScore
End Property

Public Property Let Score(ByVal Score As String)
    msScore = Score
End Property

Public Property Get Code() As String
    Code = msCode
End Property

Public Property Let Code(ByVal Code As String)
    msCode = Code
End Property

Public Property Get Expert_Quals() As String
    Expert_Quals = msExpert_Quals
End Property

Public Property Let Expert_Quals(ByVal Expert_Quals As String)
    msExpert_Quals = Expert_Quals
End Property

Public Property Get Exempt() As String
    Exempt = msExempt
End Property

Public Property Let Exempt(ByVal Exempt As String)
    msExempt = Exempt
End Property

Public Property Get Exempt_Date() As String
    Exempt_Date = msExempt_Date
End Property

Public Property Let Exempt_Date(ByVal Exempt_Date As String)
    msExempt_Date = Exempt_Date
End Property
