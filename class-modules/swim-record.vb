Rem Attribute VBA_ModuleType=VBAClassModule
Option VBASupport 1
Option ClassModule
Option Explicit

Private msSwim As String
Private msStatus As String
Private msRequal As String

Public Property Get SWIM() As String
    SWIM = msSwim
End Property

Public Property Let SWIM(ByVal SWIM As String)
    msSwim = SWIM
End Property

Public Property Get Requal() As String
    Requal = msRequal
End Property

Public Property Let Requal(ByVal Requal As String)
    msRequal = Requal
End Property
