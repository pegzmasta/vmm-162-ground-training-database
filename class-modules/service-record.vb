Rem Attribute VBA_ModuleType=VBAClassModule
Option VBASupport 1
Option ClassModule
Option Explicit

Private msDOR As String
Private msDCTB As String
Private msUnit As String
Private msShop As String
Private msBillet As String
Private msPMOS As String
Private msBMOS As String
Private msAMOS1 As String
Private msAMOS2 As String
Private msSecurity As String

Public Property Get DOR() As String
    DOR = msDOR
End Property

Public Property Let DOR(ByVal DOR As String)
    msDOR = DOR
End Property

Public Property Get DCTB() As String
    DCTB = msDCTB
End Property

Public Property Let DCTB(ByVal DCTB As String)
    msDCTB = DCTB
End Property

Public Property Get Unit() As String
    Unit = msUnit
End Property

Public Property Let Unit(ByVal Unit As String)
    msUnit = Unit
End Property

Public Property Get Shop() As String
    Shop = msShop
End Property

Public Property Let Shop(ByVal Shop As String)
    msShop = Shop
End Property

Public Property Get Billet() As String
    Billet = msBillet
End Property

Public Property Let Billet(ByVal Billet As String)
    msBillet = Billet
End Property

Public Property Get PMOS() As String
    PMOS = msPMOS
End Property

Public Property Let PMOS(ByVal PMOS As String)
    msPMOS = PMOS
End Property

Public Property Get BMOS() As String
    BMOS = msBMOS
End Property

Public Property Let BMOS(ByVal BMOS As String)
    msBMOS = BMOS
End Property

Public Property Get AMOS1() As String
    AMOS1 = msAMOS1
End Property

Public Property Let AMOS1(ByVal AMOS1 As String)
    msAMOS1 = AMOS1
End Property

Public Property Get AMOS2() As String
    AMOS2 = msAMOS2
End Property

Public Property Let AMOS2(ByVal AMOS2 As String)
    msAMOS2 = AMOS2
End Property

Public Property Get Security() As String
    Security = msSecurity
End Property

Public Property Let Security(ByVal Security As String)
    msSecurity = Security
End Property