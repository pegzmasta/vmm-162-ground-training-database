Rem Attribute VBA_ModuleType=VBAClassModule
Option VBASupport 1
Option ClassModule
Option Explicit

Private msOrig_Entry As String
Private msEnlist As String
Private msEAS As String
Private msEOS As String
Private msPEF As String
Private msBonus_PEF As String
Private msCollege_PEF As String

Public Property Get Orig_Entry() As String
    Orig_Entry = msOrig_Entry
End Property

Public Property Let Orig_Entry(ByVal Orig_Entry As String)
    msOrig_Entry = Orig_Entry
End Property

Public Property Get Enlist() As String
    Enlist = msEnlist
End Property

Public Property Let Enlist(ByVal Enlist As String)
    msEnlist = Enlist
End Property

Public Property Get EAS() As String
    EAS = msEAS
End Property

Public Property Let EAS(ByVal EAS As String)
    msEAS = EAS
End Property

Public Property Get EOS() As String
    EOS = msEOS
End Property

Public Property Let EOS(ByVal EOS As String)
    msEOS = EOS
End Property

Public Property Get PEF() As String
    PEF = msPEF
End Property

Public Property Let PEF(ByVal PEF As String)
    msPEF = PEF
End Property

Public Property Get Bonus_PEF() As String
    Bonus_PEF = msBonus_PEF
End Property

Public Property Let Bonus_PEF(ByVal Bonus_PEF As String)
    msBonus_PEF = Bonus_PEF
End Property

Public Property Get College_PEF() As String
    College_PEF = msCollege_PEF
End Property

Public Property Let College_PEF(ByVal College_PEF As String)
    msCollege_PEF = College_PEF
End Property