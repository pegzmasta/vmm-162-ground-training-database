Rem Attribute VBA_ModuleType=VBAClassModule
Option VBASupport 1
Option ClassModule
Option Explicit

Private msUnit_Clm As String
Private msShop_Clm As String
Private msRifle_Clm As String
Private msPFT_Clm As String
Private msCFT_Clm As String
Private msSwim_Clm As String
Private msBelts_Clm As String
Private msInstructors_Clm As String
Private msNBC_Clm As String

Public Property Get Unit_Clm() As String
    Unit_Clm = msUnit_Clm
End Property

Public Property Let Unit_Clm(ByVal sUnit_Clm As String)
    msUnit_Clm = sUnit_Clm
End Property

Public Property Get Shop_Clm() As String
    Shop_Clm = msShop_Clm
End Property

Public Property Let Shop_Clm(ByVal sShop_Clm As String)
    msShop_Clm = sShop_Clm
End Property

Public Property Get Rifle_Clm() As String
    Rifle_Clm = msRifle_Clm
End Property

Public Property Let Rifle_Clm(ByVal sRifle_Clm As String)
    msRifle_Clm = sRifle_Clm
End Property

Public Property Get PFT_Clm() As String
    PFT_Clm = msPFT_Clm
End Property

Public Property Let PFT_Clm(ByVal sPFT_Clm As String)
    msPFT_Clm = sPFT_Clm
End Property

Public Property Get CFT_Clm() As String
    CFT_Clm = msCFT_Clm
End Property

Public Property Let CFT_Clm(ByVal sCFT_Clm As String)
    msCFT_Clm = sCFT_Clm
End Property

Public Property Get Swim_Clm() As String
    Swim_Clm = msSwim_Clm
End Property

Public Property Let Swim_Clm(ByVal sSwim_Clm As String)
    msSwim_Clm = sSwim_Clm
End Property

Public Property Get Belts_Clm() As String
    Belts_Clm = msBelts_Clm
End Property

Public Property Let Belts_Clm(ByVal sBelts_Clm As String)
    msBelts_Clm = sBelts_Clm
End Property

Public Property Get Instructors_Clm() As String
    Instructors_Clm = msInstructors_Clm
End Property

Public Property Let Instructors_Clm(ByVal sInstructors_Clm As String)
    msInstructors_Clm = sInstructors_Clm
End Property

Public Property Get NBC_Clm() As String
    NBC_Clm = msNBC_Clm
End Property

Public Property Let NBC_Clm(ByVal sNBC_Clm As String)
    msNBC_Clm = sNBC_Clm
End Property