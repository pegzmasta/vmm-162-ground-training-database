Rem Attribute VBA_ModuleType=VBADocumentModule
Option VBASupport 1
Public Update As Boolean

Private Sub WorkBook_Open()

' Unprotect Thisworkbook

' Use the xlveryhidden property for the sheets collections
    
    Dim Program As Workbook
    Dim Guide As Worksheet, Log As Worksheet

    Set Program = ThisWorkbook
    Set Guide = Program.Worksheets("RAW DATA")
    Set Log = Program.Worksheets("LOG")
    
    ActiveWindow.Visible = False
    ThisWorkbook.Activate
    Application.ScreenUpdating = True
    Current_User = Application.UserName
    Guide.EnableSelection = xlNoSelection
    
    ' When Program is complete, make it Modal
    '>>Welcome.Show False
    Welcome.Show
    If Guide.Range("C2") <> "" Then: Edit = True
    
    If Current_User = Author_1 Or _
       Current_User = Author_2 Or _
       Current_User = Author_3 Then
        If Log.Range("E2") = "[DEBUG MODE]" Then
            Update = True
            ActiveWindow.Visible = True
            ThisWorkbook.Activate
            Application.WindowState = xlMaximized
            Call Show_VBAProject
        End If
    End If
End Sub

Private Sub Show_VBAProject()
    Call Access_Project
    Call Use_Password
    Call Expand_Components
End Sub

Private Sub Access_Project()
    SendKeys "%{F11}", True
    SendKeys "^r", True
    SendKeys "{DOWN}", True
End Sub

Private Sub Use_Password()
    SendKeys "%p", True
    SendKeys "pegzmasta", True
    SendKeys "{TAB}", True
    SendKeys "{ENTER}", True
End Sub

Private Sub Expand_Components()
    SendKeys "{DOWN 3}", True
    SendKeys "{RIGHT}", True
    SendKeys "{DOWN 3}", True
    SendKeys "{RIGHT}", True
    SendKeys "{DOWN 4}", True
    SendKeys "{RIGHT}", True
    SendKeys "{UP 2}", True
    SendKeys "{ENTER}", True
    SendKeys "^r", True
    SendKeys "{UP 3}", True
    SendKeys "{RIGHT}", True
    SendKeys "{UP}", True
    SendKeys "{RIGHT}", True
    SendKeys "{DOWN 5}", True
    SendKeys "^r{END}", True
    SendKeys "{UP 10}", True
    SendKeys "{ENTER}", True
    SendKeys "^{END}", True
    SendKeys "^{UP 8}", True
End Sub

Private Sub Workbook_BeforeClose(Cancel As Boolean)
    Application.DisplayAlerts = False
    
    Dim Program As Workbook
    Dim Guide As Worksheet, Log As Worksheet

    Set Program = ThisWorkbook
    Set Guide = Program.Worksheets("RAW DATA")
    Set Log = Program.Worksheets("LOG")
    
    Log.Unprotect Password:="pegzmasta"
    
    If Log.Range("E2") <> "" Then: Log.Range("E2") = ""

    If Log.Range("C2") <> "" Then
        Log.Range("C2") = ""
        ActiveCell = Range("A5")
        Log.Protect Password:="pegzmasta", Contents:=True, UserInterfaceOnly:=True
        Log.EnableSelection = xlNoSelection
        ThisWorkbook.Sheets(1).Visible = xlVeryHidden
        Program.Close savechanges:=True
        Application.Quit
        ElseIf Update = True Then
            If Log.Rows(1 & ":" & 1).Locked = False Then
                Log.Rows(1 & ":" & 1).Locked = True
                Log.Rows(2 & ":" & 2).Locked = True
                Log.Rows(3 & ":" & 3).Locked = True
            End If
            Log.Protect Password:="pegzmasta", Contents:=True, UserInterfaceOnly:=True
            ThisWorkbook.Sheets(1).Visible = xlVeryHidden
            Program.Save
            Application.Quit
    End If
    
    If Log.Rows(1 & ":" & 1).Locked = False Then
        Log.Rows(1 & ":" & 1).Locked = True
        Log.Rows(2 & ":" & 2).Locked = True
        Log.Rows(3 & ":" & 3).Locked = True
    End If
    Log.Protect Password:="pegzmasta", Contents:=True, UserInterfaceOnly:=True
    Application.Quit
    Application.DisplayAlerts = False
End Sub