Rem Attribute VBA_ModuleType=VBAFormModule
Option VBASupport 1
Private Sub Label1_Click()
    With Welcome.Label1
        .Caption = vbTab & vbTab & "WELCOME!"
        .BackColor = vbGreen
        With .Font
            .Bold = True
            .Size = 18
        End With
    End With
End Sub

Private Sub Label2_Click()
    
    Dim Msg As String
    
    Msg = "Welcome to the " & Chr(34) & "[VMM-162] Training & Education Shell!" & Chr(34)
    Msg = Msg & vbNewLine & vbNewLine & "This Shell is Program Driven and will allow you to either: View, Update, or Analyze "
    Msg = Msg & "the Training & Education Database on the Share Drive.  Your permissions are calculated the moment you enter the shell, and "
    Msg = Msg & "will dictate which options are accessible." & vbNewLine & vbNewLine
    Msg = Msg & "All of the information concerning your Basic Training Record (BTR) and "
    Msg = Msg & "Basic Individual Record (BIR), will be stored inside of a Database.  MOL contains online copies of your "
    Msg = Msg & "Training & Education Data; the purpose of this program, is to store a local copy of your Training & Education Data for analysis."
    
    Welcome.Label2.Caption = Msg
    Msg = Empty
End Sub

Private Sub Label3_Click()
    With Welcome.Label3
        Dim Rank As Variant
        
        Full_Name = Application.UserName
        Full_Name = User_Name(Full_Name)
        Rank = CALC_Grade(CHK_RANK(Full_Name))
        Full_Name = Proper_Rank(Rank) & " " & UCase(Mid(STR_SPLIT(Full_Name, ",", 1), 1, 1)) & LCase(Mid(STR_SPLIT(Full_Name, ",", 1), 2, Len(Full_Name)))
        .Caption = CALC_SPC(Full_Name, Label3.Width) & Full_Name
        .BackColor = vbGreen
        .ForeColor = vbRed
        With .Font
            .Bold = True
            .Size = 10
        End With
    End With
End Sub

Private Sub CommandButton1_Click()
    Call GTDbase
End Sub

Private Sub CommandButton2_Click()
    Unload Welcome
    ThisWorkbook.Sheets(1).Visible = xlVeryHidden
    Application.Quit
End Sub

Private Sub CommandButton4_Click()
    
    ' Declare Program variables
    Dim Program As Workbook
    Dim Database As Worksheet, Log As Worksheet, Test As Worksheet
    
    ' Define Program references
    Set Program = ThisWorkbook
    Set Database = Program.Worksheets("RAW DATA")
    Set Log = Program.Worksheets("LOG")
    Set Test = Program.Worksheets("TEST")
    
    Welcome.CommandButton4.Enabled = True
    Msg = "Would you like to unlock this worksheet for editing?"
    Response = MsgBox(Msg, 35, My_Title)
    
    If Response = vbYes Then
        Update = True
        Log.Unprotect Password:="pegzmasta"
        Log.Range("C2") = "[EDIT MODE]"
        ElseIf Response = vbNo Then: Update = True
        Else: Update = False
    End If
    If Update = True Then
        Log.Unprotect Password:="pegzmasta"
        ThisWorkbook.Sheets(1).Visible = xlSheetVisible
        Log.Range("E2") = "[DEBUG MODE]"
    End If
    
    If Log.Rows(1 & ":" & 1).Locked = False Then
        Log.Rows(1 & ":" & 1).Locked = True
        Log.Rows(2 & ":" & 2).Locked = True
        Log.Rows(3 & ":" & 3).Locked = True
    End If

    Log.Protect Password:="pegzmasta", Contents:=True, UserInterfaceOnly:=True
    
    If Update = True Then: Unload Welcome
End Sub

Private Sub UserForm_Initialize()
    Dim Current_User As String
    
    Application.WindowState = xlMinimized
    Call Label1_Click
    Call Label2_Click
    Call Label3_Click
    
    Current_User = Application.UserName
    If Current_User = Author_1 Then
        Welcome.CommandButton4.Enabled = True
        ElseIf Current_User = Author_2 Then
            Welcome.CommandButton4.Enabled = True
            ElseIf Current_User = Author_3 Then
                Welcome.CommandButton4.Enabled = True
    Else: Welcome.CommandButton4.Enabled = False
    End If
End Sub

Private Sub UserForm_QueryClose(Cancel As Integer, _
  CloseMode As Integer)
  If CloseMode = vbFormControlMenu Then
    Cancel = True
    MsgBox "Please press the " & Chr(34) & "Exit" & Chr(34) & " button!"
  End If
End Sub
