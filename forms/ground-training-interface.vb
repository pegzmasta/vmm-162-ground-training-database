Rem Attribute VBA_ModuleType=VBAFormModule
Option VBASupport 1
Option Explicit

Public Query As Boolean

Private StrArray() As Variant
Private Options_Num As Integer

Private Remove_Option As Boolean
Private No_Trigger As Boolean
Private Get_Tables As Boolean
Private Scope_Toggle(2) As Boolean
Private Config_Option(5) As Boolean
Private Personnel_Only As Boolean
Private Get_Duty_Status As Boolean
Private Get_Contract As Boolean
Private Get_Service As Boolean
Private Get_Education As Boolean
Private Get_BCP As Boolean
Private Get_PFT As Boolean
Private Get_CFT As Boolean
Private Get_Swim As Boolean
Private Get_MCMAP As Boolean
Private Get_NBC As Boolean
Private Get_Rifle As Boolean
Private Get_Pistol As Boolean

Private mcolRecords As Collection
Private mcolPersonnel As Collection
Private mcolDuty_status As Collection
Private mcolContract As Collection
Private mcolService As Collection
Private mcolEducation As Collection
Private mcolBCP As Collection
Private mcolPFT As Collection
Private mcolCFT As Collection
Private mcolSwim As Collection
Private mcolMCMAP As Collection
Private mcolNBC As Collection
Private mcolRifle As Collection
Private mcolPistol As Collection

Private Sub CheckBox1_Click()
    Dim TButton As Integer
    Static Active As Boolean
    Active = Not Active
    
    If Active = True Then
        With Me
            Frame13.Visible = True
            With .ToggleButton17
                .Value = True
                .Value = False
            End With
            For TButton = 1 To 4
                With Me.Controls("ToggleButton" & TButton)
                    .Enabled = False
                End With
            Next TButton
        End With
    Else
        Me.Frame13.Visible = False
        Rifle_Complete = False
        Rifle_Incomplete = False
        For TButton = 1 To 4
            With Me.Controls("ToggleButton" & TButton)
                .Enabled = True
            End With
        Next TButton
    End If
End Sub

Private Sub CheckBox2_Click()
    Dim TButton As Integer
    Static Active As Boolean
    Active = Not Active
    
    If Active = True Then
        With Me
            .Frame14.Visible = True
            With .ToggleButton18
                .Value = True
                .Value = False
            End With
            For TButton = 5 To 8
                With Me.Controls("ToggleButton" & TButton)
                    .Enabled = False
                End With
            Next TButton
            .ComboBox3.Enabled = False
            .ComboBox3.Text = "NONE"
        End With
    Else
        Me.Frame14.Visible = False
        PFT_Complete = False
        PFT_Incomplete = False
        For TButton = 5 To 8
            With Me.Controls("ToggleButton" & TButton)
                .Enabled = True
            End With
        Next TButton
        Me.ComboBox3.Enabled = True
    End If
End Sub

Private Sub CheckBox3_Click()
    Dim TButton As Integer
    Static Active As Boolean
    Active = Not Active
    
    If Active = True Then
        With Me
            Frame15.Visible = True
            With .ToggleButton19
                .Value = True
                .Value = False
            End With
            For TButton = 9 To 12
                With Me.Controls("ToggleButton" & TButton)
                    .Enabled = False
                End With
            Next TButton
            .ComboBox4.Enabled = False
            .ComboBox4.Text = "NONE"
        End With
    Else
        Me.Frame15.Visible = False
        CFT_Complete = False
        CFT_Incomplete = False
        For TButton = 9 To 12
            With Me.Controls("ToggleButton" & TButton)
                .Enabled = True
            End With
        Next TButton
        Me.ComboBox4.Enabled = True
    End If
End Sub

Private Sub CheckBox4_Click()
    Dim TButton As Integer
    Static Active As Boolean
    Active = Not Active
    
    If Active = True Then
        With Me
            Frame16.Visible = True
            With .ToggleButton20
                .Value = True
                .Value = False
            End With
            .ComboBox5.Enabled = False
            .ComboBox5.Text = "NONE"
        End With
    Else
        Me.Frame16.Visible = False
        Swim_Complete = False
        Swim_Incomplete = False
        Me.ComboBox5.Enabled = True
    End If
End Sub

Private Sub ComboBox1_Change()
    Me.ComboBox1.MatchEntry = fmMatchEntryComplete
End Sub

Private Sub ComboBox1_AfterUpdate()
    Dim Msg As String, Response As String
    If Me.ComboBox1.MatchFound = False Then
        Msg = "Your input does not match any of the Entries in this control!"
        Response = MsgBox(Msg, vbOKOnly & vbInformation, "Error: Bad Input!")
        Me.ComboBox1.Text = "NONE"
    End If
End Sub

Private Sub ComboBox2_Change()
    Me.ComboBox2.MatchEntry = fmMatchEntryComplete
End Sub

Private Sub ComboBox2_AfterUpdate()
    Dim Msg As String, Response As String
    If Me.ComboBox2.MatchFound = False Then
        Msg = "Your input does not match any of the Entries in this control!"
        Response = MsgBox(Msg, vbOKOnly & vbInformation, "Error: Bad Input!")
        Me.ComboBox2.Text = "NONE"
    End If
End Sub

Private Sub ComboBox3_Change()
    Me.ComboBox3.MatchEntry = fmMatchEntryComplete
End Sub

Private Sub ComboBox3_AfterUpdate()
    Dim Msg As String, Response As String
    If Me.ComboBox3.MatchFound = False Then
        Msg = "Your input does not match any of the Entries in this control!"
        Response = MsgBox(Msg, vbOKOnly & vbInformation, "Error: Bad Input!")
        Me.ComboBox3.Text = "NONE"
    End If
End Sub

Private Sub ComboBox4_Change()
    Me.ComboBox4.MatchEntry = fmMatchEntryComplete
End Sub

Private Sub ComboBox4_AfterUpdate()
    Dim Msg As String, Response As String
    If Me.ComboBox4.MatchFound = False Then
        Msg = "Your input does not match any of the Entries in this control!"
        Response = MsgBox(Msg, vbOKOnly & vbInformation, "Error: Bad Input!")
        Me.ComboBox4.Text = "NONE"
    End If
End Sub

Private Sub ComboBox5_Change()
    Me.ComboBox5.MatchEntry = fmMatchEntryComplete
End Sub

Private Sub ComboBox5_AfterUpdate()
    Dim Msg As String, Response As String
    If Me.ComboBox5.MatchFound = False Then
        Msg = "Your input does not match any of the Entries in this control!"
        Response = MsgBox(Msg, vbOKOnly & vbInformation, "Error: Bad Input!")
        Me.ComboBox5.Text = "NONE"
    End If
End Sub

Private Sub ComboBox6_Change()
    Me.ComboBox6.MatchEntry = fmMatchEntryComplete
End Sub

Private Sub ComboBox6_AfterUpdate()
    Dim Msg As String, Response As String
    If Me.ComboBox6.MatchFound = False Then
        Msg = "Your input does not match any of the Entries in this control!"
        Response = MsgBox(Msg, vbOKOnly & vbInformation, "Error: Bad Input!")
        Me.ComboBox6.Text = "NONE"
    End If
End Sub

Private Sub ComboBox7_Change()
    Me.ComboBox7.MatchEntry = fmMatchEntryComplete
End Sub

Private Sub ComboBox7_AfterUpdate()
    Dim Msg As String, Response As String
    If Me.ComboBox7.MatchFound = False Then
        Msg = "Your input does not match any of the Entries in this control!"
        Response = MsgBox(Msg, vbOKOnly & vbInformation, "Error: Bad Input!")
        Me.ComboBox7.Text = "NONE"
    End If
End Sub

Private Sub ComboBox8_KeyPress(ByVal KeyAscii As MSForms.ReturnInteger)
    If KeyAscii = 114 Then: Remove_Option = True
    If Remove_Option = True Then
        Me.ComboBox8 = "Remove#-"
        If IsNumeric(Chr(KeyAscii)) = True Then
            If Chr(KeyAscii) > Me.ComboBox8.ListCount Then Exit Sub
            Me.ComboBox8.RemoveItem (Chr(KeyAscii) - 1)
            Remove_Option = False
            SendKeys "{ESC}", True
        End If
    End If
End Sub

Sub CommandButton1_Click()
    Dim Msg As String, Response As String
    
    ' You can't view the Database
    If Database_Empty = True Then
        If Query = False Then
            Msg = "There is no Database to view.  Select the " & Chr(34) & "Update Button" & Chr(34) & " to create a new Database with all the significant data from MOL."
            Response = MsgBox(Msg, vbOKOnly & vbExclamation, "No Database to View!")
        Else
            Msg = "You can't Query this program, when there is no Database to extract information from!?  Create a new Database by Selecting the " & Chr(34) & "Update Button." & Chr(34)
            Response = MsgBox(Msg, vbOKOnly & vbExclamation, "No Database to Extract Data from!")
        End If
        Exit Sub
    End If
    
    Dim i As Long
    Dim li As ListItem
    
    If Personnel_Only = True Then: Get_Tables = True
    
    If Get_Tables = False Then
        Get_Duty_Status = True
        Get_Contract = True
        Get_Service = True
        Get_Education = True
        Get_BCP = True
        Get_PFT = True
        Get_CFT = True
        Get_Swim = True
        Get_MCMAP = True
        Get_NBC = True
        Get_Rifle = True
        Get_Pistol = True
    End If
    
    With UFmain.ListView1
        .ListItems.Clear
        .ColumnHeaders.Clear
        
        ' Add Columns! | _
        ````````````````
        .ColumnHeaders.Add , , "RANK", UFmain.ToggleButton14.Width
        .ColumnHeaders.Add , , "FULL NAME", UFmain.TextBox1.Width + UFmain.ToggleButton14.Width
        .ColumnHeaders.Add , , "EDIPI", UFmain.TextBox4.Width
        .ColumnHeaders.Add , , "SEX", UFmain.ToggleButton14.Width
        .ColumnHeaders.Add , , "DOB", UFmain.TextBox4.Width
        .ColumnHeaders.Add , , "E-MAIL", UFmain.TextBox1.Width + UFmain.ToggleButton14.Width
        .ColumnHeaders.Add , , "CIVILIAN ED LEVEL", UFmain.TextBox4.Width + 20
        .ColumnHeaders.Add , , "CERT", UFmain.TextBox4.Width
        .ColumnHeaders.Add , , "MAJOR", UFmain.TextBox1.Width + UFmain.ToggleButton14.Width
        
        If Get_Duty_Status = True Then
            .ColumnHeaders.Add , , "STATUS", UFmain.TextBox4.Width
            .ColumnHeaders.Add , , "LIMIT", UFmain.TextBox1.Width + UFmain.ToggleButton14.Width
            .ColumnHeaders.Add , , "DUTY LIMIT ED", UFmain.TextBox4.Width
            .ColumnHeaders.Add , , "STR CAT", UFmain.TextBox1.Width + UFmain.ToggleButton14.Width * 2
            .ColumnHeaders.Add , , "STR CAT ED", UFmain.TextBox4.Width
            .ColumnHeaders.Add , , "COMBAT CAS", UFmain.TextBox4.Width
            .ColumnHeaders.Add , , "COMBAT CAS ED", UFmain.TextBox4.Width
        End If
        
        If Get_Contract = True Then
            .ColumnHeaders.Add , , "ORIG ENTRY", UFmain.TextBox4.Width
            .ColumnHeaders.Add , , "ENLIST", UFmain.TextBox4.Width
            .ColumnHeaders.Add , , "EAS", UFmain.TextBox4.Width
            .ColumnHeaders.Add , , "EOS", UFmain.TextBox4.Width
            .ColumnHeaders.Add , , "PEF", 2 * (UFmain.TextBox1.Width + UFmain.ToggleButton14.Width)
            .ColumnHeaders.Add , , "BONUS PEF", 2 * (UFmain.TextBox1.Width + UFmain.ToggleButton14.Width)
            .ColumnHeaders.Add , , "COLLEGE PEF", 2 * (UFmain.TextBox1.Width + UFmain.ToggleButton14.Width)
        End If
        
        If Get_Service = True Then
            .ColumnHeaders.Add , , "DOR", UFmain.TextBox4.Width
            .ColumnHeaders.Add , , "DCTB", UFmain.TextBox4.Width
            .ColumnHeaders.Add , , "UNIT", UFmain.ComboBox5.Width
            .ColumnHeaders.Add , , "SHOP", UFmain.TextBox4.Width
            .ColumnHeaders.Add , , "BILLET", UFmain.ComboBox5.Width
            .ColumnHeaders.Add , , "PMOS", UFmain.ToggleButton14.Width
            .ColumnHeaders.Add , , "BMOS", UFmain.ToggleButton14.Width
            .ColumnHeaders.Add , , "AMOS1", UFmain.ToggleButton14.Width
            .ColumnHeaders.Add , , "AMOS2", UFmain.ToggleButton14.Width
            .ColumnHeaders.Add , , "SECURITY", UFmain.ToggleButton14.Width + 5
        End If
        
        If Get_Education = True Then
            .ColumnHeaders.Add , , "OPSEC", UFmain.TextBox4.Width
            .ColumnHeaders.Add , , "IAA", UFmain.TextBox4.Width
            .ColumnHeaders.Add , , "PTP", UFmain.TextBox1.Width + UFmain.ToggleButton14.Width * 3
            .ColumnHeaders.Add , , "NLMB", UFmain.TextBox4.Width
            .ColumnHeaders.Add , , "SAPR", UFmain.TextBox1.Width + UFmain.ToggleButton14.Width + 20
            .ColumnHeaders.Add , , "PME_COMPLETE", UFmain.TextBox4.Width
            .ColumnHeaders.Add , , "PME", 2 * (UFmain.TextBox1.Width + UFmain.ToggleButton14.Width) + 40
        End If
        
        If Get_BCP = True Then
            .ColumnHeaders.Add , , "HEIGHT", UFmain.ToggleButton14.Width
            .ColumnHeaders.Add , , "WEIGHT", UFmain.ToggleButton14.Width
            .ColumnHeaders.Add , , "BODY FAT %", UFmain.TextBox4.Width
            .ColumnHeaders.Add , , "EFF DATE", UFmain.TextBox4.Width
            .ColumnHeaders.Add , , "WITHIN STANDARDS?", UFmain.TextBox4.Width + 20
        End If
        
        If Get_PFT = True Then
            .ColumnHeaders.Add , , "PFT", UFmain.TextBox4.Width
            .ColumnHeaders.Add , , "PULL-UPS", UFmain.ToggleButton14.Width
            .ColumnHeaders.Add , , "CRUNCHES", UFmain.TextBox4.Width
            .ColumnHeaders.Add , , "RUN", UFmain.ToggleButton14.Width
            .ColumnHeaders.Add , , "SCORE", UFmain.ToggleButton14.Width
            .ColumnHeaders.Add , , "CLASS", UFmain.ToggleButton14.Width
            .ColumnHeaders.Add , , "DESCRIPTION", UFmain.TextBox1.Width + UFmain.ToggleButton14.Width + 20
        End If
        
        If Get_CFT = True Then
            .ColumnHeaders.Add , , "CFT", UFmain.TextBox4.Width
            .ColumnHeaders.Add , , "MTC", UFmain.ToggleButton14.Width
            .ColumnHeaders.Add , , "AMMO LIFTS", UFmain.TextBox4.Width
            .ColumnHeaders.Add , , "MUF", UFmain.ToggleButton14.Width
            .ColumnHeaders.Add , , "SCORE", UFmain.ToggleButton14.Width
            .ColumnHeaders.Add , , "CLASS", UFmain.ToggleButton14.Width
            .ColumnHeaders.Add , , "DESCRIPTION", UFmain.TextBox1.Width + UFmain.ToggleButton14.Width + 20
        End If
        
        If Get_Swim = True Then
            .ColumnHeaders.Add , , "SWIM", UFmain.ComboBox5.Width
            .ColumnHeaders.Add , , "REQUAL", UFmain.TextBox4.Width
        End If
        
        If Get_MCMAP = True Then
            .ColumnHeaders.Add , , "MCMAP", UFmain.TextBox4.Width
            .ColumnHeaders.Add , , "QUAL", UFmain.ToggleButton14.Width
            .ColumnHeaders.Add , , "BELT", UFmain.TextBox4.Width
            .ColumnHeaders.Add , , "INSTR COMP DATE", UFmain.ComboBox7.Width
            .ColumnHeaders.Add , , "RECERT", UFmain.TextBox4.Width
        End If
        
        If Get_NBC = True Then: .ColumnHeaders.Add , , "NBC DATE", UFmain.TextBox4.Width
        
        If Get_Rifle = True Then
            .ColumnHeaders.Add , , "RIFLE", UFmain.TextBox4.Width
            .ColumnHeaders.Add , , "TABLE-I", UFmain.ToggleButton14.Width
            .ColumnHeaders.Add , , "TABLE-II", UFmain.ToggleButton14.Width
            .ColumnHeaders.Add , , "SCORE", UFmain.ToggleButton14.Width
            .ColumnHeaders.Add , , "CODE", UFmain.TextBox4.Width
            .ColumnHeaders.Add , , "EXPERT QUALS", UFmain.TextBox4.Width
            .ColumnHeaders.Add , , "EXEMPT", UFmain.TextBox4.Width
            .ColumnHeaders.Add , , "EXEMPT DATE", UFmain.TextBox4.Width
        End If
        
        If Get_Pistol = True Then
            .ColumnHeaders.Add , , "PISTOL", UFmain.TextBox4.Width
            .ColumnHeaders.Add , , "SCORE", UFmain.ToggleButton14.Width
            .ColumnHeaders.Add , , "CODE", UFmain.TextBox4.Width
            .ColumnHeaders.Add , , "EXPERT QUALS", UFmain.TextBox4.Width
            .ColumnHeaders.Add , , "EXEMPT", UFmain.TextBox4.Width
            .ColumnHeaders.Add , , "EXEMPT DATE", UFmain.TextBox4.Width
        End If
        
        ' Set some properties! | _
        ````````````````````````
        .HideColumnHeaders = False
        .View = lvwReport
        .Gridlines = True
        
        ' Output Database Values (Filtered/Unfiltered) to the ListView Control! | _
        `````````````````````````````````````````````````````````````````````````
        For i = 1 To mcolPersonnel.Count
            Dim Counter As Integer
            Counter = 0
            Current_Grade = CALC_Grade(mcolPersonnel(i).Rank)
            
            ' If there are options that have been set for the Records, in terms of Ranks, then evaluate each rank _
              before posting into the ListView control (e.g. A filtered list of Non-Commission Officers)
            If Get_Ranks = True Then
                If List_Junior_Marines = True Then
                    If Current_Grade >= 4 Then
                        Next_Grade = True
                    Else: GoTo List_Personnel
                    End If
                End If
                If List_NCO = True Then
                    If Current_Grade <= 3 Or Current_Grade >= 6 Then
                        Next_Grade = True
                    Else: GoTo List_Personnel
                    End If
                End If
                If List_SNCO = True Then
                    If Current_Grade <= 5 Or Current_Grade >= 10 Then
                        Next_Grade = True
                    Else: GoTo List_Personnel
                    End If
                End If
                If List_Officers = True Then
                    If Current_Grade <= 10 Then
                        Next_Grade = True
                    Else: GoTo List_Personnel
                    End If
                End If
                If Next_Grade = True Then GoTo Next_Personnel
            End If
List_Personnel:
            Set li = .ListItems.Add(, , mcolPersonnel(i).Rank)
                
            ' Personnel Table _
            _______________________________________
            li.SubItems(1) = mcolPersonnel(i).NAME
            li.SubItems(2) = mcolPersonnel(i).EDIPI
            li.SubItems(3) = mcolPersonnel(i).Sex
            li.SubItems(4) = mcolPersonnel(i).DOB
            li.SubItems(5) = mcolPersonnel(i).EMail
            li.SubItems(6) = mcolPersonnel(i).Ed_Level
            li.SubItems(7) = mcolPersonnel(i).CERT
            li.SubItems(8) = mcolPersonnel(i).Major
            Counter = 8
            
            ' Duty Status Table _
            _____________________________________________
            If Get_Duty_Status = True Then
                li.SubItems(9) = mcolDuty_status(i).Status
                li.SubItems(10) = mcolDuty_status(i).Limit
                li.SubItems(11) = mcolDuty_status(i).Limit_Date
                li.SubItems(12) = mcolDuty_status(i).STR_CAT
                li.SubItems(13) = mcolDuty_status(i).STR_CAT_Date
                li.SubItems(14) = mcolDuty_status(i).ComCasualty
                li.SubItems(15) = mcolDuty_status(i).ComCas_Date
                Counter = Counter + 7
            End If
            
            ' Contract Table _
            _____________________________________________
            If Get_Contract = True Then
                If Options_Num = 0 Then
                    li.SubItems(16) = mcolContract(i).Orig_Entry
                    li.SubItems(17) = mcolContract(i).Enlist
                    li.SubItems(18) = mcolContract(i).EAS
                    li.SubItems(19) = mcolContract(i).EOS
                    li.SubItems(20) = mcolContract(i).PEF
                    li.SubItems(21) = mcolContract(i).Bonus_PEF
                    li.SubItems(22) = mcolContract(i).College_PEF
                Else
                    Counter = Counter + 1: li.SubItems(Counter) = mcolContract(i).Orig_Entry
                    Counter = Counter + 1: li.SubItems(Counter) = mcolContract(i).Enlist
                    Counter = Counter + 1: li.SubItems(Counter) = mcolContract(i).EAS
                    Counter = Counter + 1: li.SubItems(Counter) = mcolContract(i).EOS
                    Counter = Counter + 1: li.SubItems(Counter) = mcolContract(i).PEF
                    Counter = Counter + 1: li.SubItems(Counter) = mcolContract(i).Bonus_PEF
                    Counter = Counter + 1: li.SubItems(Counter) = mcolContract(i).College_PEF
                End If
            End If
            
            ' Service Table _
            _____________________________________________
            If Get_Service = True Then
                If Options_Num = 0 Then
                    li.SubItems(23) = mcolService(i).DOR
                    li.SubItems(24) = mcolService(i).DCTB
                    li.SubItems(25) = mcolService(i).Unit
                    li.SubItems(26) = mcolService(i).Shop
                    li.SubItems(27) = mcolService(i).Billet
                    li.SubItems(28) = mcolService(i).PMOS
                    li.SubItems(29) = mcolService(i).BMOS
                    li.SubItems(30) = mcolService(i).AMOS1
                    li.SubItems(31) = mcolService(i).AMOS2
                    li.SubItems(32) = mcolService(i).Security
                Else
                    Counter = Counter + 1: li.SubItems(Counter) = mcolService(i).DOR
                    Counter = Counter + 1: li.SubItems(Counter) = mcolService(i).DCTB
                    Counter = Counter + 1: li.SubItems(Counter) = mcolService(i).Unit
                    Counter = Counter + 1: li.SubItems(Counter) = mcolService(i).Shop
                    Counter = Counter + 1: li.SubItems(Counter) = mcolService(i).Billet
                    Counter = Counter + 1: li.SubItems(Counter) = mcolService(i).PMOS
                    Counter = Counter + 1: li.SubItems(Counter) = mcolService(i).BMOS
                    Counter = Counter + 1: li.SubItems(Counter) = mcolService(i).AMOS1
                    Counter = Counter + 1: li.SubItems(Counter) = mcolService(i).AMOS2
                    Counter = Counter + 1: li.SubItems(Counter) = mcolService(i).Security
                End If
            End If
                                               
            ' Education Table _
            __________________________________________________________________________________
            If Get_Education = True Then
                If Options_Num = 0 Then
                    li.SubItems(33) = mcolEducation(i).OPSEC
                    li.SubItems(34) = mcolEducation(i).IAA
                    li.SubItems(35) = mcolEducation(i).PTP
                    li.SubItems(36) = mcolEducation(i).NLMB
                    li.SubItems(37) = mcolEducation(i).SAPR
                    li.SubItems(38) = mcolEducation(i).PME_Completed
                    li.SubItems(39) = mcolEducation(i).PME
                    

                Else
                    Counter = Counter + 1: li.SubItems(Counter) = mcolEducation(i).OPSEC
                    Counter = Counter + 1: li.SubItems(Counter) = mcolEducation(i).IAA
                    Counter = Counter + 1: li.SubItems(Counter) = mcolEducation(i).PTP
                    Counter = Counter + 1: li.SubItems(Counter) = mcolEducation(i).NLMB
                    Counter = Counter + 1: li.SubItems(Counter) = mcolEducation(i).SAPR
                    Counter = Counter + 1: li.SubItems(Counter) = mcolEducation(i).PME_Completed
                    Counter = Counter + 1: li.SubItems(Counter) = mcolEducation(i).PME
                End If
            End If
            
            ' BCP Table _
            __________________________________________________________________________
            If Get_BCP = True Then
                If Options_Num = 0 Then
                    li.SubItems(40) = mcolBCP(i).Height
                    li.SubItems(41) = mcolBCP(i).Weight
                    li.SubItems(42) = mcolBCP(i).Body_Fat
                    li.SubItems(43) = mcolBCP(i).Effective
                    li.SubItems(44) = mcolBCP(i).Status
                Else
                    Counter = Counter + 1: li.SubItems(Counter) = mcolBCP(i).Height
                    Counter = Counter + 1: li.SubItems(Counter) = mcolBCP(i).Weight
                    Counter = Counter + 1: li.SubItems(Counter) = mcolBCP(i).Body_Fat
                    Counter = Counter + 1: li.SubItems(Counter) = mcolBCP(i).Effective
                    Counter = Counter + 1: li.SubItems(Counter) = mcolBCP(i).Status
                End If
            End If
                        
            ' PFT Table _
            _______________________________________________________________________
            If Get_PFT = True Then
                If Options_Num = 0 Then
                    li.SubItems(45) = mcolPFT(i).PFT
                    li.SubItems(46) = mcolPFT(i).Pull_Ups
                    li.SubItems(47) = mcolPFT(i).Crunches
                    li.SubItems(48) = mcolPFT(i).Run
                    li.SubItems(49) = mcolPFT(i).Score
                    li.SubItems(50) = mcolPFT(i).Class
                    li.SubItems(51) = mcolPFT(i).Description
                Else
                    Counter = Counter + 1: li.SubItems(Counter) = mcolPFT(i).PFT
                    Counter = Counter + 1: li.SubItems(Counter) = mcolPFT(i).Pull_Ups
                    Counter = Counter + 1: li.SubItems(Counter) = mcolPFT(i).Crunches
                    Counter = Counter + 1: li.SubItems(Counter) = mcolPFT(i).Run
                    Counter = Counter + 1: li.SubItems(Counter) = mcolPFT(i).Score
                    Counter = Counter + 1: li.SubItems(Counter) = mcolPFT(i).Class
                    Counter = Counter + 1: li.SubItems(Counter) = mcolPFT(i).Description
                End If
            End If
            
            ' CFT Table _
            _______________________________________________________________________
            If Get_CFT = True Then
                If Options_Num = 0 Then
                    li.SubItems(52) = mcolCFT(i).CFT
                    li.SubItems(53) = mcolCFT(i).MTC
                    li.SubItems(54) = mcolCFT(i).Ammo_Lifts
                    li.SubItems(55) = mcolCFT(i).MUF
                    li.SubItems(56) = mcolCFT(i).Score
                    li.SubItems(57) = mcolCFT(i).Class
                    li.SubItems(58) = mcolCFT(i).Description
                Else
                    Counter = Counter + 1: li.SubItems(Counter) = mcolCFT(i).CFT
                    Counter = Counter + 1: li.SubItems(Counter) = mcolCFT(i).MTC
                    Counter = Counter + 1: li.SubItems(Counter) = mcolCFT(i).Ammo_Lifts
                    Counter = Counter + 1: li.SubItems(Counter) = mcolCFT(i).MUF
                    Counter = Counter + 1: li.SubItems(Counter) = mcolCFT(i).Score
                    Counter = Counter + 1: li.SubItems(Counter) = mcolCFT(i).Class
                    Counter = Counter + 1: li.SubItems(Counter) = mcolCFT(i).Description
                End If
            End If
            
            ' Swim Table _
            ________________________________________________________________________
            If Get_Swim = True Then
                If Options_Num = 0 Then
                    li.SubItems(59) = mcolSwim(i).SWIM
                    li.SubItems(60) = mcolSwim(i).Requal
                Else
                    Counter = Counter + 1: li.SubItems(Counter) = mcolSwim(i).SWIM
                    Counter = Counter + 1: li.SubItems(Counter) = mcolSwim(i).Requal
                End If
            End If
            
            ' MCMAP Table _
            _____________________________________________________________________________
            If Get_MCMAP = True Then
                If Options_Num = 0 Then
                    li.SubItems(61) = mcolMCMAP(i).MCMAP
                    li.SubItems(62) = mcolMCMAP(i).Qual
                    li.SubItems(63) = mcolMCMAP(i).Belt
                    li.SubItems(64) = mcolMCMAP(i).Instructor
                    li.SubItems(65) = mcolMCMAP(i).Recert
                Else
                    Counter = Counter + 1: li.SubItems(Counter) = mcolMCMAP(i).MCMAP
                    Counter = Counter + 1: li.SubItems(Counter) = mcolMCMAP(i).Qual
                    Counter = Counter + 1: li.SubItems(Counter) = mcolMCMAP(i).Belt
                    Counter = Counter + 1: li.SubItems(Counter) = mcolMCMAP(i).Instructor
                    Counter = Counter + 1: li.SubItems(Counter) = mcolMCMAP(i).Recert
                End If
            End If
                        
            ' Gas Chamber Table _
            ______________________________________________________________________
            If Get_NBC = True Then
                If Options_Num = 0 Then
                    li.SubItems(66) = mcolNBC(i).NBC
                Else: Counter = Counter + 1: li.SubItems(Counter) = mcolNBC(i).NBC
                End If
            End If
            
            ' Rifle Table _
            _______________________________________________________________________
            If Get_Rifle = True Then
                If Options_Num = 0 Then
                    li.SubItems(67) = mcolRifle(i).Rifle
                    li.SubItems(68) = mcolRifle(i).TableI
                    li.SubItems(69) = mcolRifle(i).TableII
                    li.SubItems(70) = mcolRifle(i).Score
                    li.SubItems(71) = mcolRifle(i).Code
                    li.SubItems(72) = mcolRifle(i).Expert_Quals
                    li.SubItems(73) = mcolRifle(i).Exempt
                    li.SubItems(74) = mcolRifle(i).Exempt_Date
                Else
                    Counter = Counter + 1: li.SubItems(Counter) = mcolRifle(i).Rifle
                    Counter = Counter + 1: li.SubItems(Counter) = mcolRifle(i).TableI
                    Counter = Counter + 1: li.SubItems(Counter) = mcolRifle(i).TableII
                    Counter = Counter + 1: li.SubItems(Counter) = mcolRifle(i).Score
                    Counter = Counter + 1: li.SubItems(Counter) = mcolRifle(i).Code
                    Counter = Counter + 1: li.SubItems(Counter) = mcolRifle(i).Expert_Quals
                    Counter = Counter + 1: li.SubItems(Counter) = mcolRifle(i).Exempt
                    Counter = Counter + 1: li.SubItems(Counter) = mcolRifle(i).Exempt_Date
                End If
            End If
            
            ' Pistol Table _
            _______________________________________________________________________
            If Get_Pistol = True Then
                If Options_Num = 0 Then
                    li.SubItems(75) = mcolPistol(i).Pistol
                    li.SubItems(76) = mcolPistol(i).Score
                    li.SubItems(77) = mcolPistol(i).Code
                    li.SubItems(78) = mcolPistol(i).Expert_Quals
                    li.SubItems(79) = mcolPistol(i).Exempt
                    li.SubItems(80) = mcolPistol(i).Exempt_Date
                Else
                    Counter = Counter + 1: li.SubItems(Counter) = mcolPistol(i).Pistol
                    Counter = Counter + 1: li.SubItems(Counter) = mcolPistol(i).Score
                    Counter = Counter + 1: li.SubItems(Counter) = mcolPistol(i).Code
                    Counter = Counter + 1: li.SubItems(Counter) = mcolPistol(i).Expert_Quals
                    Counter = Counter + 1: li.SubItems(Counter) = mcolPistol(i).Exempt
                    Counter = Counter + 1: li.SubItems(Counter) = mcolPistol(i).Exempt_Date
                End If
            End If
            
            Counter = 0
Next_Personnel:
            Next_Grade = False
        Next i
        
        '>>For i = 1 To 32
            '>>If i = 1 Then
                '>>With UFmain.ListView1.ListItems(User_Row - 2)
                    '>>.Bold = True
                    '>>.ForeColor = vbYellow
                '>>End With
            '>>End If
            '>>With UFmain.ListView1.ListItems(User_Row - 2).ListSubItems(i)
                '>>.Bold = True
                '>>.ForeColor = vbYellow
            '>>End With
        '>>Next i
                            
        '>>ListView1_ItemClick .ListItems(.SelectedItem.Index) 'fill edit controls
    End With
    Call Label11_Click
End Sub

Private Sub CommandButton2_Click()
    Unload Me
End Sub

Private Sub CommandButton3_Click()
    Dim Combo As Integer, Toggle As Integer, ChkBox As Integer, TxtBox As Integer
                
    Query = Not Query
    
    ' Reset These Boolean Values (It's toggled back on inadvertantly via the first CommandButton and Configuration Panel)
    Personnel_Only = False
    Get_Duty_Status = False
    Get_Contract = False
    Get_Service = False
    Get_Education = False
    Get_BCP = False
    Get_PFT = False
    Get_CFT = False
    Get_Swim = False
    Get_MCMAP = False
    Get_NBC = False
    Get_Rifle = False
    Get_Pistol = False
    Ranks_After_Query = False
    Back2Query = False
    List_Junior_Marines = False
    List_NCO = False
    List_SNCO = False
    List_Officers = False
    Remove_Option = False
        
    If Query = True Then
        With Me.CommandButton3
            .Caption = "Reset Query"
            .ForeColor = vbYellow
            .BackColor = vbRed
        End With
        
        ' Store extra options [located inside the "Options" ComboBox] inside of an array, that will trigger Boolean variables later on ...
        If Me.ComboBox8.ListCount > 0 Then
        
            Dim i As Long
            Dim Options_Arr() As Variant
            
            Options_Num = Me.ComboBox8.ListCount
            ReDim Options_Arr(Options_Num)
            
            For i = 0 To Options_Num - 1: Options_Arr(i) = Me.ComboBox8.List(i, 0): Next i
            
            For i = 0 To Options_Num - 1
                Select Case Options_Arr(i)
                    '>>Case "ListView: Grid":
                    '>>Case "ListView: BTR":
                    Case "Personnel Only": Personnel_Only = True
                    Case "Get Duty Status Data": Get_Duty_Status = True
                    Case "Get Contract Data": Get_Contract = True
                    Case "Get Service Data": Get_Service = True
                    Case "Get Education Data": Get_Education = True
                    Case "Get BCP Data": Get_BCP = True
                    Case "Get PFT Data": Get_PFT = True
                    Case "Get CFT Data": Get_CFT = True
                    Case "Get Swim Data": Get_Swim = True
                    Case "Get MCMAP Data": Get_MCMAP = True
                    Case "Get NBC Data": Get_NBC = True
                    Case "Get Rifle Data": Get_Rifle = True
                    Case "Get Pistol Data": Get_Pistol = True
                    Case "List Officers": List_Officers = True
                    Case "List SNCO's": List_SNCO = True
                    Case "List NCO's": List_NCO = True
                    Case "List Junior Marines": List_Junior_Marines = True
                End Select
            Next i
        End If
        
        ' If Personnel_Only is selected, then Remove all other Table Queries (Boolean Values will be reset later on)
        If Personnel_Only = True Then
            For i = Me.ComboBox8.ListCount - 1 To 0 Step -1
                If STR_SPLIT(Me.ComboBox8.List(i, 0), " ", 1) = "Get" Then: Me.ComboBox8.RemoveItem (i)
            Next i
            Get_Duty_Status = False
            Get_Contract = False
            Get_Service = False
            Get_Education = False
            Get_BCP = False
            Get_PFT = False
            Get_CFT = False
            Get_Swim = False
            Get_MCMAP = False
            Get_NBC = False
            Get_Rifle = False
            Get_Pistol = False
        End If
        
        ' THESE... are the sequence of commands that will control how the queries behave
        Call CommandButton1_Click
        Call Send_Query
        If Query_Counter = 0 Then
            If Me.ComboBox8.ListCount = 0 Then
                Query = Not Query
                GoTo Reset_Form
            End If
        End If
        Call Label11_Click
        If Back2Query = True Then
            Back2Query = False
            Call CommandButton1_Click
            Call Send_Query
            Call Label11_Click
        End If
    Else
    
Reset_Form:
        With Me.CommandButton3
            .Caption = "Send Query"
            .ForeColor = vbBlue
            .BackColor = RGB(100, 100, 100)
        End With
        
        ' Reset the Percentages Label for the ListView Report
        Me.Label11.Caption = ""
        
        ' These global variables have to be reset manually; the other global variables will be reset by the Userform
        NBC_1 = False
        NBC_2 = False
        
        ' Clear the TextBoxes on Page 1 of the Multipage Control
        For TxtBox = 1 To 4
            UFmain.Controls("TextBox" & TxtBox).Text = ""
        Next TxtBox
        
        ' Set each Combobox in the Multipage control to "NONE"
        For Combo = 1 To 7
            UFmain.Controls("ComboBox" & Combo).Text = "NONE"
        Next Combo
        
        ' Unpress any Toggle Buttons
        For Toggle = 1 To 16
            UFmain.Controls("ToggleButton" & Toggle).Value = False
        Next Toggle
        UFmain.ToggleButton21.Value = False
        
        ' Uncheck any CheckBoxes
        For ChkBox = 1 To 4
            If UFmain.Controls("CheckBox" & ChkBox).Value = True Then: UFmain.Controls("CheckBox" & ChkBox).Value = False
        Next ChkBox
                    
        ' Clear the ListView control
        Me.ListView1.ListItems.Clear
        
        ' Clear the Options Drop-down Menu
        Me.ComboBox8.Clear
        
        ' Reset the Boolean Values that affect the Options in the previous control
        Personnel_Only = False
        Get_Tables = False
        Get_Duty_Status = False
        Get_Contract = False
        Get_Service = False
        Get_Education = False
        Get_BCP = False
        Get_PFT = False
        Get_CFT = False
        Get_Swim = False
        Get_MCMAP = False
        Get_NBC = False
        Get_Rifle = False
        Get_Pistol = False
        Get_Ranks = False
        List_Junior_Marines = False
        List_NCO = False
        List_SNCO = False
        List_Officers = False
                
        ' Set the focus on the Multipage Control to the first page
        Me.MultiPage1.Value = 0
        
        ' Reset the ListBox Control
        Me.ListBox1.Clear
        Me.ListBox1.AddItem "ListView: Grid", 0
        Me.ListBox1.AddItem "ListView: BTR", 1
    End If
End Sub

Private Sub CommandButton4_Click()
    ThisWorkbook.Sheets(1).Visible = xlSheetVisible
    Call Get_Files
    Alpha_Roster = "False"
    BIR = "False"
    BTR = "False"
    ThisWorkbook.Sheets(1).Visible = xlVeryHidden
    Database_Empty = False
End Sub

Private Sub CommandButton5_Click()
    Dim Msg As String, Response As String
    
    If Database_Empty = False Then
        ' Warn the User that if they Clear the Database that they will have to Update it later on to use this program
        Msg = "This option exists in order to remove any PII contained in this program, and to allow it to be used by others.  "
        Msg = Msg & "If another Unit plans on using this program, then another S-3 Clerk [with MOL permissions to the Report Bridge] must speak to me, "
        Msg = Msg & "so that I can modify this program for their use (i.e. certain procedures must be edited).  These procedures are the LR() Function, Userform_Initialize Sub-Routine, and the "
        Msg = Msg & "Declaration Space in the Initialize Module.  Once these routines have been modified, the program can be turned over to the Clerk."
        Msg = Msg & vbNewLine & vbNewLine & "Are you sure you want to DELETE everything in the Database?"
        Response = MsgBox(Msg, vbYesNo + vbExclamation, "Are you out of your mind, or in the clear...?")
        If Response = "6" Then
            Last_Row = LR()
            ThisWorkbook.Sheets(1).Range("A3:AI" & Last_Row).ClearContents
            Msg = "The Database is now empty!  To fill it, click " & Chr(34) & "Update Database." & Chr(34)
            Msg = Msg & vbNewLine & vbNewLine & "NOTE: There are still records saved in memory-- just in case!?"
            Response = MsgBox(Msg, vbOKOnly + vbInformation, "What's done is done... 8-(")
        Else: Response = MsgBox("May the force be with you!", vbOKOnly + vbInformation, "Wise Choice...")
        End If
    Else: Response = MsgBox("The Database is already empty!?  Why even click this button?", vbOKOnly + vbInformation, "What were you thinking...")
    End If
End Sub

Private Sub Label1_Click()
    Dim Msg As String

    Msg = vbTab & vbTab & "   VMM-162" & vbNewLine
    Msg = Msg & "Training & Education Shell"
    With Me.Label1
        .Caption = Msg
        .BackColor = vbBlack
        .ForeColor = vbRed
        With .Font
            .Bold = True
            .Size = 24
        End With
    End With
End Sub

Private Sub Label9_Click()
    With Me.Label9
        .Caption = " Query :  [Enter Criteria]"
        .Font.Size = 12
        .Font.Bold = True
        .BackColor = vbBlack
        .ForeColor = vbRed
    End With
End Sub

Private Sub Label10_Click()
    With Me.Label10
        .Caption = "Options:"
        .Font.Bold = True
        .BackColor = vbBlue
        .ForeColor = vbYellow
    End With
End Sub

Private Sub Label11_Click()
    Dim Total_Records As Integer, Listed_Records As Integer, Unlisted_Records As Integer, Percentages As String

    On Error Resume Next
    Err = 0
    Total_Records = UFmain.Personnel.Count
    Listed_Records = UFmain.ListView1.ListItems.Count
    If Err = 91 Then: Listed_Records = 0
    Unlisted_Records = Total_Records - Listed_Records
    If Err = 91 Then: Unlisted_Records = Total_Records
    
    Percentages = " Total Records = " & CStr(Total_Records) & " (100%)" & vbTab & vbTab & vbTab & vbTab & _
                  "Records Listed = " & CStr(Listed_Records) & " (" & CStr(CInt(Round(Listed_Records / Total_Records * 100, 1))) & "%)" & vbTab & vbTab & vbTab & vbTab & _
                  "Records NOT Listed = " & CStr(Unlisted_Records) & " (" & CStr(CInt(Round((Listed_Records / Total_Records * 100 - 100) * -1, 1))) & "%)"
    
    If Err = 91 Then
        Err = 0
        With Me.Label11
            If Listed_Records <> 0 And Total_Records <> 0 Then
                .Caption = Percentages
            Else: .Caption = ""
            End If
            .BackColor = vbRed
            .ForeColor = vbYellow
            .BorderColor = vbRed
            .BorderStyle = fmBorderStyleSingle
        End With
    Else
        With Me.Label11
            If Listed_Records <> 0 And Total_Records <> 0 Then
                .Caption = Percentages
            Else: .Caption = ""
            End If
            .BackColor = vbRed
            .ForeColor = vbYellow
            .BorderColor = vbRed
            .BorderStyle = fmBorderStyleSingle
        End With
    End If
End Sub

Private Sub Label17_Click()
    With Me.Label17
        Dim Current_User As Variant
        
        Current_User = Application.UserName
        Current_User = User_Name(Current_User)
        Rank = CALC_Grade(CHK_RANK(Full_Name))
        Current_User = Proper_Rank(1 * Rank) & " " & _
                       UCase(Mid(STR_SPLIT(Current_User, ",", 1), 1, 1)) & LCase(Mid(STR_SPLIT(Current_User, ",", 1), 2, Len(Current_User))) & ", " & _
                       UCase(Mid(STR_SPLIT(Current_User, ",", 2), 1, 1)) & LCase(Mid(STR_SPLIT(Current_User, ",", 2), 2, Len(STR_SPLIT(Current_User, ",", 2)) - 4)) & _
                       UCase(Mid(STR_SPLIT(Current_User, ",", 2), Len(STR_SPLIT(Current_User, ",", 2)) - 2))
        .Caption = "The Current User is : " & Current_User & _
                   CALC_SPC(Current_User, Label17.Width) & _
                   "The Author is : < " & STR_SPLIT(Author_1, " ", 2) & " " & STR_SPLIT(Author_1, " ", 1) & ", " & STR_SPLIT(Author_1, " ", 3) & " " & STR_SPLIT(Author_1, " ", 4) & ". >"
        .Font.Bold = True
        .ForeColor = vbRed
        .BackColor = vbBlack
        .BorderColor = vbBlack
        .BorderStyle = fmBorderStyleSingle
    End With
End Sub

Private Sub ListBox1_Click()
' Options | _
'''''''''''
'
' + Display Mode = Grid Style / BTR Viewer
' + Display the Choices presented from ToggleButtons14-16
' + General Options will be visible if none of the Buttons have been toggled in the Configuration Panel
'
' NOTE: if the Display mode is BTR Viewer, then make all pages in the Multipage Control hidden-- except the first page

    Me.ListBox1.Clear
    Me.ListBox1.AddItem "ListView: Grid", 0
    Me.ListBox1.AddItem "ListView: BTR", 1
End Sub

Private Sub ListBox1_Change()
    Dim i As Long, Add_Option As Boolean
    Add_Option = False
    
    If Scope_Toggle(0) = True Then
        Get_Tables = True
        If Config_Option(0) = False And _
           Config_Option(1) = False And _
           Config_Option(2) = False And _
           Config_Option(3) = False And _
           Config_Option(4) = False And _
           Config_Option(5) = False Then
            If Me.ListBox1.ListIndex = 0 Then
                Config_Option(0) = Not Config_Option(0)
                Me.ListBox1.Clear
                Me.ListBox1.AddItem StrArray(0, 0)
                Me.ListBox1.AddItem StrArray(0, 1)
                '>>SendKeys "{TAB}", True
                Exit Sub
                ElseIf Me.ListBox1.ListIndex = 1 Then
                    Config_Option(1) = Not Config_Option(1)
                    ' I tell the program to wait; otherwise, the initial click will select an option instantly, since _
                      the posting of the array occurs faster than the processing of the click.
                    Rem-- this only occurs when selecting values in a 2-Column Array on the second row
                    Application.Wait (Now + TimeValue("0:00:02")) '<<---- Wait a sec
                    Me.ListBox1.Clear
                    Me.ListBox1.AddItem StrArray(1, 0) '<<--------------- Now, post the array
                    Me.ListBox1.AddItem StrArray(1, 1) '<<--------------- Without "Wait", this element will be selected
                    '>>SendKeys "{TAB}", True
                    Exit Sub
                    ElseIf Me.ListBox1.ListIndex = 2 Then
                        Config_Option(2) = Not Config_Option(2)
                        ' I tell the program to wait; otherwise, the initial click will select an option instantly, since _
                          the posting of the array occurs faster than the processing of the click.
                        Rem-- this only affects the elements of the array whose values can be scrolled to the second row of the ListBox Control (Dimension 1: 1-4 = "yes"; 0, 5-6 = "no")
                        Application.Wait (Now + TimeValue("0:00:02")) '<<---- Wait a sec
                        Me.ListBox1.Clear
                        Me.ListBox1.AddItem StrArray(2, 0) '<<--------------- Now, post the array
                        Me.ListBox1.AddItem StrArray(2, 1) '<<--------------- Without "Wait", this element will be selected
                        '>>SendKeys "{TAB}", True
                        Exit Sub
                        ElseIf Me.ListBox1.ListIndex = 3 Then
                            Config_Option(3) = Not Config_Option(3)
                            ' I tell the program to wait; otherwise, the initial click will select an option instantly, since _
                              the posting of the array occurs faster than the processing of the click.
                            Rem-- this only affects the elements of the array whose values can be scrolled to the second row of the ListBox Control (Dimension 1: 1-4 = "yes"; 0, 5-6 = "no")
                            Application.Wait (Now + TimeValue("0:00:02")) '<<---- Wait a sec
                            Me.ListBox1.Clear
                            Me.ListBox1.AddItem StrArray(3, 0) '<<--------------- Now, post the array
                            Me.ListBox1.AddItem StrArray(3, 1) '<<--------------- Without "Wait", this element will be selected
                            '>>SendKeys "{TAB}", True
                            Exit Sub
                            ElseIf Me.ListBox1.ListIndex = 4 Then
                                Config_Option(4) = Not Config_Option(4)
                                ' I tell the program to wait; otherwise, the initial click will select an option instantly, since _
                                  the posting of the array occurs faster than the processing of the click.
                                Rem-- this only affects the elements of the array whose values can be scrolled to the second row of the ListBox Control (Dimension 1: 1-4 = "yes"; 0, 5-6 = "no")
                                Application.Wait (Now + TimeValue("0:00:01")) '<<---- Wait a sec
                                Me.ListBox1.Clear
                                Me.ListBox1.AddItem StrArray(4, 0) '<<--------------- Now, post the array
                                Me.ListBox1.AddItem StrArray(4, 1) '<<--------------- Without "Wait", this element will be selected
                                '>>SendKeys "{TAB}", True
                                Exit Sub
                                ElseIf Me.ListBox1.ListIndex = 5 Then
                                    Config_Option(5) = Not Config_Option(5)
                                    Me.ListBox1.Clear
                                    Me.ListBox1.AddItem StrArray(5, 0)
                                    Me.ListBox1.AddItem StrArray(5, 1)
                                    '>>SendKeys "{TAB}", True
                                    Exit Sub
            Else: Me.ComboBox8.AddItem StrArray(6, 0): Exit Sub
            End If
        End If
        If Config_Option(0) = True Or _
           Config_Option(1) = True Or _
           Config_Option(2) = True Or _
           Config_Option(3) = True Or _
           Config_Option(4) = True Or _
           Config_Option(5) = True Then
               For i = 0 To Me.ComboBox8.ListCount - 1
                   If Me.ListBox1.List(0, 0) = Me.ComboBox8.List(i, 0) Then
                       Add_Option = False
                   Else
                       Add_Option = True
                   End If
               Next i
               If Me.ComboBox8.ListCount = 0 Then: Add_Option = True
               If Me.ListBox1.ListIndex = 0 Then
                   If Add_Option = True Then: Me.ComboBox8.AddItem Me.ListBox1.List(0, 0)
               End If
               For i = 0 To Me.ComboBox8.ListCount - 1
                   If Me.ListBox1.List(1, 0) = Me.ComboBox8.List(i, 0) Then
                       Add_Option = False
                   Else
                       Add_Option = True
                   End If
               Next i
               If Me.ComboBox8.ListCount = 0 Then: Add_Option = True
               If Me.ListBox1.ListIndex = 1 Then
                   If Add_Option = True Then: Me.ComboBox8.AddItem Me.ListBox1.List(1, 0)
               End If
            For i = 0 To 5: Config_Option(i) = Not Config_Option(i): Next i
            Add_Option = False
            Scope_Toggle(0) = Not Scope_Toggle(0)
        End If
    End If
    If Scope_Toggle(1) = True Then
        Get_Ranks = True
        For i = 0 To Me.ComboBox8.ListCount - 1
            If Me.ListBox1.List(0, 0) = Me.ComboBox8.List(i, 0) Then
                Add_Option = False
            Else
                Add_Option = True
            End If
        Next i
        If Me.ComboBox8.ListCount = 0 Then: Add_Option = True
        If Me.ListBox1.ListIndex = 0 Then
            If Add_Option = True Then: Me.ComboBox8.AddItem Me.ListBox1.List(0, 0)
        End If
        For i = 0 To Me.ComboBox8.ListCount - 1
            If Me.ListBox1.List(1, 0) = Me.ComboBox8.List(i, 0) Then
                Add_Option = False
            Else
                Add_Option = True
            End If
        Next i
        If Me.ComboBox8.ListCount = 0 Then: Add_Option = True
        If Me.ListBox1.ListIndex = 1 Then
            If Add_Option = True Then: Me.ComboBox8.AddItem Me.ListBox1.List(1, 0)
        End If
        For i = 0 To Me.ComboBox8.ListCount - 1
            If Me.ListBox1.List(2, 0) = Me.ComboBox8.List(i, 0) Then
                Add_Option = False
            Else
                Add_Option = True
            End If
        Next i
        If Me.ComboBox8.ListCount = 0 Then: Add_Option = True
        If Me.ListBox1.ListIndex = 2 Then
            If Add_Option = True Then: Me.ComboBox8.AddItem Me.ListBox1.List(2, 0)
        End If
        For i = 0 To Me.ComboBox8.ListCount - 1
            If Me.ListBox1.List(3, 0) = Me.ComboBox8.List(i, 0) Then
                Add_Option = False
            Else
                Add_Option = True
            End If
        Next i
        If Me.ComboBox8.ListCount = 0 Then: Add_Option = True
        If Me.ListBox1.ListIndex = 3 Then
            If Add_Option = True Then: Me.ComboBox8.AddItem Me.ListBox1.List(3, 0)
        End If
        Add_Option = False
        Scope_Toggle(1) = Not Scope_Toggle(1)
    End If
End Sub

Private Sub SpinButton1_SpinDown()
    If Me.MultiPage1.Value <> 0 Then: Me.MultiPage1.Value = Me.MultiPage1.Value - 1
End Sub

Private Sub SpinButton1_SpinUp()
    If Me.MultiPage1.Value <> 12 Then: Me.MultiPage1.Value = Me.MultiPage1.Value + 1
End Sub

Private Sub TextBox1_Change()
    If Me.TextBox1.Text <> "" Then
        With Me.TextBox4
            .Text = ""
            .Enabled = False
            .BackColor = vbGreen
        End With
        Me.Label5.Font.Strikethrough = True
    Else
        With Me
            If .TextBox1.Text = "" And _
               .TextBox2.Text = "" And _
               .TextBox3.Text = "" Then
                   .TextBox4.Enabled = True
                   .TextBox4.BackColor = vbBlack
            End If
        End With
        Me.Label5.Font.Strikethrough = False
    End If
End Sub

Private Sub TextBox2_Change()
    If Me.TextBox2.Text <> "" Then
        With Me.TextBox4
            .Text = ""
            .Enabled = False
            .BackColor = vbGreen
        End With
        Me.Label5.Font.Strikethrough = True
    Else
        With Me
            If .TextBox1.Text = "" And _
               .TextBox2.Text = "" And _
               .TextBox3.Text = "" Then
                   .TextBox4.Enabled = True
                   .TextBox4.BackColor = vbBlack
            End If
        End With
        Me.Label5.Font.Strikethrough = False
    End If
End Sub

Private Sub TextBox3_Change()
    If Me.TextBox3.Text <> "" Then
        With Me.TextBox4
            .Text = ""
            .Enabled = False
            .BackColor = vbGreen
        End With
        Me.Label5.Font.Strikethrough = True
    Else
        With Me
            If .TextBox1.Text = "" And _
               .TextBox2.Text = "" And _
               .TextBox3.Text = "" Then
                   .TextBox4.Enabled = True
                   .TextBox4.BackColor = vbBlack
            End If
        End With
        Me.Label5.Font.Strikethrough = False
    End If
End Sub

Private Sub TextBox4_Change()
    Dim TBox As Integer
    
    If Me.TextBox4.Text <> "" Then
        For TBox = 1 To 3
            With Me.Controls("TextBox" & TBox)
                .Enabled = False
                .BackColor = vbGreen
            End With
        Next TBox
        Me.Label2.Font.Strikethrough = True
        Me.Label3.Font.Strikethrough = True
        Me.Label4.Font.Strikethrough = True
    Else
        For TBox = 1 To 3
            With Me.Controls("TextBox" & TBox)
                .Enabled = True
                .BackColor = vbBlack
            End With
        Next TBox
        Me.Label2.Font.Strikethrough = False
        Me.Label3.Font.Strikethrough = False
        Me.Label4.Font.Strikethrough = False
    End If
End Sub

Private Sub ToggleButton1_Click()
    Static Active As Boolean
    Active = Not Active
    
    If Active = True Then
        Marksman = True
    Else: Marksman = False
    End If
End Sub

Private Sub ToggleButton2_Click()
    Static Active As Boolean
    Active = Not Active
    
    If Active = True Then
        Sharpshooter = True
    Else: Sharpshooter = False
    End If
End Sub

Private Sub ToggleButton3_Click()
    Static Active As Boolean
    Active = Not Active
    
    If Active = True Then
        Expert = True
    Else: Expert = False
    End If
End Sub

Private Sub ToggleButton4_Click()
    Static Active As Boolean
    Active = Not Active
    
    If Active = True Then
        UNQ = True
    Else: UNQ = False
    End If
End Sub

Private Sub ToggleButton5_Click()
    Static Active As Boolean
    Active = Not Active
    
    If Active = True Then
        PFT_1st = True
    Else: PFT_1st = False
    End If
End Sub

Private Sub ToggleButton6_Click()
    Static Active As Boolean
    Active = Not Active
    
    If Active = True Then
        PFT_2nd = True
    Else: PFT_2nd = False
    End If
End Sub

Private Sub ToggleButton7_Click()
    Static Active As Boolean
    Active = Not Active
    
    If Active = True Then
        PFT_3rd = True
    Else: PFT_3rd = False
    End If
End Sub

Private Sub ToggleButton8_Click()
    Static Active As Boolean
    Active = Not Active
    
    If Active = True Then
        PFT_UNQ = True
    Else: PFT_UNQ = False
    End If
End Sub

Private Sub ToggleButton9_Click()
    Static Active As Boolean
    Active = Not Active
    
    If Active = True Then
        CFT_1st = True
    Else: CFT_1st = False
    End If
End Sub

Private Sub ToggleButton10_Click()
    Static Active As Boolean
    Active = Not Active
    
    If Active = True Then
        CFT_2nd = True
    Else: CFT_2nd = False
    End If
End Sub

Private Sub ToggleButton11_Click()
    Static Active As Boolean
    Active = Not Active
    
    If Active = True Then
        CFT_3rd = True
    Else: CFT_3rd = False
    End If
End Sub

Private Sub ToggleButton12_Click()
    Static Active As Boolean
    Active = Not Active
    
    If Active = True Then
        CFT_UNQ = True
    Else: CFT_UNQ = False
    End If
End Sub

Private Sub ToggleButton13_Click()
    Static Active As Boolean
    Active = Not Active

    If Active = True Then
        NBC_1 = True
    Else: NBC_1 = False
    End If
End Sub

Private Sub ToggleButton14_Change()
    If Me.ToggleButton14.Value = False Then
        Scope_Toggle(0) = False
        No_Trigger = True
        With Me.ListBox1
            .ColumnCount = 1
            .Clear
            .AddItem "ListView: Grid", 0
            .AddItem "ListView: BTR", 1
        End With
    End If
End Sub

Private Sub ToggleButton14_Click()
' Options | _
```````````
'
' +     1.) View Personnel Table - This table must remain visible for identification; remove this option (all the other tables can be toggled 'ON' or 'OFF')
' +     2.) View Rifle Table
' +     3.) View PFT Table
' +     4.) View CFT Table
' +     5.) View Swim Table
' +     6.) View MCMAP Table
' +     7.) View NBC Table
    
    If No_Trigger = False Then
        Scope_Toggle(0) = Not Scope_Toggle(0)
        Config_Option(0) = False
        Config_Option(1) = False
        Config_Option(2) = False
        Config_Option(3) = False
        Config_Option(4) = False
        Config_Option(5) = False
        If Scope_Toggle(0) = False Then
            With Me.ListBox1
                .ColumnCount = 1
                .Clear
                .AddItem "ListView: Grid", 0
                .AddItem "ListView: BTR", 1
            End With
        Else
            ReDim StrArray(6, 1)
            StrArray(0, 0) = "Get Duty Status Data"
            StrArray(0, 1) = "Get Contract Data"
            StrArray(1, 0) = "Get Service Data"
            StrArray(1, 1) = "Get Education Data"
            StrArray(2, 0) = "Get BCP Data"
            StrArray(2, 1) = "Get PFT Data"
            StrArray(3, 0) = "Get CFT Data"
            StrArray(3, 1) = "Get Swim Data"
            StrArray(4, 0) = "Get MCMAP Data"
            StrArray(4, 1) = "Get NBC Data"
            StrArray(5, 0) = "Get Rifle Data"
            StrArray(5, 1) = "Get Pistol Data"
            StrArray(6, 0) = "Personnel_Only"
            With Me.ListBox1
                .Clear
                .ColumnCount = 2
                .List = StrArray
            End With
        End If
    End If
    No_Trigger = False
End Sub

Private Sub ToggleButton15_Change()
    If Me.ToggleButton15.Value = False Then
        Scope_Toggle(1) = False
        No_Trigger = True
        With Me.ListBox1
            .ColumnCount = 1
            .Clear
            .AddItem "ListView: Grid", 0
            .AddItem "ListView: BTR", 1
        End With
    End If
End Sub

Private Sub ToggleButton15_Click()
' Options | _
```````````
'
' +     1.) Officers
' +     2.) SNCO's
' +     3.) NCO's
' +     4.) Junior Marines

    If No_Trigger = False Then
        Scope_Toggle(1) = Not Scope_Toggle(1)
        If Scope_Toggle(1) = False Then
            With Me.ListBox1
                .ColumnCount = 1
                .Clear
                .AddItem "ListView: Grid", 0
                .AddItem "ListView: BTR", 1
            End With
        Else
            ReDim StrArray(3, 0)
            StrArray(0, 0) = "List Officers"
            StrArray(1, 0) = "List SNCO's"
            StrArray(2, 0) = "List NCO's"
            StrArray(3, 0) = "List Junior Marines"
            With Me.ListBox1
                .Clear
                .ColumnCount = 1
                .List = StrArray
            End With
        End If
    End If
    No_Trigger = False
End Sub

Private Sub ToggleButton16_Click()

' Options | _
```````````
'
' +     1.) Remove EDIPI Field
' +     2.) Remove DCTB Field
' +     3.) Remove Unit Field
' +     4.) Remove Shop Field
'
' *     Create an option to add more fields utilizing the Log Sheet (might not be necessary, depending if MOL Update Files available)

End Sub

Private Sub ToggleButton17_Click()
    Static Active As Boolean
    Active = Not Active
        
    If Active = True Then
        With Me.ToggleButton17
            .Caption = "Complete"
            .Font.Bold = True
            .BackColor = vbGreen
            .ForeColor = vbBlack
        End With
        Rifle_Complete = True
        Rifle_Incomplete = False
    Else
        With Me.ToggleButton17
            .Caption = "NOT Complete"
            .Font.Bold = True
            .BackColor = vbRed
            .ForeColor = vbWhite
        End With
        Rifle_Complete = False
        Rifle_Incomplete = True
    End If
End Sub

Private Sub ToggleButton18_Click()
    Static Active As Boolean
    Active = Not Active
        
    If Active = True Then
        With Me.ToggleButton18
            .Caption = "Complete"
            .Font.Bold = True
            .BackColor = vbGreen
            .ForeColor = vbBlack
        End With
        PFT_Complete = True
        PFT_Incomplete = False
    Else
        With Me.ToggleButton18
            .Caption = "NOT Complete"
            .Font.Bold = True
            .BackColor = vbRed
            .ForeColor = vbWhite
        End With
        PFT_Complete = False
        PFT_Incomplete = True
    End If
End Sub

Private Sub ToggleButton19_Click()
    Static Active As Boolean
    Active = Not Active
        
    If Active = True Then
        With Me.ToggleButton19
            .Caption = "Complete"
            .Font.Bold = True
            .BackColor = vbGreen
            .ForeColor = vbBlack
        End With
        CFT_Complete = True
        CFT_Incomplete = False
    Else
        With Me.ToggleButton19
            .Caption = "NOT Complete"
            .Font.Bold = True
            .BackColor = vbRed
            .ForeColor = vbWhite
        End With
        CFT_Complete = False
        CFT_Incomplete = True
    End If
End Sub

Private Sub ToggleButton20_Click()
    Static Active As Boolean
    Active = Not Active
        
    If Active = True Then
        With Me.ToggleButton20
            .Caption = "Complete"
            .Font.Bold = True
            .BackColor = vbGreen
            .ForeColor = vbBlack
        End With
        Swim_Complete = True
        Swim_Incomplete = False
    Else
        With Me.ToggleButton20
            .Caption = "NOT Complete"
            .Font.Bold = True
            .BackColor = vbRed
            .ForeColor = vbWhite
        End With
        Swim_Complete = False
        Swim_Incomplete = True
    End If
End Sub

Private Sub ToggleButton21_Click()
    Static Active As Boolean
    Active = Not Active

    If Active = True Then
        NBC_2 = True
    Else: NBC_2 = False
    End If
End Sub

Private Sub UserForm_Initialize()
    Dim PageCOM As Control, UFcontrol As Control
    Dim Combo As Integer, Counter As Integer, Last_Element As Integer, Target As Integer
    
    Query = False
    No_Trigger = False
    Marksman = False
    Sharpshooter = False
    Expert = False
    UNQ = False
    PFT_1st = False
    PFT_2nd = False
    PFT_3rd = False
    PFT_UNQ = False
    CFT_1st = False
    CFT_2nd = False
    CFT_3rd = False
    CFT_UNQ = False
    NBC_1 = False
    Get_Tables = False
    Get_Ranks = False
    Ranks_After_Query = False
    Back2Query = False
    List_Junior_Marines = False
    List_NCO = False
    List_SNCO = False
    List_Officers = False
    
    Call Label1_Click
    Call Label9_Click
    Call Label10_Click
    Call Label11_Click
    Call Label17_Click
    Call ListBox1_Click
    
    With Me
        .OptionButton1.Value = True
        .OptionButton2.Enabled = False
        .OptionButton3.Enabled = False
        .ToggleButton16.Enabled = False
        .BackColor = vbBlack
        .Frame13.Visible = False
        .ComboBox9.Enabled = False
        .ComboBox9.BackColor = vbGreen
        .Image2.BorderColor = vbBlack
        .SpinButton1.ForeColor = vbBlue
        .SpinButton1.BackColor = RGB(100, 100, 100)
        .MultiPage1.BackColor = vbBlack
        .Caption = "«VMM-162» [Training & Education] Shell-Script " & This_Title
        
        ' Enable Checkboxes within the ListBox Control
        .ListBox1.ListStyle = fmListStyleOption
        .ListBox1.MultiSelect = fmMultiSelectExtended
        .ComboBox8.Clear
        For Each PageCOM In Me.MultiPage1.Pages
            For Each UFcontrol In PageCOM.Controls
                If TypeName(UFcontrol) = "Frame" Then
                    UFcontrol.BackColor = RGB(200, 200, 200)
                End If
                If TypeName(UFcontrol) = "Label" Then
                    UFcontrol.BackColor = RGB(200, 200, 200)
                End If
                If TypeName(UFcontrol) = "TextBox" Then
                    UFcontrol.Font.Size = 8
                    UFcontrol.BackColor = vbBlack
                    UFcontrol.ForeColor = vbGreen
                End If
                If TypeName(UFcontrol) = "ComboBox" Then
                    UFcontrol.Font.Size = 8
                    UFcontrol.BackColor = vbBlack
                    UFcontrol.ForeColor = vbGreen
                End If
            Next UFcontrol
        Next PageCOM
        With .Image1
            .BackColor = vbBlack
            .BorderColor = vbBlack
        End With
        With .Image2
            .BackColor = vbBlack
            .BorderColor = vbBlack
        End With
        With .ListBox1
            .Font.Size = 8
            .BackColor = vbBlack
            .ForeColor = vbGreen
        End With
        With .ListView1
            .Font.Size = 8
            .BackColor = vbBlack
            .ForeColor = vbGreen
        End With
        With .Frame1
            .Font.Bold = True
            .ForeColor = vbRed
            .BackColor = vbBlack
            .BorderColor = vbRed
            .BorderStyle = fmBorderStyleSingle
        End With
        With .Frame12
            .Font.Bold = True
            .ForeColor = vbYellow
            .BackColor = vbRed
            .BorderColor = vbYellow
            .BorderStyle = fmBorderStyleSingle
        End With
        For Target = 10 To 11
            With .Controls("Frame" & Target)
                .Font.Bold = True
                .ForeColor = vbYellow
                .BackColor = vbBlue
                .BorderColor = vbRed
                .BorderStyle = fmBorderStyleSingle
            End With
        Next Target
        For Target = 13 To 16
            .Controls("Frame" & Target).Visible = False
        Next Target
        For Target = 1 To 24
            With .Controls("Checkbox" & Target)
                .ForeColor = vbBlack
                .BackColor = RGB(200, 200, 200)
            End With
            If Target < 6 Then
                With .Controls("CommandButton" & Target)
                    .BackColor = RGB(100, 100, 100)
                    .ForeColor = vbBlue
                    .Font.Bold = True
                End With
            End If
            If Target < 4 Then
                With .Controls("OptionButton" & Target)
                    .ForeColor = vbWhite
                    .BackColor = vbBlack
                End With
            ElseIf Target < 6 Then
                With .Controls("OptionButton" & Target)
                    .ForeColor = vbBlack
                    .BackColor = RGB(200, 200, 200)
                End With
            End If
        Next Target
        
        Call Get_ComboList
        For Combo = 1 To 8
            With .Controls("ComboBox" & Combo)
                .Font.Size = 8
                .BackColor = vbBlack
                .ForeColor = vbGreen
                If Combo = 8 Then: Exit For

                Counter = 1
                .AddItem "NONE", 0
                Select Case Combo
                    Case 1: Last_Element = LastUE
                    Case 2: Last_Element = LastSE
                    Case 3: Last_Element = LastPE
                    Case 4: Last_Element = LastCE
                    Case 5: Last_Element = LastWE
                    Case 6: Last_Element = LastBE
                    Case 7: Last_Element = LastIE
                End Select
                For Target = 0 To Last_Element
                    If Target <> 0 Then: Counter = Counter + 1
                    Select Case Combo
                        Case 1: .AddItem UnitList(Target), Counter
                        Case 2: .AddItem ShopList(Target), Counter
                        Case 3: .AddItem PFT_List(Target), Counter
                        Case 4: .AddItem CFT_List(Target), Counter
                        Case 5: .AddItem Swim_Qual(Target), Counter
                        Case 6
                            If Belt_List(Target) <> Empty Then
                                .AddItem Belt_List(Target), Counter
                            End If
                        Case 7
                            If Belt_Inst(Target) <> Empty Then
                                .AddItem Belt_Inst(Target), Counter
                            End If
                    End Select
                Next Target
                .Text = "NONE"
            End With
        Next Combo
    End With
    ThisWorkbook.Sheets(1).Visible = xlVeryHidden
End Sub

Private Sub UserForm_QueryClose(Cancel As Integer, _
  CloseMode As Integer)
  If CloseMode = vbFormControlMenu Then
    Cancel = True
    MsgBox "Please press the " & Chr(34) & "Exit Program" & Chr(34) & " button!"
  End If
End Sub

Private Sub UserForm_Terminate()
    Set colPersonnel = Me.Personnel
    Set colRifle = Me.Rifle
    Set colPFT = Me.PFT
    Set colCFT = Me.CFT
    Set colSwim = Me.SWIM
    Set colMCMAP = Me.MCMAP
    Set colNBC = Me.NBC
End Sub

Public Property Get Records() As Collection
    Set Records = mcolRecords
End Property

Public Property Set Records(colRecords As Collection)
    Set mcolRecords = colRecords
End Property

Public Property Get Personnel() As Collection
    Set Personnel = mcolPersonnel
End Property

Public Property Set Personnel(colPersonnel As Collection)
    Set mcolPersonnel = colPersonnel
End Property

Public Property Get Duty_Status() As Collection
    Set Duty_Status = mcolDuty_status
End Property

Public Property Set Duty_Status(colDuty_Status As Collection)
    Set mcolDuty_status = colDuty_Status
End Property

Public Property Get Contract() As Collection
    Set Contract = mcolContract
End Property

Public Property Set Contract(colContract As Collection)
    Set mcolContract = colContract
End Property

Public Property Get Service() As Collection
    Set Service = mcolService
End Property

Public Property Set Service(colService As Collection)
    Set mcolService = colService
End Property

Public Property Get Education() As Collection
    Set Education = mcolEducation
End Property

Public Property Set Education(colEducation As Collection)
    Set mcolEducation = colEducation
End Property

Public Property Get BCP() As Collection
    Set BCP = mcolBCP
End Property

Public Property Set BCP(BCP As Collection)
    Set mcolBCP = colBCP
End Property

Public Property Get PFT() As Collection
    Set PFT = mcolPFT
End Property

Public Property Set PFT(colPFT As Collection)
    Set mcolPFT = colPFT
End Property

Public Property Get CFT() As Collection
    Set CFT = colCFT
End Property

Public Property Set CFT(colCFT As Collection)
    Set mcolCFT = colCFT
End Property

Public Property Get SWIM() As Collection
    Set SWIM = mcolSwim
End Property

Public Property Set SWIM(colSwim As Collection)
    Set mcolSwim = colSwim
End Property

Public Property Get MCMAP() As Collection
    Set MCMAP = mcolMCMAP
End Property

Public Property Set MCMAP(colMCMAP As Collection)
    Set mcolMCMAP = colMCMAP
End Property

Public Property Get NBC() As Collection
    Set NBC = mcolNBC
End Property

Public Property Set NBC(colNBC As Collection)
    Set mcolNBC = colNBC
End Property

Public Property Get Rifle() As Collection
    Set Rifle = mcolRifle
End Property

Public Property Set Rifle(colRifle As Collection)
    Set mcolRifle = colRifle
End Property

Public Property Get Pistol() As Collection
    Set Pistol = mcolPistol
End Property

Public Property Set Pistol(colPistol As Collection)
    Set mcolPistol = colPistol
End Property